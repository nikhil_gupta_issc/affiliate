<?php
$host       =   "localhost";
$user       =   "eduloan";
$password   =   "eduloans@123";
$db         =   "zenfox";

$conn =  mysqli_connect($host, $user, $password, $db);
if(!$conn){
    die(mysqli_connect_error());
}

$query_string_affiliate_tracker = "SELECT * FROM affiliate_tracker";
$query_obj_affiliate_tracker = mysqli_query($conn, $query_string_affiliate_tracker);
if(!$query_obj_affiliate_tracker){
    die(mysqli_error($conn));
}

while($result_affiliate_tracker = mysqli_fetch_assoc($query_obj_affiliate_tracker)){
    $tracker_id = $result_affiliate_tracker['tracker_id'];
    $query_string_sum_clicks = "SELECT sum(no_of_clicks) AS total_clicks FROM trackers_clicks WHERE tracker_id = " . $tracker_id;
    $query_obj_sum_clicks = mysqli_query($conn, $query_string_sum_clicks);
    if(!$query_obj_sum_clicks){
        die(mysqli_error($conn));
    }
    $result_sum = mysqli_fetch_assoc($query_obj_sum_clicks);
    if($result_sum){
        $total_clicks = $result_sum['total_clicks'];
    }

    $commission_type = $result_affiliate_tracker['commission_type'];
    if($commission_type == 'AMOUNT'){
        $amount = $result_affiliate_tracker['commission'];
        $total_commission = $amount * $total_clicks;

        $query_update = "UPDATE affiliate_tracker SET earning = " . $total_commission . " WHERE tracker_id = " . $tracker_id;
        if(!mysqli_query($conn, $query_update)){
            die(mysqli_error($conn));
        }
    }
    else{
        $amount = $result_affiliate_tracker['commission'];
        $total_commission = $amount * $total_clicks * 10;

        $query_update = "UPDATE affiliate_tracker SET earning = " . $total_commission . " WHERE tracker_id = " . $tracker_id;
        if(!mysqli_query($conn, $query_update)){
            die(mysqli_error($conn));
        }
    }
}
?>
