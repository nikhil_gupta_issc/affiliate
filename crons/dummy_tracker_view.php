<?php
$host       =   "localhost";
$user       =   "eduloan";
$password   =   "eduloans@123";
$db         =   "zenfox";

$conn =  mysqli_connect($host, $user, $password, $db);
if(!$conn){
    die(mysqli_connect_error());
}

$yesterday_date = date('Y-m-d', strtotime("-1 days"));

$query_string_trackers = "SELECT * FROM affiliate_tracker";
$query_obj_trackers = mysqli_query($conn, $query_string_trackers);
if(!$query_obj_trackers){
    die(mysqli_error($conn));
}

while($result_trackers = mysqli_fetch_assoc($query_obj_trackers)){
    $tracker_id = $result_trackers['tracker_id'];

    $query_string = "SELECT * FROM trackers_clicks WHERE tracker_id = " . $tracker_id;
    $query_obj = mysqli_query($conn, $query_string);
    if(!$query_obj){
        die(mysqli_error($conn));
    }

    if(!mysqli_num_rows($query_obj)){
        $query_string_static = "SELECT * FROM trackers_clicks WHERE tracker_id = 1";
        $query_obj_static = mysqli_query($conn, $query_string_static);
        if(!$query_obj_static){
            die(mysqli_error($conn));
        }

        while($result_static = mysqli_fetch_assoc($query_obj_static)){
            $no_of_clicks = mt_rand(0, 100);
            $query_insert_static = "INSERT INTO trackers_clicks(tracker_id, no_of_clicks, date) VALUES(" . $tracker_id . ", " . $no_of_clicks . ", '" . $result_static['date'] . "')";
            if(!mysqli_query($conn, $query_insert_static)){
                die(mysqli_error($conn));
            }
        }
    }

    $query_string_tracker_clicks = "SELECT * FROM trackers_clicks WHERE date = '" . $yesterday_date . "' AND tracker_id = " . $tracker_id;
    $query_obj_tracker_clicks = mysqli_query($conn, $query_string_tracker_clicks);
    if(!$query_obj_tracker_clicks){
        die(mysqli_error($conn));
    }

    if(!mysqli_num_rows($query_obj_tracker_clicks)){
        $no_of_clicks = mt_rand(0, 100);
        $query_insert_tracker_clicks = "INSERT INTO trackers_clicks(tracker_id, no_of_clicks, date) VALUES(" . $tracker_id . ", " . $no_of_clicks . ", '" . $yesterday_date . "')";
        if(!mysqli_query($conn, $query_insert_tracker_clicks)){
            die(mysqli_error($conn));
        }
    }
}
?>
