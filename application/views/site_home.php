<!DOCTYPE html>
<html lang="en-US">

<head>
    <title>
        Affiliate
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('sitehome-assets/style.css'); ?>">
</head>

<body>
    <!--  Nav bar begins -->
    <nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar" id="navbarScroll">
        <!-- Brand and toggle get grouped for better mobile display -->
        <a href="index.html" class="navbar-brand"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="lni-menu"></i>
        </button>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">
                <li class="nav-item active">
                    <a class="nav-link" href="#home">
                        Home
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#learn">
                        Learn more
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#about">
                        About
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">
                        Contact
                    </a>
                </li>
                <li class="nav-item boxes">
                    <!-- <a class="nav-link" data-toggle="modal" data-target=".register-form-modal-lg">
                        Sign Up
                    </a> -->
                    <a class="nav-link btn login-nav" href="<?php echo base_url('auth/signup'); ?>">
                        Sign Up
                    </a>
                </li>
                <li class="nav-item boxes login-nav">
                    <a class="nav-link btn login" href="<?php echo base_url('login'); ?>">
                        Login
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- Navbar Ends -->
    <div data-spy="scroll" data-target="#navbarScroll" data-offset='60'>
        <div class="main-body img-fluid" id="home">
            <div class="container">
                <div class="row home-body-section">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 intro-board">
                        <div class="intro-section">
                        <h2>Explore</h2>

                        <h1 class="igaming"> iGaming World</h1><br>

                            <!-- <button class="btn btn-success btn-md signup"> SIGN UP</button> -->
                            <a class="btn btn-success btn-md signup" href="<?php echo base_url('auth/signup'); ?>">SIGN UP</a>
                            <a class="btn login btn-light btn-md" href="<?php echo base_url('/login'); ?>">LOGIN</a>
                            <!-- <button class="btn login btn-light btn-md"> LOGIN</button> -->
                        </div>
                    </div>

                </div>
                <div class="diag-home">
                    <div class="flex">
                        <div class="row skewed-row text-center">
                            <div class="col-md-3">
                                <div class="card card-0">
                                    <img class="card-img-top" src="<?php echo base_url('sitehome-assets/assets/icons/chip.svg'); ?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">The best deals</h5>
                                        <p class="card-text">Our deals are the best you
                                            will find on any network</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-1">
                                    <img class="card-img-top" src="<?php echo base_url('sitehome-assets/assets/icons/clock.svg'); ?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">On time payments</h5>
                                        <p class="card-text">We always pay within 15 days of the close of the month</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-2">
                                    <img class="card-img-top" src="<?php echo base_url('sitehome-assets/assets/icons/add-user-button.svg'); ?>"
                                        alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">Experience</h5>
                                        <p class="card-text">Our dedicated team has decades of experience in iGaming</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-3">
                                    <img class="card-img-top" src="<?php echo base_url('sitehome-assets/assets/icons/like-thumb-up.svg'); ?>"
                                        alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title">Trusted partners</h5>
                                        <p class="card-text">We only work with brands we know personally and trust</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-body" id="learn">
            <div class="container-fluid listed-items">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row affiliate-row">
                            <img class="affiliate" src="<?php echo base_url('sitehome-assets/assets/images/affiliate-icon-1.png'); ?>" />
                            <h2>Affiliate</h2>
                        </div>
                        <h3>Join - Promote - Earn</h3>
                        <div>
                            <ul class="publish-list">
                                <li>One simple login</li>
                                <li>One on time payment</li>
                                <li>One point of contact</li>
                                <li>Access to 100+ brands</li>
                                <li>Access to 500+ campaigns</li>
                                <li>Dedicated help & support</li>
                                <li>Exclusive campaigns & promotions</li>
                                <input type="button" class="publisher" data-toggle="modal"
                                    data-target=".register-form-modal-lg" value="✓ Become a publisher now" />
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row publisher-row">
                            <img class="advertiser" src="<?php echo base_url('sitehome-assets/assets/images/business-person.png'); ?>" height="50" width="50"
                                alt="Advertiser">
                            <h2>Advertiser</h2>
                        </div>
                        <h3>Join - Advertise - Get Results</h3>
                        <div>
                            <ul class="publish-list">
                                <li>One point of contact</li>
                                <li>High quality traffic</li>
                                <li>A large pool of affiliates</li>
                                <li>Multiple countries</li>
                                <li>Web, e-mail & mobile traffic</li>
                                <li>Flexibility & understanding</li>
                                <li>Diligent campaign management</li>
                                <button type="button" class="publisher" class="nav-link" href="#contact"><i
                                        class="fa fa-phone phone"></i>
                                    Contact
                                    Us</button>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- <img class="infographic" src="assets/images/infographic.png"> -->
            </div>
            <div class="white-board">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-lg-4 align-middle">
                            <div id="count">
                                <h2>150+ <span class="count">Brands</span></h2>
                            </div>
                        </div>
                        <div class="col-lg-4 align-middle">
                            <div id="count">
                                <h2>1000+ <span class="count">Campaigns</span></h2>
                            </div>
                        </div>
                        <div class="col-lg-4 align-middle">
                            <div id="count">
                                <h2>1000+ <span class="count">affiliates</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-body" id="about">
            <div class="flex p-1">
            <div class="container about">
                    <h2>About </h2>
                    <p class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at massa ex. Aenean eget diam elementum, cursus lectus non, porttitor enim. Cras sollicitudin ligula quis enim hendrerit molestie. Maecenas a augue mi. Donec pulvinar at orci in porta. Phasellus porttitor lorem at eros cursus ultricies in in massa. Ut sollicitudin libero dignissim placerat dictum. Etiam pulvinar eros nec odio varius dapibus. Mauris vitae lectus at arcu blandit pellentesque. Morbi nisi libero, laoreet et porttitor et, convallis sed ante. Morbi rutrum luctus orci vel vestibulum.<br>
                    </p>

                    <p>Pellentesque fermentum dapibus nunc, eu posuere quam ullamcorper vel. Aenean mollis aliquet neque in consectetur. Vestibulum laoreet ex vitae bibendum pellentesque. Aliquam vitae tellus a leo ullamcorper rhoncus a quis diam. Mauris sem turpis, congue vehicula risus pellentesque, consequat tempor quam. Nullam id nunc erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas aliquet nisi quis scelerisque egestas. Suspendisse est velit, consectetur luctus orci et, scelerisque accumsan nulla. Duis malesuada magna id semper faucibus. Praesent sit amet mattis ex, faucibus mattis lorem.</p>

                    <p> Maecenas vulputate arcu et volutpat laoreet. Etiam pretium diam ut ligula pulvinar lobortis. Donec a cursus risus. Pellentesque eros enim, dictum non eros non, sollicitudin mattis justo. Maecenas eget posuere arcu.<br></p>
                </div>
            </div>
        </div>
        <div class="main-body" id="contact">
            <h3 class="text-center">Contact Us</h3>
            <div class="flex">
                <div class="container-fluid">
                    <div class="row contact">
                        <div class="col-md-6 col-xs-12">
                           
                            <div class="content-frm">
                                <h2 class="h2">Contact</h2>
                                <!-- Display the status message -->
                                <?php if(!empty($status)){ ?>
                                <div class="status text-primary <?php echo $status['type']; ?>"><?php echo $status['msg']; ?></div>
                                <?php } ?>
                                
                                <!-- Contact form -->
                                <form action="" method="post">
                                    
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control" value="<?php echo !empty($postData['name'])?$postData['name']:''; ?>" placeholder="Your Name">
                                        <?php echo form_error('name','<p class="text-danger">','</p>'); ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control" value="<?php echo !empty($postData['email'])?$postData['email']:''; ?>" placeholder="Your Email">
                                        <?php echo form_error('email','<p class="text-danger">','</p>'); ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="text" name="subject" class="form-control" value="<?php echo !empty($postData['subject'])?$postData['subject']:''; ?>" placeholder="Your Company">
                                        <?php echo form_error('subject','<p class="text-danger">','</p>'); ?>
                                    </div>
                                    
                                    <div class="form-group">
                                        <textarea name="message" class="form-control" rows="7" placeholder="Message.."><?php echo !empty($postData['message'])?$postData['message']:''; ?></textarea>
                                        <?php echo form_error('message','<p class="text-danger">','</p>'); ?>
                                    </div>
                                    
                                    <input type="submit" name="contactSubmit" class="btn btn-dark btn-md" value="Submit">
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12 space">
                           
                                <h3 class="text-center">Social Media</h3>
                            
                            <h5>You can also come in contact with us on social media if that is more your style</h5>
                            <div class="social-links">
                                <ul class='social-media'>
                                    <li>
                                        <a href="#" class="twitter"><i class="fa fa-twitter"></i>Twitter</a></li>
                                    <li>
                                        <a href="#" class="facebook"><i class="fa fa-facebook"></i>Facebook</a></li>
                                    <li>
                                        <a href="#" class="instagram"><i class="fa fa-instagram"></i>Instagram</a></li>
                                    <li>
                                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i>LinkedIn</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer bg-dark">
        <p class="text-center text-gray">Copyright 2019</p>
    </footer>
    
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="<?php echo base_url('sitehome-assets/script.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('sitehome-assets/scripts/contactform.js'); ?>"></script>
</body>

</html>