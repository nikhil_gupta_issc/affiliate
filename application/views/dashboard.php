<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/libs/css/style.css">
    <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="assets/vendor/vector-map/jqvmap.css">
    <link href="assets/vendor/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/daterangepicker/daterangepicker.css" />
    <title>Concept - Bootstrap 4 Admin Dashboard Template</title>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="/">Affiliate</a>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="assets/images/avatar-1.jpg" alt="" class="user-avatar-md rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <div class="nav-user-info">
                                    <h5 class="mb-0 text-white nav-user-name"><?=$userData['firstname'] . " " . $userData['lastname']?></h5>
                                    <span class="status"></span><span class="ml-2">Available</span>
                                </div>
                                <a class="dropdown-item" href="#"><i class="fas fa-user mr-2"></i>Account</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i>Setting</a>
                                <a class="dropdown-item" href="/auth/logout"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Menu
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link active" href="/dashboard" data-toggle="collapse" aria-expanded="false" data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-user-circle"></i>Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class="fa fa-fw fa-rocket"></i>Tracker</a>
                                <div id="submenu-2" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="/tracker/create">Create</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/tracker/view">View</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fa fa-fw fa-rocket"></i>Images</a>
                                <div id="submenu-3" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="/images/upload">Image Upload</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/images">View Images</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-finance">
                <div class="container-fluid dashboard-content">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h3 class="mb-2">Tracker Clicks </h3>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Tracker Clicks</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="offset-xl-10 col-xl-2 col-lg-2 col-md-6 col-sm-12 col-12">
                            <form>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="daterange" value="<?=date('Y-m-d', strtotime("-7 days"))?> - <?=date('Y-m-d', strtotime("-1 days"))?>" />
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if($trackers)
                        {
                            foreach($trackers as $index => $trackerDetail)
                            {
                                /*
                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="card">
                                        <h5 class="card-header"><?=$trackerDetail['tracker_name']?></h5>
                                        <div class="card-body">
                                            <div class="metric-value d-inline-block">
                                                <h1 class="mb-1"><?=$trackerDetail['clicks']?></h1>
                                            </div>
                                        </div>
                                        <div class="card-body bg-light p-t-40 p-b-40">
                                            <div id="sparkline-revenue-<?=$index?>"></div>
                                        </div>
                                    </div>
                                </div>
                                */
                                ?>
                                <div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="card">
                                        <h5 class="card-header"><?=$trackerDetail['tracker_name']?></h5>
                                        <div class="card-body">
                                            <div class="ct-chart-line-invoice-<?=$index?> ct-golden-section"></div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end revenue year  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- gross profit  -->
                        <!-- ============================================================== -->
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Earning</h5>
                                <div class="card-body">
                                    <div id="morris_gross" style="height: 272px;"></div>
                                </div>
                                <div class="card-footer bg-white">
                                    <p>Total Earning <span class="float-right text-dark"><?=$totalEarning?></span></p>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end gross profit  -->
                        <!-- ============================================================== -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- end profit margin -->
                    <!-- ============================================================== -->
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            Copyright © 2018 Concept. All rights reserved. Dashboard by <a href="https://colorlib.com/wp/">Colorlib</a>.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- jquery 3.3.1  -->
    <script type="text/javascript" src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script type="text/javascript"  src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script type="text/javascript"  src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- chart chartist js -->
    <script type="text/javascript"  src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <script type="text/javascript"  src="assets/vendor/charts/chartist-bundle/Chartistjs.js"></script>
    <script type="text/javascript" src="assets/vendor/charts/chartist-bundle/chartist-plugin-threshold.js"></script>
    <!-- chart C3 js -->
    <script type="text/javascript" src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <!-- chartjs js -->
    <script type="text/javascript" src="assets/vendor/charts/charts-bundle/Chart.bundle.js"></script>
    <script type="text/javascript" src="assets/vendor/charts/charts-bundle/chartjs.js"></script>
    <!-- sparkline js -->
    <script type="text/javascript" src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- dashboard finance js -->
    <!--script src="assets/libs/js/dashboard-finance.js"></script-->
    <!-- main js -->
    <script type="text/javascript" src="assets/libs/js/main-js.js"></script>
    <!-- gauge js -->
    <script type="text/javascript" src="assets/vendor/gauge/gauge.min.js"></script>
    <!-- morris js -->
    <script type="text/javascript" src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script type="text/javascript" src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <script type="text/javascript" src="assets/vendor/charts/morris-bundle/morrisjs.html"></script>
    <!-- daterangepicker js -->
    <script type="text/javascript"  src="../../../../cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="../../../../cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
    $(function() {
        var revenueString = '';
        var revenueArray = [];
        var trackerCount = '<?=$trackersCount?>';
        var allTrackers = JSON.parse('<?=$jsonTrackers?>');
        var trackerEarning = {};
        var allEarnings = [];
        for(var i = 0; i < parseInt(trackerCount); i++){
            revenueString = allTrackers[i]['revenueString'];
            trackerEarning = {};
            trackerEarning['value'] = allTrackers[i]['earning'];
            trackerEarning['label'] = allTrackers[i]['tracker_name'];
            allEarnings.push(trackerEarning);
	    if(revenueString){
		    revenueArray = revenueString.split(',');
	            $("#sparkline-revenue-" + i).sparkline(revenueArray, {
        	        type: 'line',
                	width: '99.5%',
	                height: '100',
        	        lineColor: '#5969ff',
                	fillColor: '',
	                lineWidth: 2,
        	        spotColor: undefined,
                	minSpotColor: undefined,
	                maxSpotColor: undefined,
        	        highlightSpotColor: undefined,
                	highlightLineColor: undefined,
	                resize:true
        	    });
	    }

            new Chartist.Line('.ct-chart-line-invoice-' + i, {
                labels: allTrackers[i]['click_date']['date'],
                series: [allTrackers[i]['click_date']['clicks']]
            }, {
                /*fullWidth: true,
                chartPadding: {
                    right: 40
                },*/
                axisY: {
                    labelInterpolationFnc: function(value) {
                        return value;
                    }
                },


            });
        }

        Morris.Donut({
             element: 'morris_gross',
             data: allEarnings,
             labelColor: '#5969ff',
             colors: [
                 '#5969ff',
                 '#a8b0ff'
             ],

             formatter: function(x) { return x + "%" },
               resize: true

         });
        $('input[name="daterange"]').daterangepicker({
            opens: 'left'
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });
    </script>
</body>
</html>
