<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $page_title; ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('sitehome-assets/home/home.css?v=1.1'); ?>">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">

<style>
.dataTables_empty {
    display: none;
  }
</style>

</head>
<body>
<div class="wrapper">
    <!-- Header Starts here -->
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="logo"></div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
            <div class="notification">
                <div class="row notification-row">
                    <p class="notif-name"> Hi, <b> <?=$userData['firstname'] . " " . $userData['lastname']?> </b></span></p>
                    <span class="notif"><i class="fa fa-bell" aria-hidden="true"></i></span>
                </div>
                <div class="row month">
                    Your <span class="font-weight-bold mx-1"> <?php echo date("F");?> </span> month stats
                </div>
                <div class="row">
                    <ul class="stats-list">
                        <li>Clicks <br> <?= $total_clicks; ?></li>
                        <li>Sign Ups <br> <?= $total_signups; ?></li>
                        <li>FTD <br> <?= $total_deposits; ?></li>
                        <li>Profit <br> &euro;<?= round($profit_per_day, 2) ?></li>
                        <!-- <li><button class="btn">&dollar;Details</button></li> -->
                    </ul>
                </div>
            </div>
        </div>
            <!-- Header Ends here -->
    </div>
    <div class="container-fluid">
        <div class="row nav-pills-row">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link <?=(!isset($_GET['page']) || $_GET['page']=='home') ? 'active': ''?>" id="pills-home-tab" href="/affiliate" role="tab"
                        aria-controls="pills-home" aria-selected="true">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='stats') ? 'active': ''?>" id="pills-stats-tab" href="/affiliate?page=stats" role="tab"
                        aria-controls="pills-stats" aria-selected="false">Stats</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='campaign') ? 'active': ''?>" id="pills-campaign-tab"  href="/affiliate?page=campaign" role="tab"
                        aria-controls="pills-campaign" aria-selected="false">Campaigns</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='deals') ? 'active': ''?>" id="pills-deals-tab"  href="/affiliate?page=deals" role="tab"
                        aria-controls="pills-deals" aria-selected="false">Deals</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='payments') ? 'active': ''?>" id="pills-payment-tab"  href="/affiliate?page=payments" role="tab"
                        aria-controls="pills-payment" aria-selected="false">Payments</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='promotions') ? 'active': ''?>" id="pills-promotions-tab"  href="/affiliate?page=promotions"
                        role="tab" aria-controls="pills-promotions" aria-selected="false">Promotions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='advertisers') ? 'active': ''?>" id="pills-advertisers-tab"  href="/affiliate?page=advertisers"
                        role="tab" aria-controls="pills-advertisers" aria-selected="false">Advertisers Requests</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='advertisers-deal') ? 'active': ''?>" id="pills-advertisers-tab-deal"  href="/affiliate?page=advertisers-deal"
                        role="tab" aria-controls="pills-advertisers" aria-selected="false">Deals Requests</a>
                </li>
                <li class="nav-item float-lg-right float-right profile-btn">
                    <a class="nav-link" <?=(isset($_GET['page']) && $_GET['page']=='profile') ? 'active': ''?> id="pills-profile-tab" href="/affiliate?page=profile" role="tab"
                        aria-controls="pills-profile" aria-selected="false">Profile</a>
                </li>
                <li class="nav-item float-lg-right float-right logout-btn">
                    <a class="nav-link"  href="/auth/logout">Logout</a>
                </li>
            </ul>
        </div>
        <!-- Navbar Ends here -->
        <!-- Page Content begins here -->
        <div class="container-fluid tab-content-container">
            <div class="tab-content" id="pills-tabContent">
                <!-- Home Tab content begins herre -->
                <div class="tab-pane fade <?=(!isset($_GET['page']) || $_GET['page']=='home') ? 'show active': ''?>" id="pills-home" role="tabpanel"
                    aria-labelledby="pills-home-tab">
                    <div class="container">
                        <div class="row stats-row">
                            <div class="col-md-2"><?= $new_advertisers; ?>
                                <div class="box">New Advertisers</div>
                                <img class="tap" src="<?php echo base_url('../assets/icons/affiliate_click.png'); ?>" height="25" width="25" alt="Tap">
                            </div>
                            <div class="col-md-2"><?= $total_clicks; ?>
                                <div class="box">Clicks</div>
                                <img class="tap" src="<?php echo base_url('sitehome-assets/assets/icons/tap.svg'); ?>" height="25" width="25" alt="Tap">
                            </div>
                            <div class="col-md-2"><?= $total_signups; ?>
                                <div class="box">Signups</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/list.svg'); ?>" height="25" width="25" alt="List">
                            </div>
                            <div class="col-md-2"><?= $total_deposits; ?>
                                <div class="box">FTD</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/credit-card.svg'); ?>" height="25" width="25"
                                    alt="FTD">
                            </div>
                            <!--div class="col-md-2">
                                <div class="box">CPA</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/visiting-card.svg'); ?>" height="25" width="25"
                                    alt="CPA">
                            </div-->
                            <div class="col-md-2"><?=$signup_to_deposit?>
                                <div class="box">Signup to deposit</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/double-angle-pointing-to-right.svg'); ?>"
                                    height="20" width="20" alt="Right">
                            </div>
                            <div class="col-md-2"> &euro; <?= round($profit_per_day, 2); ?><br>
                                <div class="box">Profit (Avg per day)</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/funds.svg'); ?>" height="25" width="25"
                                    alt="Cash">
                            </div>
                        </div>
                    </div>
                    <div class="container wrapper-container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card section-1">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/images/stats.png'); ?>" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Recent Earning</span>
                                        <button class="btn btn-default show_loss">Show Losses</button>
                                    </div>
                                    <table class="payout-table mx-3 my-3">
                                      <thead>
                                        <th>Total Advertisers</th>
                                        <th>New Advertisers</th>
                                        <th>Signups</th>
                                        <th>FTD</th>
                                        <th>Avg Earnings Per Month</th>
                                        <th>Total Earnings</th>
                                      </thead>
                                      <tr>
                                        <td><?=$total_advertisers?></td>
                                        <td><?=$new_advertisers?></td>
                                        <td><?=$total_signups?></td>
                                        <td><?=$total_deposits?></td>
                                        <td><?=$avg_month_earning?></td>
                                        <td><?=$total_earnings?></td>

                                      </tr>
                                    </table>
                                </div>
                                <div class="card section-2">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/images/pie-chart.png'); ?>" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Statistics Breakdown</span>
                                        <div class="dropdown stats-brerakdown">
                                            <button class="btn btn-light dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                <span id="selected">Choose option</span>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" onClick="affiliates()">Clicks</a>
                                                <!-- <a class="dropdown-item" onClick="newAffiliates()">New
                                                    Affiliates</a> -->
                                                <a class="dropdown-item" onClick="signUps()">Signups</a>
                                                <a class="dropdown-item" onClick="ftds()">FTD</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                    <!-- Here is the Graph canvas data -->
                                        <canvas id="myAffiliates" width="400" height="250"></canvas>
                                        <!-- <canvas id="bar-chart" width="400" height="250"></canvas> -->
                                    </div>
                                </div>
                                <div class="card section-3">
                                    <div class="row">
                                        <img class="section_img" src="../assets/images/bar-graph.jpg" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Past Month Stats</span>
                                        <div class="dropdown mothstats">
                                            <button class="btn btn-light dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                <span id="monthstats">Choose option</span>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" onClick="monthyClicks()">Clicks</a>
                                                <a class="dropdown-item" onClick="monthlySignups()">SignUps</a>
                                                <a class="dropdown-item" onClick="monthlyDeposits()">Deposits</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <canvas id="mySales" width="400" height="250"></canvas>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="bannerboard">
                                        <img class="banner-board img-fluid" src="../assets/images/ads-1.jpg"
                                            alt="banner board">
                                    </div>
                                    <div class="advboard">
                                        <img class="banner-board img-fluid" src="../assets/images/ads-2.jpg"
                                            alt="banner board">
                                    </div>
                                    <div class="prizeboard">
                                        <img class="banner-board img-fluid" src="../assets/images/ads-3.png"
                                            alt="banner board">
                                    </div>
                                    <div class="card payment-card">
                                        <div class="row">
                                            <img class="section_img" src="../assets/icons/images.png" height="40"
                                                width="40" alt="Stats">
                                            <span class="text-img"> Latest Payments</span>
                                            <button class="payment-btn btn-sm">All Payments</button>
                                        </div>

                                        <div class="row">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="leaderboard">
                                    <img class="img-fluid competition" src="<?php echo base_url('sitehome-assets/assets/images/mrgreen.jpg'); ?>"
                                        alt="Leader Board">
                                    <div class="row leaders">
                                        <ul class="leader-board">
                                            <li class="gold">
                                                <div class="row"><img src="<?php echo base_url('sitehome-assets/assets/images/gold-medal.png'); ?>"
                                                        height="20" width="20" alt="1st Prize"><span
                                                        class="leader-text"><b>1</b> FTD</span></div>
                                            </li>
                                            <li class="silver">
                                                <div class="row"><img src="<?php echo base_url('sitehome-assets/assets/images/silver-medal.png'); ?>"
                                                        height="20" width="20" alt="1st Prize"><span
                                                        class="leader-text"><b>2</b> FTD</span></div>
                                            </li>
                                            <li class="bronze">
                                                <div class="row"><img src="<?php echo base_url('sitehome-assets/assets/images/bronze-medal.png'); ?>"
                                                        height="20" width="20" alt="1st Prize"><span
                                                        class="leader-text"><b>3</b> FTD</span></div>
                                            </li>
                                            <li class="your-place">Your Place</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bannerboard">
                                    <img class="banner-board img-fluid" src="<?php echo base_url('sitehome-assets/assets/images/banner-2.jpg'); ?>"
                                        alt="banner board">
                                </div>
                                <div class="advboard">
                                    <img class="banner-board img-fluid" src="<?php echo base_url('sitehome-assets/assets/images/ads-1.jpg'); ?>"
                                        alt="banner board">
                                </div>
                                <div class="prizeboard">
                                    <img class="banner-board img-fluid" src="<?php echo base_url('sitehome-assets/assets/images/ads2.jpg'); ?>"
                                        alt="banner board">
                                </div>
                                <div class="card managerboard">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/icons/operator.jpg'); ?>" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Your Affiliate Managers</span>
                                    </div>
                                    <p class="manager-name"><b><?=$affiliate_manager['first_name'].' '.$affiliate_manager['last_name'] ?></b></p>
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/icons/skype.png'); ?>" height="30"
                                            width="30" alt="Stats">
                                        <span class="text-manager"><?= $affiliate_manager['skyp_id']?></span>
                                    </div>
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/icons/mail.png'); ?>" height="30"
                                            width="30" alt="Stats">
                                        <span class="text-manager"><?= $affiliate_manager['email']?></span>
                                    </div>
                                </div>

                                <div class="card payment-card">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets'); ?>/assets/icons/images.png" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Latest Payments</span>
                                        <button class="payment-btn btn-sm">All Payments</button>
                                    </div>

                                    <div class="row">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='stats') ? 'show active': ''?>" id="pills-stats" role="tabpanel" aria-labelledby="pills-stats-tab">
                    <div class="container statistic-container">
                        <div class="card time-card">
                            <div class="row">
                                <div class="col-lg-6 calendar-container">
                                    <b>Time Period</b>
                                    <div class="row">
                                        <p class="col-md-4 p-1 m-1"><input type="text" class="form-control" id="from-datepicker" placeholder="YYYY-MM-DD"></p>
                                        <!-- <input type="text" class="date hasDatepicker"
                                            id="from_date"> -->
                                        <p class="col-md-4 p-1 m-1"><input type="text" class="form-control" id="to-datepickerto" placeholder="YYYY-MM-DD"></p>
                                        <!-- <input type="date" name="previous_date"  id="previous_date" min="23-05-2019"> -->
                                        <button class="col-md-2 p-1 form-control m-1 btn btn-sm btn-success" id="stats_period_view">View</button>
                                    </div>
                                    <div class="alret alert-warning" id="dateWarning"></div>
                                </div>
                                <div class="col-lg-6 quick_dates">
                                    <b>Quick Months</b>
                                    <div class="row">
                                        <button class="months">Month 1</button>
                                        <button class="months">Month 2</button>
                                        <button class="months">Month 3</button>
                                        <button class="months">Month 4</button>
                                        <button class="months">Month 5</button>
                                    </div>
                                    <div class="row time-period">
                                        <button class="months">Today</button>
                                        <button class="months">Yesterday</button>
                                        <button class="months">This Year</button>
                                        <button class="months">Last Year</button>
                                        <button class="months">All Time</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- New container -->

                        <div class="container" id="time-overiew">
                            <div class="row stats-row">
                                <div class="col-md-2"><?= $new_advertisers; ?>
                                    <div class="box">New Advertisers</div>
                                    <img class="tap" src="<?php echo base_url('../assets/icons/affiliate_click.png'); ?>" height="25" width="25" alt="Tap">
                                </div>
                                <div class="col-md-2"><span id="time-clicks"><?= $total_clicks; ?></span>
                                    <div class="box">Clicks</div>
                                    <img class="tap" src="<?php echo base_url('sitehome-assets/assets/icons/tap.svg'); ?>" height="25" width="25" alt="Tap">
                                </div>
                                <div class="col-md-2"><span id="time-singups"><?= $total_signups ?></span>
                                    <div class="box">Signups</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/list.svg'); ?>" height="25" width="25"
                                        alt="List">
                                </div>
                                <div class="col-md-2"><span id="time-deposits"><?= $total_deposits; ?></span>
                                    <div class="box">FTD</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/credit-card.svg'); ?>" height="25" width="25"
                                        alt="FTD">
                                </div>
                                <!--div class="col-md-2"><span id="time-clicks"></span>
                                    <div class="box">CPA</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets'); ?>/assets/icons/visiting-card.svg" height="25"
                                        width="25" alt="CPA">
                                </div-->
                                <div class="col-md-2"><?=$signup_to_deposit?>
                                    <div class="box">Signup to deposit</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/double-angle-pointing-to-right.svg'); ?>"
                                        height="20" width="20" alt="Right">
                                </div>
                                <div class="col-md-2"> &euro; <?= round($profit_per_day, 2); ?><br>
                                    <div class="box">Profit (Avg per day)</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets'); ?>/assets/icons/funds.svg" height="25" width="25"
                                        alt="Cash">
                                </div>
                            </div>
                        </div>

                        <!-- Table section -->
                        <div class="card table-card">
                            <table class="payout-table mx-3 my-3">
                              <thead>
                                <th>Total Advertisers</th>
                                <th>New Advertisers</th>
                                <th>Signups</th>
                                <th>FTD</th>
                                <th>Avg Earnings Per Month</th>
                                <th>Total Earnings</th>
                              </thead>
                              <tr>
                                <td><?=$total_advertisers?></td>
                                <td><?=$new_advertisers?></td>
                                <td><?=$total_signups?></td>
                                <td><?=$total_deposits?></td>
                                <td><?=$avg_month_earning?></td>
                                <td><?=$total_earnings?></td>

                              </tr>
                            </table>
                            <!--table id="example" class="table table-responsive table-bordered">
                                <thead>
                                    <th>Advertiser</th>
                                    <th>Clicks</th>
                                    <th>Signups</th>
                                    <th>FTD</th>
                                    <th>Rev payout</th>
                                    <th>CPA</th>
                                    <th>CPA payout</th>
                                    <th>CPL payout</th>
                                    <th>Total payout</th>
                                </thead>
                                <tbody id="time-advertisers">
                                <?php
                                /*foreach($advertisers as $advertise) : ?>
                                <tr>
                                    <td><?= $advertise['alias'] ?></td>
                                    <td><?= $advertise['lending'] ?></td>
                                    <td><?= $advertise['signup'] ?></td>
                                    <td><?= $advertise['deposit'] ?></td>
                                    <td><?= "NA" ?></td>
                                    <td><?= "NA" ?></td>
                                    <td><?= "NA" ?></td>
                                    <td><?= "NA" ?></td>
                                    <td><?= "NA" ?></td>

                                </tr>
                                <?php endforeach; */
                                ?>
                                <tbody>
                            </table>


                            <table id="example-new" class="table table-bordered table-responsive" style="width:100%">
                                <thead>
                                    <tr>
                                    <th>Advertiser</th>
                                        <th>Clicks</th>
                                        <th>Signups</th>
                                        <th>FTD</th>
                                        <th>Rev payout</th>
                                        <th>CPA</th>
                                        <th>CPA payout</th>
                                        <th>CPL payout</th>
                                        <th>Total payout</th>
                                    </tr>
                                </thead>

                            </table-->

                            <!-- <table class="payout-table">
                                <thead>
                                    <th aria-sort="ascending">Advertiser</th>
                                    <th>Clicks</th>
                                    <th>Signups</th>
                                    <th>FTD</th>
                                    <th>Rev payout</th>
                                    <th>CPA</th>
                                    <th>CPA payout</th>
                                    <th>CPL payout</th>
                                    <th>Total payout</th>
                                </thead>
                                <tr>
                                    <td>Total</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                            </table> -->
                        </div>
                    </div>
                </div>
                <!-- Statistic COntainer Ends here -->
                <!-- Campaign container begins here -->
                <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='campaign') ? 'show active': ''?>" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">
                        <div class="card deals-card">
                            <div class="container" style="margin-left:inherit;">
                                <h3 class="my-2">Campaigns Deals</h3>
                                <div class="row">
                                <?php
                                    if($all_advertisers)
                                    {
                                        ?>
                                        <label for="advertisers" class="deal-type"><b>Select Advertiser</b></label>
                                        <select name="deal_type" id="deal-type" class="sm ml-2" onchange="changeAdvertiser('campaigns', this.value)">
                                        <option value="">All</option>
                                        <?php
                                        foreach($all_advertisers as $advertiser)
                                        {
                                            ?>
                                            <option value="<?=$advertiser['affiliate_id']?>" <?=($advertiser_id==$advertiser['affiliate_id']) ? 'selected' : ''?>><?=$advertiser['firstname'] . " " . $advertiser['lastname']?></option>
                                            <?php
                                        }
                                        ?>
                                        </select>
                                        <?php
                                    }
                                    ?>
                            </div>

                                <div class="row">
                                    <label class="deal-type" for="deal types"> <b>Deal Types</b></label>
                                    <div class="radio-class">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-outline-dark active">
                                                <input type="radio" name="options" id="option1" autocomplete="off"
                                                    checked>
                                                All Deals
                                            </label>
                                            <label class="btn btn-outline-dark">
                                                <input type="radio" name="options" id="option2" autocomplete="off">
                                                Revshare
                                            </label>
                                            <label class="btn btn-outline-dark">
                                                <input type="radio" name="options" id="option3" autocomplete="off"> CPA
                                            </label>
                                            <label class="btn btn-outline-dark">
                                                <input type="radio" name="options" id="option3" autocomplete="off"> CPL
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="deals-search">
                                    <div class="row">
                                        <label class="label-deal" for="Search"><b> Search:</b> <input type="text"
                                                placeholder="Search Deals" table="capaign_deals"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                            </div>
                        </div>
                        <div class="container deal-container" style="margin-left:inherit;">
                            <?php
                			if($campaigns)
                			{
                            foreach($campaigns as $index => $tracker)
                            {
                                if($index % 4 == 0)
                                {
                                    ?>
                                    <div class="row">
                                    <?php
                                }

                                $proposedCommission = isset($tracker['negotiations']['adv_commission']) ? $tracker['negotiations']['adv_commission'] : $tracker['commission'];
                                $proposedPercentage = isset($tracker['negotiations']['adv_percentage_hybrid']) ? $tracker['negotiations']['adv_percentage_hybrid'] : $tracker['commission_percentage_hybrid'];

                                ?>
                                <div class="col-lg-3">
                                    <div class="card deal-card">
                                        <div style="text-align: center;"><img src="<?=$tracker['banner_url']?>" height="200" width="200" alt="<?=$tracker['tracker_name']?>" style="margin-top:10px;"></div>
                                        <div class="card-body">
                                            <button class="btn-deals"><?=$tracker['deal_type']?></button>
                                            <button class="btn-details" data-toggle="modal" data-target="#campaignModal-<?=$index?>"> Details</button>
                                            <div class="modal fade" id="campaignModal-<?=$index?>" tabindex="-1" role="dialog" aria-labelledby="campaignModalLabel-<?=$index?>" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="campaignModalLabel-<?=$index?>">Deal Details</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table>
                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td><input type="text" value="<?=$tracker['tracker_name']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Deal Type</td>
                                                                    <td><input type="text" value="<?=$tracker['deal_type']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Your Commission</td>
                                                                    <td><input type="text" value="<?=$proposedCommission?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>AMS Commission</td>
                                                                    <td><input type="text" value="<?=$tracker['ams_commission']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Final Commission</td>
                                                                    <td><input type="text" value="<?=$proposedCommission - $tracker['ams_commission']?>"></td>
                                                                </tr>
                                                                <?php
                                                                if($tracker['deal_type'] == 'HYBRID')
                                                                {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Your Percentage</td>
                                                                        <td><input type="text" value="<?=$proposedPercentage?>"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>AMS Percentage</td>
                                                                        <td><input type="text" value="<?=$tracker['ams_commission_percentage_hybrid']?>"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Final Percentage</td>
                                                                        <td><input type="text" value="<?=$proposedPercentage - $tracker['ams_commission_percentage_hybrid']?>"></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <td>Website</td>
                                                                    <td><input type="text" value="<?=$tracker['website']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Tracker URL</td>
                                                                    <td>
                                                                        <textarea rows="5" cols="50">
                                                                            <?=$tracker['website'] . "?aId=" . $userData['affiliate_id']?>
                                                                        </textarea>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Close</button>
                                                            <button type="button" class="btn btn-primary">Save
                                                                changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php

                                if($index % 4 == 3)
                                {
                                    ?>
                                    </div>
                                    <?php
                                }
                            }
                            if($index % 4 != 3)
                            {
                                ?>
                                </div>
                                <?php
                            }
			}
                            ?>
                    </div>
                </div>
                <!-- Campaign container ends here -->
                <!-- Deal container begins here -->
                <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='deals') ? 'show active': ''?>" id="pills-deals" role="tabpanel" aria-labelledby="pills-deals-tab">
                    <div class="card deals-card">
                        <div class="container" style="margin-left:inherit;">
                            <h3 class="my-2">Campaigns Deals</h3>
                            <div class="row">
                                <?php
                                if($all_advertisers)
                                {
                                    ?>
                                    <label for="advertisers" class="deal-type"><b>Select Advertiser</b></label>
                                    <select name="deal_type" id="deal-type" class="sm ml-2" onchange="changeAdvertiser('deals', this.value)">
                                    <option value="">All</option>
                                    <?php
                                    foreach($all_advertisers as $advertiser)
                                    {
                                        ?>
                                        <option value="<?=$advertiser['affiliate_id']?>" <?=($advertiser_id==$advertiser['affiliate_id']) ? 'selected' : ''?>><?=$advertiser['firstname'] . " " . $advertiser['lastname']?></option>
                                        <?php
                                    }
                                    ?>
                                    </select>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="row">
                                <label class="deal-type" for="deal types"> <b>Deal Types</b></label>
                                <div class="radio-class">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-outline-dark active">
                                            <input type="radio" name="options" id="option1" autocomplete="off"
                                                checked>
                                            All Deals
                                        </label>
                                        <label class="btn btn-outline-dark">
                                            <input type="radio" name="options" id="option2" autocomplete="off">
                                            Revshare
                                        </label>
                                        <label class="btn btn-outline-dark">
                                            <input type="radio" name="options" id="option3" autocomplete="off"> CPA
                                        </label>
                                        <label class="btn btn-outline-dark">
                                            <input type="radio" name="options" id="option3" autocomplete="off"> CPL
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="deals-search">
                                <div class="row">
                                    <label class="label-deal" for="Search"><b> Search:</b> <input type="text"
                                            placeholder="Search Deals" table="capaign_deals"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container deal-container" style="margin-left:inherit;">
                        <?php
                        foreach($trackers as $index => $tracker)
                        {
                            if($index % 4 == 0)
                            {
                                ?>
                                <div class="row">
                                <?php
                            }
                            $proposedCommission = isset($tracker['negotiations']['adv_commission']) ? $tracker['negotiations']['adv_commission'] : $tracker['commission'];
                            $proposedPercentage = isset($tracker['negotiations']['adv_percentage_hybrid']) ? $tracker['negotiations']['adv_percentage_hybrid'] : $tracker['commission_percentage_hybrid'];
                            ?>

                            <div class="col-lg-3">
                                <form action="/tracker/campaign" method="POST">
                                <div class="card deal-card">
                                    <div style="text-align: center;"><img src="<?=$tracker['banner_url']?>" height="200" width="200" alt="<?=$tracker['tracker_name']?>" style="margin-top:10px;"></div>
                                    <div class="card-body">
                                        <button class="btn-deals"><?=$tracker['deal_type']?></button>
                                        <input type="button" class="btn-details" data-toggle="modal" data-target="#deal-modal-<?=$index?>" value="Details">
                                        <div class="modal fade" id="deal-modal-<?=$index?>" tabindex="-1" role="dialog" aria-labelledby="deal-exampleModalLabel-<?=$index?>" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deal-exampleModalLabel-<?=$index?>">Deal Details</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table>
                                                            <tr>
                                                                <td>Name</td>
                                                                <td><input type="text" value="<?=$tracker['tracker_name']?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Deal Type</td>
                                                                <td><input type="text" value="<?=$tracker['deal_type']?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Proposed Commission</td>
                                                                <td><input type="text" value="<?=$proposedCommission?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>AMS Commission</td>
                                                                <td><input type="text" value="<?=$tracker['ams_commission']?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Final Commission</td>
                                                                <td><input type="text" value="<?=$proposedCommission - $tracker['ams_commission']?>"></td>
                                                            </tr>
                                                            <?php
                                                            if($tracker['deal_type'] == 'HYBRID')
                                                            {
                                                                ?>
                                                                <tr>
                                                                    <td>Proposed Percentage</td>
                                                                    <td><input type="text" value="<?=$proposedPercentage?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>AMS Percentage</td>
                                                                    <td><input type="text" value="<?=$tracker['ams_commission_percentage_hybrid']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Final Percentage</td>
                                                                    <td><input type="text" value="<?=$proposedPercentage - $tracker['ams_commission_percentage_hybrid']?>"></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td>Website</td>
                                                                <td><input type="text" value="<?=$tracker['website']?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Your Offer (Commission)</td>
                                                                <td><input type="text" value="<?=isset($tracker['negotiations']['new_commission']) ? $tracker['negotiations']['new_commission'] : ''?>" name="my_offer"></td>
                                                            </tr>
                                                            <?php
                                                            if($tracker['deal_type'] == 'HYBRID')
                                                            {
                                                                ?>
                                                                <tr>
                                                                    <td>Your Offer (Percentage)</td>
                                                                    <td><input type="text" value="<?=isset($tracker['negotiations']['new_percentage_hybrid']) ? $tracker['negotiations']['new_percentage_hybrid'] : ''?>" name="my_offer_percentage"></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="submit" class="btn btn-secondary" value="Submit Your Proposal" name="proposal">
                                                        <input type="submit" class="btn btn-primary" value="Start Campaigning" name="campaigning">
                                                        <input type="hidden" name="tracker_id" value="<?=$tracker['tracker_id']?>">
                                                        <input type="hidden" name="prev_commission" value="<?=isset($tracker['negotiations']['new_commission']) ? $tracker['negotiations']['new_commission'] : $proposedCommission?>">
                                                        <input type="hidden" name="prev_percentage_hybrid" value="<?=isset($tracker['negotiations']['new_percentage_hybrid']) ? $tracker['negotiations']['new_percentage_hybrid'] : $proposedPercentage?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <?php

                            if($index % 4 == 3)
                            {
                                ?>
                                </div>
                                <?php
                            }
                        }
                        if($index % 4 != 3)
                        {
                            ?>
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                </div>
                <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='payments') ? 'show active': ''?>" id="pills-payment" role="tabpanel" aria-labelledby="pills-payment-tab">
                    <div class="card payment-tab">
                        <div class="container">
                            <h3>Payments</h3>
                        </div>
                        <table class="payment-table">
                            <thead>
                                <th aria-sort="ascending">Month</th>
                                <th>Amount</th>
                                <th>AMS Commission</th>
                                <th>Final Amount</th>
                                <th>Status</th>
                            </thead>
                            <?php foreach($payments as $transaction) : ?>
                            <tr>
                                <td><?= $transaction['month'] ?></td>
                                <td><?= $transaction['amount'] ?></td>
                                <td><?= $transaction['ams_commission'] ?></td>
                                <td><?= $transaction['final_amount'] ?></td>
                                <td><?= $transaction['status'] ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                    <div class="empty-bin"></div>
                </div>
                <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='promotions') ? 'show active': ''?>" id="pills-promotions" role="tabpanel"
                    aria-labelledby="pills-promotions-tab">
                    <div class="card deals-card">
                        <div class="container">
                            <h3>Campaigns Deals</h3>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='advertisers') ? 'show active': ''?>" id="pills-advertisers" role="tabpanel" aria-labelledby="pills-advertisers-tab">
                    <div class="container wrapper-container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card section-1">
                                    <?php
                                    if($advertisers_requests)
                                    {
                                        ?>
                                        <table class="payout-table mx-3 my-3">
                                          <thead>
                                            <th>Advertiser Id</th>
                                            <th>Name</th>
                                            <th>Deals</th>
                                          </thead>
                                        <?php
                                        foreach($advertisers_requests as $request)
                                        {
                                            ?>
                                            <tr>
                                              <td><?=$request['affiliate_id']?></td>
                                              <td><?=$request['firstname'] . ' ' . $request['lastname']?></td>
                                              <td>
                                                  <a href="/affiliate?page=deals&adv_id=<?=$request['affiliate_id']?>">Click Here</a>
                                              </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </table>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='advertisers-deal') ? 'show active': ''?>" id="pills-advertisers-deal" role="tabpanel" aria-labelledby="pills-advertisers-tab">
                    <div class="container wrapper-container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card section-1">
                                    <?php
                                    if($deals_requests)
                                    {
                                        ?>
                                        <table class="payout-table mx-3 my-3">
                                          <thead>
                                            <th>Tracker Id</th>
                                            <th>Advertiser Id</th>
                                            <th>Name</th>
                                            <th>Deals</th>
                                          </thead>
                                        <?php
                                        foreach($deals_requests as $request)
                                        {
                                            ?>
                                            <tr>
                                                <td><?=$request['tracker_id']?></td>
                                              <td><?=$request['affiliate_id']?></td>
                                              <td><?=$request['firstname'] . ' ' . $request['lastname']?></td>
                                              <td>
                                                  <a href="/affiliate?page=deals&adv_id=<?=$request['affiliate_id']?>&d_id=<?=$request['tracker_id']?>">Click Here</a>
                                              </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </table>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='profile') ? 'show active': ''?>" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="card deals-card">
                        <div class="container">
                            <h3>Profile</h3>
                        </div>
                    </div>
                    <div class="container">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#account_info"
                                    role="tab" aria-controls="home" aria-selected="true">Account Info</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#contact_info"
                                    role="tab" aria-controls="profile" aria-selected="false">Contact Info</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#account_number"
                                    role="tab" aria-controls="contact" aria-selected="false">Account Number</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#payment_info"
                                    role="tab" aria-controls="contact" aria-selected="false">Payment Info</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="account_info" role="tabpanel"
                                aria-labelledby="home-tab">
                                <div class="card">

                                    <table class="user-info-1">
                                        <tr>
                                            <td> User Name
                                                <p class="small_text">You cant change user name</p>
                                            </td>
                                            <td><?= $userData['alias'];?></td>
                                        </tr>
                                        <tr>
                                            <td> Display Name
                                            </td>
                                            <td><input type="text" class="display_name" id="display_name" value ="<?= $userData['alias'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Password
                                                <p class="small_text">Leave it blank if you dont want to change</p>
                                            </td>
                                            <td><input type="password" class="password" id="password"></td>
                                        </tr>
                                        <tr>
                                            <td> Receive newsletter per email?
                                            </td>
                                            <td><label class="switch">
                                                    <input type="checkbox" value="vat_num" checked>
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td><button> Update</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact_info" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="card">
                                    <table class="user-info-1">
                                        <tr>
                                            <td> First Nme
                                            </td>
                                            <td><input type="text" class="first_name" id="first_name" value="<?= $userData['firstname'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Last Name
                                            </td>
                                            <td><input type="text" class="last_name" id="last_name" value="<?= $userData['lastname'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Date of Birth
                                            </td>
                                            <td>
                                                <div class="row" style="margin-left: 0em;">

                                                    <input type="date" class="date" id="date" name="date_of_birth"
                                                       value="<?php echo date('Y-m-d',strtotime($userData['date_of_birth'])); ?>">
                                                    <!-- <input type="number" class="month" id="month" name="quantity"
                                                        min="1" max="12">
                                                    <input type="number" class="year" id="year" name="quantity"
                                                        max="2019"> -->
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> Phone Number
                                            </td>
                                            <td><input type="Number" class="phone_number" id="phone_number" value="<?= $userData['phone'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Email
                                            </td>
                                            <td><input type="email" class="email" id="email" value="<?= $userData['email'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Skype
                                            </td>
                                            <td><input type="skype_mail" class="skype_mail" id="skype_mail" value="<?= $userData['email'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td><button> Update</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="account_number" role="tabpanel"
                                aria-labelledby="contact-tab">
                                <div class="card">
                                    <table class="user-info-1">
                                        <tr>
                                            <td> Company Name / Account Owner
                                            </td>
                                            <td><input type="text" class="company_name" id="company_name" value="<?= $userData['company'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Address
                                            </td>
                                            <td><input type="text" class="address" id="address" value="<?= $userData['address'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Postcode
                                            </td>
                                            <td><input type="text" class="postcode" id="postcode" value="<?= $userData['zip'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>City
                                            </td>
                                            <td><input type="text" class="city" id="city" value="<?= $userData['city'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Country
                                            </td>
                                            <td><input type="text" class="country" id="country" value="<?= $userData['country'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td><button> Update</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="payment_info" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="container payment-info"><span class="general">General</span>
                                    <div class="card">
                                        <table class="user-info-1">
                                            <tr>
                                                <td> Payment Method
                                                </td>
                                                <td>
                                                    <div class="dropdown payment-dropdown">
                                                        <button class="btn btn-light dropdown-toggle" type="button"
                                                            id="dropdownMenuButton" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                            Please select payment method
                                                        </button>
                                                        <div class="dropdown-menu"
                                                            aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="#">Please select payment
                                                                method</a>
                                                            <a class="dropdown-item" href="#">Bankwire</a>
                                                            <a class="dropdown-item" href="#">Skrill/
                                                                Moneybookers</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Tax Registered
                                                </td>
                                                <td><input type="text" class="tax_num" id="tax_num"></td>
                                            </tr>
                                            <tr>
                                                <td> Vat Number
                                                </td>
                                                <td><label class="switch">
                                                        <input type="checkbox" value="vat_num" checked>
                                                        <span class="slider round"></span>
                                                    </label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td><button> Update</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    &copy; CopyRight Blockmatrix
</footer>

<!-- scripts section starts here -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="<?php echo base_url('sitehome-assets/home/home.js'); ?>" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.jqueryui.min.js"></script>

</body>
<script>

    $('.stats-brerakdown a').click(function(){
        $('#selected').text($(this).text());
    });

    $('.mothstats a').click(function(){
        $('#monthstats').text($(this).text());
    });

monthyClicks();
affiliates();

function affiliates() {
    var clicksGraphData = JSON.parse(`<?php echo $lindings_grpah; ?>`);
    drawGraphOne(clicksGraphData, 'Clicks');
}

function drawGraphOne(gData, gName){
    var ctx = document.getElementById('myAffiliates').getContext('2d');
    this.myAffiliates = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: gData.label,
            datasets: [{
                label: gName,
                data: gData.data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            events: []
        }
    });

}



function newAffiliates() {

    var ctx = document.getElementById('myAffiliates').getContext('2d');
    this.myAffiliates = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['March', 'April', 'May', 'June', 'July', 'August'],
        datasets: [{
            label: ' of New Affiliates',
            data: [1,2, 3, 4, 4, 6],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        events: []
    }
});



}

function changeAdvertiser(page, advId){
    window.location.href="/affiliate?page=" + page + "&adv_id=" + advId;
}

function signUps() {
    var singupGraphData = JSON.parse(`<?php echo $signup_grpah; ?>`);
    drawGraphOne(singupGraphData, 'SignUps');
}

function ftds() {
    var depositsGraphData = JSON.parse(`<?php echo $deposits_grpah; ?>`);
    drawGraphOne(depositsGraphData, 'Deposits');
}

function monthyClicks() {
    var monthlyClicksGraphData = JSON.parse(`<?php echo $lendings_monthly_grpah; ?>`);
    drawMonthlyGraph(monthlyClicksGraphData, 'Monthly Clicks');
}

function monthlySignups() {
    var monthlySignupsGraphData = JSON.parse(`<?php echo $signup_monthly_grpah; ?>`);
    drawMonthlyGraph(monthlySignupsGraphData, 'Monthly Sign Ups');

}

function monthlyDeposits() {
    var monthlyDepositsGraphData = JSON.parse(`<?php echo $deposit_monthly_grpah; ?>`);
    drawMonthlyGraph(monthlyDepositsGraphData, 'Monthly Deposits');


}

function drawMonthlyGraph(gData, gName){
    var ctx = document.getElementById('mySales').getContext('2d');
    var mySales = new Chart(ctx, {
    type: 'bar',
    data: {
            labels: gData.label,
            datasets: [{
                label: gName,
                data: gData.data,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            events: []
        }
    });
}
$(document).ready(function(){
    $('#example').DataTable({paging: false, info: false, sorting: false});

    $("#from-datepicker").datepicker({dateFormat:'yy-mm-dd',  maxDate: 0});
    $("#to-datepickerto").datepicker({dateFormat:'yy-mm-dd',  maxDate: 0});
    $("#example-new").css('display', "none");



   $("#stats_period_view").click(function(){
    var fromDate = new Date($('#from-datepicker').val());
    var toDate = new Date($('#to-datepickerto').val());
    var CurrentDate = new Date();

    if(fromDate < toDate && toDate < CurrentDate){

        // $.ajax({
        //     post: "",
        //     data: {from_date: fromDate, to_date: toDate},
        //     success: function(datat){
        //         console.log(datat, "...here you go");
        //     }
        // });

        $.ajax({
         type: "POST",
         url: "getstatsbytimeintervel",
         data: {from_date: $("#from-datepicker").val(), to_date: $('#to-datepickerto').val()},
         dataType: "json",
         cache:false,
         success:
              function(data){
                  var objdata = data;
                  var advertisers = objdata.advertisers;
                  //var advertisers =  JSON.stringify(objdata.advertisers);
                  $("#time-clicks").html(objdata.overview_stats.totalClicks);
                  $("#time-singups").html(objdata.overview_stats.totalSignups);
                  $("#time-deposits").html(objdata.overview_stats.totalDeposits);

                //   advertisers.foreach((advt)=>{
                //      console.log(advt);
                //  })
                console.log(advertisers);
                //$("#example").css("display", "none");
                $("#example").css("display", "none");

                var data = [
                            {
                                "affiliate_id": "6",
                                "alias": "Karan",
                                "signup": "0",
                                "lending": "0",
                                "deposit": "0"
                            },
                            {
                                "affiliate_id": "7",
                                "alias": "Amer",
                                "signup": "0",
                                "lending": "0",
                                "deposit": "0"
                            },
                            {
                                "affiliate_id": "8",
                                "alias": "Sharuk",
                                "signup": "0",
                                "lending": "0",
                                "deposit": "0"
                            },
                            {
                                "affiliate_id": "9",
                                "alias": "Adi",
                                "signup": "0",
                                "lending": "0",
                                "deposit": "0"
                            }
                        ];

                console.log(typeof data,"====",typeof advertisers );
                $('#example').DataTable().destroy();
                 $('#myTable').empty();
                 $("#example-new").css('display', "block");
                $('#example-new').DataTable( {
                    destroy: true,
                    data: advertisers,
                    columns: [

                                { data: 'alias' },
                                { data: 'signup' },
                                { data: 'lending' },
                                { data: 'deposit' }
                            ],
                    searching: false,
                    paging: false,
                    info: false
                } );


               // var dataTable = $('').dataTable();


               // advertisers.forEach(myFunction);

                //alert(data);  //as a debugging message.
              }

        });

    }else{
        alert(' To date is not greater than the current date.');
    }

  });
  function myFunction(item, index) {
      $("#time-advertisers").html("<tr><td></td><td></td><td></td><td></td><td></td></tr>")
  console.log(item, index);
}
  function convertDate(inputFormat) {
        function pad(s) { return (s < 10) ? '0' + s : s; }
        var d = new Date(inputFormat);
        return [pad(d.getMonth()+1), pad(d.getDate()), d.getFullYear()].join('-');
    }
});
</script>
</html>
