<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $page_title; ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('sitehome-assets/home/home.css'); ?>">
</head>
<body>
<div class="wrapper">
    <!-- Header Starts here -->
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="logo"></div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
            <div class="notification">
                <div class="row notification-row">
                    <p class="notif-name"> Hi, <b> <?=$userData['firstname'] . " " . $userData['lastname']?> </b></span></p>
                    <span class="notif"><i class="fa fa-bell" aria-hidden="true"></i></span>
                </div>
                <div class="row month">
                    Your <span class="font-weight-bold mx-1"> <?php echo date("F");?> </span> month stats
                </div>
                <div class="row">
                    <ul class="stats-list">
                        <li>Clicks <br> <?= $home_overview['clicks']; ?></li>
                        <li>Sign Ups <br> <?= $home_overview['signups']; ?></li>
                        <li>FTD <br> <?= $home_overview['fist_time_deposit']; ?></li>
                        <li>CPA <br><?= $home_overview['cost_per_action']; ?></li>
                        <li>Profit <br> &dollar;<?= $home_overview['profit_per_day']; ?></li>
                        <!-- <li><button class="btn">Details</button></li> -->
                    </ul>
                </div>
            </div>
        </div>
            <!-- Header Ends here -->
    </div>
    <div class="container-fluid">
        <div class="row nav-pills-row">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                        aria-controls="pills-home" aria-selected="true">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-stats-tab" data-toggle="pill" href="#pills-stats" role="tab"
                        aria-controls="pills-stats" aria-selected="false">Stats</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-campaign-tab" data-toggle="pill" href="#pills-campaign" role="tab"
                        aria-controls="pills-campaign" aria-selected="false">Campaigns</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-deals-tab" data-toggle="pill" href="#pills-deals" role="tab"
                        aria-controls="pills-deals" aria-selected="false">Deals</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-payment-tab" data-toggle="pill" href="#pills-payment" role="tab"
                        aria-controls="pills-payment" aria-selected="false">Payments</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-promotions-tab" data-toggle="pill" href="#pills-promotions"
                        role="tab" aria-controls="pills-promotions" aria-selected="false">Promotions</a>
                </li>
                <li class="nav-item float-lg-right float-right profile-btn">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
                        aria-controls="pills-profile" aria-selected="false">Profile</a>
                </li>
                <li class="nav-item float-lg-right float-right logout-btn">
                    <a class="nav-link"  href="/auth/logout"
                        >Logout</a>
                </li>
            </ul>
        </div>
        <!-- Navbar Ends here -->
        <!-- Page Content begins here -->
        <div class="container-fluid tab-content-container">
            <div class="tab-content" id="pills-tabContent">
                <!-- Home Tab content begins herre -->
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                    aria-labelledby="pills-home-tab">
                    <div class="container">
                        <div class="row stats-row">
                            <div class="col-md-2"><?= $home_overview['clicks']; ?>
                                <div class="box">Clicks</div>
                                <img class="tap" src="<?php echo base_url('sitehome-assets/assets/icons/tap.svg'); ?>" height="25" width="25" alt="Tap">
                            </div>
                            <div class="col-md-2"><?= $home_overview['signups']; ?>
                                <div class="box">Signups</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/list.svg'); ?>" height="25" width="25" alt="List">
                            </div>
                            <div class="col-md-2"><?= $home_overview['fist_time_deposit']; ?>
                                <div class="box">FTD</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/credit-card.svg'); ?>" height="25" width="25"
                                    alt="FTD">
                            </div>
                            <div class="col-md-2"><?= $home_overview['cost_per_action']; ?>
                                <div class="box">CPA</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/visiting-card.svg'); ?>" height="25" width="25"
                                    alt="CPA">
                            </div>
                            <div class="col-md-2"><?= $home_overview['signup_to_deposit']; ?>
                                <div class="box">Signup to deposit</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/double-angle-pointing-to-right.svg'); ?>"
                                    height="20" width="20" alt="Right">
                            </div>
                            <div class="col-md-2 profit"> &euro; <?= $home_overview['profit_per_day']; ?><br>
                                <span class="span-class"> Avg per day</span>
                                <div class="box">Profit</div>
                                <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/funds.svg'); ?>" height="25" width="25"
                                    alt="Cash">
                            </div>
                        </div>
                    </div>
                    <div class="container wrapper-container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card section-1">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/images/stats.png'); ?>" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Recent Earning</span>
                                        <button class="btn btn-default show_loss">Show Losses</button>
                                    </div>
                                </div>
                                <div class="card section-2">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/images/pie-chart.png'); ?>" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Statistics Breakdown</span>
                                        <div class="dropdown">
                                            <button class="btn btn-light dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                Clicks
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#">Signups</a>
                                                <a class="dropdown-item" href="#">FTD</a>
                                                <a class="dropdown-item" href="#">Profits</a>
                                            </div>
                                        </div>
                                        <div class="dropdown">
                                            <button class="btn btn-light dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                Months
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#">August</a>
                                                <a class="dropdown-item" href="#">July</a>
                                                <a class="dropdown-item" href="#">June</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card section-3">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/images/bar-graph.jpg'); ?>" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Past Month Stats</span>
                                        <div class="dropdown">
                                            <button class="btn btn-light dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                Profit
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="#">Profit</a>
                                                <a class="dropdown-item" href="#">Clicks</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card section-4">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/images/bar-graph.jpg'); ?>" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img">This Months Competition</span>
                                        <div class="container">
                                            <img class="img-fluid competition" src="<?php echo base_url('sitehome-assets/assets/images/mrgreen.jpg'); ?>"
                                                alt="Competions">
                                            <button class="btn btn-default btn-md competition-container">See Recent
                                                Competitions</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="leaderboard">
                                    <img class="img-fluid competition" src="<?php echo base_url('sitehome-assets/assets/images/mrgreen.jpg'); ?>"
                                        alt="Leader Board">
                                    <div class="row leaders">
                                        <ul class="leader-board">
                                            <li class="gold">
                                                <div class="row"><img src="<?php echo base_url('sitehome-assets/assets/images/gold-medal.png'); ?>"
                                                        height="20" width="20" alt="1st Prize"><span
                                                        class="leader-text"><b>1</b> FTD</span></div>
                                            </li>
                                            <li class="silver">
                                                <div class="row"><img src="<?php echo base_url('sitehome-assets/assets/images/silver-medal.png'); ?>"
                                                        height="20" width="20" alt="1st Prize"><span
                                                        class="leader-text"><b>2</b> FTD</span></div>
                                            </li>
                                            <li class="bronze">
                                                <div class="row"><img src="<?php echo base_url('sitehome-assets/assets/images/bronze-medal.png'); ?>"
                                                        height="20" width="20" alt="1st Prize"><span
                                                        class="leader-text"><b>3</b> FTD</span></div>
                                            </li>
                                            <li class="your-place">Your Place</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="bannerboard">
                                    <img class="banner-board img-fluid" src="<?php echo base_url('sitehome-assets/assets/images/banner-2.jpg'); ?>"
                                        alt="banner board">
                                </div>
                                <div class="advboard">
                                    <img class="banner-board img-fluid" src="<?php echo base_url('sitehome-assets/assets/images/ads-1.jpg'); ?>"
                                        alt="banner board">
                                </div>
                                <div class="prizeboard">
                                    <img class="banner-board img-fluid" src="<?php echo base_url('sitehome-assets/assets/images/ads2.jpg'); ?>"
                                        alt="banner board">
                                </div>
                                <div class="card managerboard">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/icons/operator.jpg'); ?>" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Your Affiliate Managers</span>
                                    </div>
                                    <p class="manager-name"><b><?=$affiliate_manager['first_name'].' '.$affiliate_manager['last_name'] ?></b></p>
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/icons/skype.png'); ?>" height="30"
                                            width="30" alt="Stats">
                                        <span class="text-manager"><?= $affiliate_manager['skyp_id']?></span>
                                    </div>
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets/assets/icons/mail.png'); ?>" height="30"
                                            width="30" alt="Stats">
                                        <span class="text-manager"><?= $affiliate_manager['email']?></span>
                                    </div>
                                </div>

                                <div class="card payment-card">
                                    <div class="row">
                                        <img class="section_img" src="<?php echo base_url('sitehome-assets'); ?>/assets/icons/images.png" height="40"
                                            width="40" alt="Stats">
                                        <span class="text-img"> Latest Payments</span>
                                        <button class="payment-btn btn-sm">All Payments</button>
                                    </div>

                                    <div class="row">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-stats" role="tabpanel" aria-labelledby="pills-stats-tab">
                    <div class="container statistic-container">
                        <div class="card time-card">
                            <div class="row">
                                <div class="col-lg-6 calendar-container">
                                    <b>Time Period</b>
                                    <div class="row">
                                        <input type="date" class="date hasDatepicker" name="today_date"
                                            id="today_date" max="23-08-2019">
                                        <input type="date" name="previous_date" id="previous_date" min="23-05-2019">
                                        <button class="btn btn-success">View</button>
                                    </div>
                                </div>
                                <div class="col-lg-6 quick_dates">
                                    <b>Quick Months</b>
                                    <div class="row">
                                        <button class="months">Month 1</button>
                                        <button class="months">Month 2</button>
                                        <button class="months">Month 3</button>
                                        <button class="months">Month 4</button>
                                        <button class="months">Month 5</button>
                                    </div>
                                    <div class="row time-period">
                                        <button class="months">Today</button>
                                        <button class="months">Yesterday</button>
                                        <button class="months">This Year</button>
                                        <button class="months">Last Year</button>
                                        <button class="months">All Time</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- New container -->

                        <div class="container">
                            <div class="row stats-row">
                                <div class="col-md-2"><?= $home_overview['clicks']; ?>
                                    <div class="box">Clicks</div>
                                    <img class="tap" src="<?php echo base_url('sitehome-assets/assets/icons/tap.svg'); ?>" height="25" width="25" alt="Tap">
                                </div>
                                <div class="col-md-2"><?= $home_overview['signups']; ?>
                                    <div class="box">Signups</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/list.svg'); ?>" height="25" width="25"
                                        alt="List">
                                </div>
                                <div class="col-md-2"><?= $home_overview['fist_time_deposit']; ?>
                                    <div class="box">FTD</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/credit-card.svg'); ?>" height="25" width="25"
                                        alt="FTD">
                                </div>
                                <div class="col-md-2"><?= $home_overview['cost_per_action']; ?>
                                    <div class="box">CPA</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets'); ?>/assets/icons/visiting-card.svg" height="25"
                                        width="25" alt="CPA">
                                </div>
                                <div class="col-md-2"><?= $home_overview['signup_to_deposit']; ?>
                                    <div class="box">Signup to deposit</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets/assets/icons/double-angle-pointing-to-right.svg'); ?>"
                                        height="20" width="20" alt="Right">
                                </div>
                                <div class="col-md-2 profit"> &euro; <?= $home_overview['profit_per_day']; ?><br>
                                    <span class="span-class">&euro; Avg per day</span>
                                    <div class="box">Profit</div>
                                    <img class="icons" src="<?php echo base_url('sitehome-assets'); ?>/assets/icons/funds.svg" height="25" width="25"
                                        alt="Cash">
                                </div>
                            </div>
                        </div>

                        <!-- Table section -->
                        <div class="card table-card">
                            <div class="search">
                                <div class="row">
                                    <label for="Search"> Search: <input type="text" table="table_name"></label>
                                </div>
                            </div>

                            <table class="payout-table">
                                <thead>
                                    <th aria-sort="ascending">Advertiser</th>
                                    <th>Clicks</th>
                                    <th>Signups</th>
                                    <th>FTD</th>
                                    <th>Rev payout</th>
                                    <th>CPA</th>
                                    <th>CPA payout</th>
                                    <th>CPL payout</th>
                                    <th>Total payout</th>
                                </thead>
                                <?php foreach($advertisers as $advertise) : ?>
                                <tr>
                                    <td><?= $advertise['name'] ?></td>
                                    <td><?= $advertise['clicks'] ?></td>
                                    <td><?= $advertise['signups'] ?></td>
                                    <td><?= $advertise['first_time_deposit'] ?></td>
                                    <td><?= $advertise['rev_payout'] ?></td>
                                    <td><?= $advertise['cost_per_action'] ?></td>
                                    <td><?= $advertise['cpa_payout'] ?></td>
                                    <td><?= $advertise['cpl_payout'] ?></td>
                                    <td><?= $advertise['total_payout'] ?></td>
                                    
                                </tr>
                                <?php endforeach; ?>
                            </table>

                            <!-- <table class="payout-table">
                                <thead>
                                    <th aria-sort="ascending">Advertiser</th>
                                    <th>Clicks</th>
                                    <th>Signups</th>
                                    <th>FTD</th>
                                    <th>Rev payout</th>
                                    <th>CPA</th>
                                    <th>CPA payout</th>
                                    <th>CPL payout</th>
                                    <th>Total payout</th>
                                </thead>
                                <tr>
                                    <td>Total</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                            </table> -->
                        </div>
                    </div>
                </div>
                <!-- Statistic COntainer Ends here -->
                <!-- Campaign container begins here -->
                <div class="tab-pane fade" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">
                    <div class="card campaign-card">
                        <div class="container">
                            <h3>Campaigns</h3>
                        </div>
                    </div>
                </div>
                <!-- Campaign container ends here -->
                <!-- Deal container begins here -->
                <div class="tab-pane fade" id="pills-deals" role="tabpanel" aria-labelledby="pills-deals-tab">
                    <div class="card deals-card">
                        <div class="container">
                            <h3>Campaigns Deals</h3>
                            <div class="row">
                                <label class="deal-type" for="deal types"> <b>Deal Types</b></label>
                                <div class="radio-class">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-outline-dark active">
                                            <input type="radio" name="options" id="option1" autocomplete="off"
                                                checked>
                                            All Deals
                                        </label>
                                        <label class="btn btn-outline-dark">
                                            <input type="radio" name="options" id="option2" autocomplete="off">
                                            Revshare
                                        </label>
                                        <label class="btn btn-outline-dark">
                                            <input type="radio" name="options" id="option3" autocomplete="off"> CPA
                                        </label>
                                        <label class="btn btn-outline-dark">
                                            <input type="radio" name="options" id="option3" autocomplete="off"> CPL
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="deals-search">
                                <div class="row">
                                    <label class="label-deal" for="Search"><b> Search:</b> <input type="text"
                                            placeholder="Search Deals" table="capaign_deals"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container deal-container">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="card deal-card">
                                    <img src="<?php echo base_url('sitehome-assets/assets/images/Hajper.png'); ?>" height="120" alt="Hajper">
                                    <div class="card-body deal-btn">
                                        <button class="btn-deals">10% Revshare</button>
                                        <button class="btn-details"> Details</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="card deal-card">
                                    <img src="<?php echo base_url('sitehome-assets/assets/images/TurboVegas.png'); ?>" height="120" alt="Turbo Vegas">
                                    <div class="card-body deal-btn">
                                        <button class="btn-deals">10% Revshare</button>
                                        <button class="btn-details"> Details</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="card deal-card">
                                    <img src="<?php echo base_url('sitehome-assets/assets/images/Speedy Casino.png'); ?>" height="120" alt="Speedy Casino">
                                    <div class="card-body deal-btn">
                                        <button class="btn-deals">10% Revshare</button>
                                        <button class="btn-details"> Details</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="card deal-card">
                                    <img src="<?php echo base_url('sitehome-assets/assets/images/Hyper Casino.png'); ?>" height="120" alt="Hyper Casino">
                                    <div class="card-body deal-btn">
                                        <button class="btn-deals">10% Revshare</button>
                                        <button class="btn-details"> Details</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-payment" role="tabpanel" aria-labelledby="pills-payment-tab">
                    <div class="card payment-tab">
                        <div class="container">
                            <h3>Payments</h3>
                        </div>
                        <table class="payment-table">
                            <thead>
                                <th aria-sort="ascending">Period</th>
                                <th>Created</th>
                                <th>Amount</th>
                                <th>Status</th>
                            </thead>
                            <?php foreach($payments as $transaction) : ?>
                            <tr>
                                <td><?= $transaction['period'] ?></td>
                                <td><?= $transaction['created'] ?></td>
                                <td><?= $transaction['Amount'] ?></td>
                                <td><?= $transaction['status'] ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                    <div class="empty-bin"></div>
                </div>
                <div class="tab-pane fade" id="pills-promotions" role="tabpanel"
                    aria-labelledby="pills-promotions-tab">
                    <div class="card deals-card">
                        <div class="container">
                            <h3>Campaigns Deals</h3>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="card deals-card">
                        <div class="container">
                            <h3>Profile</h3>
                        </div>
                    </div>
                    <div class="container">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#account_info"
                                    role="tab" aria-controls="home" aria-selected="true">Account Info</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#contact_info"
                                    role="tab" aria-controls="profile" aria-selected="false">Contact Info</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#account_number"
                                    role="tab" aria-controls="contact" aria-selected="false">Account Number</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#payment_info"
                                    role="tab" aria-controls="contact" aria-selected="false">Payment Info</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="account_info" role="tabpanel"
                                aria-labelledby="home-tab">
                                <div class="card">
                               
                                    <table class="user-info-1">
                                        <tr>
                                            <td> User Name
                                                <p class="small_text">You cant change user name</p>
                                            </td>
                                            <td><?= $userData['alias'];?></td>
                                        </tr>
                                        <tr>
                                            <td> Display Name
                                            </td>
                                            <td><input type="text" class="display_name" id="display_name" value ="<?= $userData['displayname'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Password
                                                <p class="small_text">Leave it blank if you dont want to change</p>
                                            </td>
                                            <td><input type="password" class="password" id="password"></td>
                                        </tr>
                                        <tr>
                                            <td> Receive newsletter per email?
                                            </td>
                                            <td><label class="switch">
                                                    <input type="checkbox" value="vat_num" checked>
                                                    <span class="slider round"></span>
                                                </label></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td><button> Update</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact_info" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="card">
                                    <table class="user-info-1">
                                        <tr>
                                            <td> First Nme
                                            </td>
                                            <td><input type="text" class="first_name" id="first_name" value="<?= $userData['firstname'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Last Name
                                            </td>
                                            <td><input type="text" class="last_name" id="last_name" value="<?= $userData['lastname'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Date of Birth
                                            </td>
                                            <td>
                                                <div class="row" style="margin-left: 0em;">
                                               
                                                    <input type="date" class="date" id="date" name="date_of_birth"
                                                       value="<?php echo date('Y-m-d',strtotime($userData['date_of_birth'])); ?>">
                                                    <!-- <input type="number" class="month" id="month" name="quantity"
                                                        min="1" max="12">
                                                    <input type="number" class="year" id="year" name="quantity"
                                                        max="2019"> -->
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> Phone Number
                                            </td>
                                            <td><input type="Number" class="phone_number" id="phone_number" value="<?= $userData['phone'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Email
                                            </td>
                                            <td><input type="email" class="email" id="email" value="<?= $userData['email'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Skype
                                            </td>
                                            <td><input type="skype_mail" class="skype_mail" id="skype_mail" value="<?= $userData['email'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td><button> Update</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="account_number" role="tabpanel"
                                aria-labelledby="contact-tab">
                                <div class="card">
                                    <table class="user-info-1">
                                        <tr>
                                            <td> Company Name / Account Owner
                                            </td>
                                            <td><input type="text" class="company_name" id="company_name" value="<?= $userData['company'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Address
                                            </td>
                                            <td><input type="text" class="address" id="address" value="<?= $userData['address'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td> Postcode
                                            </td>
                                            <td><input type="text" class="postcode" id="postcode" value="<?= $userData['zip'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>City
                                            </td>
                                            <td><input type="text" class="city" id="city" value="<?= $userData['city'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Country
                                            </td>
                                            <td><input type="text" class="country" id="country" value="<?= $userData['country'];?>"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td><button> Update</button></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="payment_info" role="tabpanel"
                                aria-labelledby="profile-tab">
                                <div class="container payment-info"><span class="general">General</span>
                                    <div class="card">
                                        <table class="user-info-1">
                                            <tr>
                                                <td> Payment Method
                                                </td>
                                                <td>
                                                    <div class="dropdown payment-dropdown">
                                                        <button class="btn btn-light dropdown-toggle" type="button"
                                                            id="dropdownMenuButton" data-toggle="dropdown"
                                                            aria-haspopup="true" aria-expanded="false">
                                                            Please select payment method
                                                        </button>
                                                        <div class="dropdown-menu"
                                                            aria-labelledby="dropdownMenuButton">
                                                            <a class="dropdown-item" href="#">Please select payment
                                                                method</a>
                                                            <a class="dropdown-item" href="#">Bankwire</a>
                                                            <a class="dropdown-item" href="#">Skrill/
                                                                Moneybookers</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> Tax Registered
                                                </td>
                                                <td><input type="text" class="tax_num" id="tax_num"></td>
                                            </tr>
                                            <tr>
                                                <td> Vat Number
                                                </td>
                                                <td><label class="switch">
                                                        <input type="checkbox" value="vat_num" checked>
                                                        <span class="slider round"></span>
                                                    </label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td><button> Update</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    &copy; CopyRight Blockmatrix
</footer>

<!-- scripts section starts here -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="<?php echo base_url('sitehome-assets/home/home.js'); ?>" type="text/javascript"></script>
</body>
</html>