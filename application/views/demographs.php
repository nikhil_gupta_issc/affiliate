<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
        crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" 
        rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" 
        crossorigin="anonymous">
    <title>Home Page</title>
    <link rel="stylesheet" href="/sitehome-assets/home/demographs.css">
  </head>
  <body>
   <header>
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <div class="logo"></div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="notification p-1">
                    <div class="notification-row">
                        <p class="notif-name"> Hi, <b> {{company.name}} </b>
                            <span class="notif pull-right"><i class="fa fa-bell" aria-hidden="true">
                            </i></span></p>
                    </div>
                    <div class="month">
                        Your {{month}} stats
                    </div>
                    <div class="pt-2">
                        <ul class="stats-list ml-2">
                            <li>Clicks <br> 0</li>
                            <li>Sign Ups <br> 0</li>
                            <li>FTD <br> 0</li>
                            <li>CPA <br> 0</li>
                            <li>Profit <br> &dollar; 0</li>
                            <li><a href="#" class="d-inline pull-right mr-auto"><button class="btn">Details</button></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
   </header>

   <main>
       <div class="container" id="navbar">
            <div class="row nav-pills-row">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" 
                        href="#pills-home" role="tab"
                            aria-controls="pills-home" aria-selected="true">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-stats-tab" data-toggle="pill" href="#pills-stats" role="tab"
                            aria-controls="pills-stats" aria-selected="false">Stats</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-campaign-tab" data-toggle="pill" href="#pills-campaign" role="tab"
                            aria-controls="pills-campaign" aria-selected="false">Campaigns</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-deals-tab" data-toggle="pill" href="#pills-deals" role="tab"
                            aria-controls="pills-deals" aria-selected="false">Deals</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-payment-tab" data-toggle="pill" href="#pills-payment" role="tab"
                            aria-controls="pills-payment" aria-selected="false">Payments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-promotions-tab" data-toggle="pill" href="#pills-promotions"
                            role="tab" aria-controls="pills-promotions" aria-selected="false">Promotions</a>
                    </li>
                    <li class="nav-item float-lg-right float-right profile-btn">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item float-lg-right float-right logout-btn">
                        <a class="nav-link" id="pills-logout-tab" data-toggle="pill" href="#pills-logout" role="tab"
                            aria-controls="pills-logout" onClick="" aria-selected="false">Logout</a>
                    </li>
                </ul>
            </div>
       </div>
       <div id="alert-welcome" class="container">
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>We need some information from you</strong><br>
            Before we can payout your earnings you need to fill in:<br>
            - Account owner information<br>
            - Payment Method<br>
            You can enter your information in your profile<br>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
       </div>
       <div class="container">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                    aria-labelledby="pills-home-tab">
                    <div class="container">
                        <div class="row stats-row">
                            <div class="col-md-2">0
                                <div class="box">Clicks</div>
                                <img class="tap" src="../assets/icons/tap.svg" height="25" width="25" alt="Tap">
                            </div>
                            <div class="col-md-2">0
                                <div class="box">Signups</div>
                                <img class="icons" src="../assets/icons/list.svg" height="25" width="25" alt="List">
                            </div>
                            <div class="col-md-2">0
                                <div class="box">FTD</div>
                                <img class="icons" src="../assets/icons/credit-card.svg" height="25" width="25"
                                    alt="FTD">
                            </div>
                            <div class="col-md-2">0
                                <div class="box">CPA</div>
                                <img class="icons" src="../assets/icons/visiting-card.svg" height="25" width="25"
                                    alt="CPA">
                            </div>
                            <div class="col-md-2">0%
                                <div class="box">Signup to deposit</div>
                                <img class="icons" src="../assets/icons/double-angle-pointing-to-right.svg"
                                    height="20" width="20" alt="Right">
                            </div>
                            <div class="col-md-2 profit"> &euro; 0<br>
                                <span class="span-class">&euro; Avg per day</span>
                                <div class="box">Profit</div>
                                <img class="icons" src="../assets/icons/funds.svg" height="25" width="25"
                                    alt="Cash">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container bg-light py-2 px-1 mt-2">
                <div class="container py-2 px-1 mt-2">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Impressions</h5>
                                </div>
                                <div class="card-body">
                                    <canvas id="LineChartOne"></canvas>
                                </div>
                                <div class="footer"></div>  
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Clicks</h5>
                                </div>
                                <div class="card-body">
                                    <canvas id="LineChartTwo"></canvas>
                                </div>
                                <div class="footer"></div>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container py-2 px-1 mt-2">
                    <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Actions</h5>
                                    </div>
                                    <div class="card-body">
                                        <canvas id="LineChartThree"></canvas>
                                    </div>
                                    <div class="footer"></div>  
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Top Campagins</h5>
                                    </div>
                                    <div class="card-body">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Campagin Name</th>
                                                    <th>Total Sep</th>
                                                    <th>CPC</th>
                                                    <th>CPM</th>
                                                    <th>CPA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Facebook Add Campagin</td>
                                                    <td></td>
                                                    <td>$1,41</td>
                                                    <td>$1,41</td>
                                                    <td>$1,41</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                       
                                    </div>
                                    <div class="footer"></div>  
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
       </div>
   </main>

   <footer></footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" 
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" 
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" 
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" 
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
    <script>
        var impsGraphData = JSON.parse(`<?php echo $impressions_graph; ?>`);
        console.log(impsGraphData.dataset_two);

        var clicksGraphData = JSON.parse(`<?php echo $clicks_graph; ?>`);
        console.log(impsGraphData.dataset_two);

        var actionsGraphData = JSON.parse(`<?php echo $actions_graph; ?>`);
        console.log(impsGraphData.dataset_two);

        var ctx_one = document.getElementById('LineChartOne').getContext('2d');
        var ctx_two = document.getElementById('LineChartTwo').getContext('2d');
        var ctx_three = document.getElementById('LineChartThree').getContext('2d');
        drawLineChart(ctx_one, impsGraphData.lables, impsGraphData.dataset_one, impsGraphData.dataset_two);
        drawLineChart(ctx_two, clicksGraphData.lables, clicksGraphData.dataset_one, clicksGraphData.dataset_two);
        drawLineChart(ctx_three, actionsGraphData.lables, actionsGraphData.dataset_one, actionsGraphData.dataset_two);

        function drawLineChart(ctx_two, lables, datasetOne, datasetTwo){
            var myLineChart = new Chart(ctx_two, {
                type: 'line',
                data: {
                        //labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul','Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                        labels: lables,
                        datasets: [{
                            label: 'Total Spent',
                            borderColor: "#0000A0",
                            fill: false,
                            data: datasetOne
                        },
                        {
                            label: 'Impressions',
                            borderColor: "#E10000",
                            fill: false,
                            data: datasetTwo
                        }]
                    },
                options: {
                            scales: {
                                xAxes: [{
                                    gridLines: {
                                        display:false
                                    }
                                }],
                                yAxes: [{
                                    ticks: {
                                        max: 12,
                                        min: 0,
                                        stepSize: 4
                                    },
                                    gridLines: {
                                        display:false
                                    }
                                }]
                            },
                            responsive: true,
                            elements: {
                                line: {
                                    tension: 0 // disables bezier curves
                                },
                                point: {
                                    radius: 0
                                }
                            }
                        }
            });
        }
    </script>
  </body>
</html>