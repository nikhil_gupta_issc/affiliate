<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/libs/css/style.css">
    <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    .bg-image {
        background-image: linear-gradient(to bottom, rgba(166, 132, 211, 0.52), rgba(182, 211, 128, 0.52)), url("<?php echo base_url('sitehome-assets/assets/images/poker-1.png');?>");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
}
    </style>
</head>

<body class="bg-image" >
    <!-- ============================================================== -->
    <!-- login page  -->
    <!-- ============================================================== -->
    <div class="container" >
    <div class="row justify-content-center">
        <div class="col-md-6">
        <div class="card p-4">
            <div class="card-header"><span class="h3 font-weight-bold text-dark">Login Form</span></div>
            <div class="card-body">
                <form action="/login" method="POST">
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-user icon m-2"></i></div>
                        
                        </div>
                        <input class="form-control form-control-lg" id="username" type="text" placeholder="Username" autocomplete="off" name="username">
                        <!-- <input type="text" class="form-control" id="inlineFormInputGroupUsername" placeholder="Username"> -->
                    </div>
                        
                    </div>
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fa fa-key icon m-2"></i></div>
                        </div>
                        <input class="form-control form-control-lg" id="password" type="password" placeholder="Password" name="password">
                    </div>
                    <div class="form-group mt-4">
                        <button type="submit" class="btn btn-success text-white btn-lg btn-block">Sign in</button>
                    </div>
                    </div>
                </form>
            </div>
            <div class="card-footer bg-white p-0  ">
                <div class="row">
                    <div class="col-md-6 card-footer-item card-footer-item-bordered">
                        <a href="/auth/signup" class="footer-link">Create An Account</a></div>
                    <div class="col-md-6 card-footer-item card-footer-item-bordered">
                        <a href="#" class="footer-link">Forgot Password</a>
                    </div>
                </div>
            </div>
        </div>
        </div>     
    </div>
       
    </div>

    <!-- ============================================================== -->
    <!-- end login page  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
</body>

</html>
