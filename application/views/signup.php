<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Concept - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="../assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="../assets/libs/css/style.css">
    <link rel="stylesheet" href="../assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
    .number {
        color: rgb(255, 255, 255);
        font-size: 18px;
        left: 0;
        position: absolute;
        background: none 0px 0px repeat scroll rgb(48, 74, 143);
        padding: 5px 10px;
        border-radius: 2px;
}

.bg-class {
    background-image: url(../sitehome-assets/assets/images/—Pngtree—blue%20science%20technology%20background_88873.jpg);
    margin-bottom: 2em;
    color: rgb(233, 233, 233);
    text-align: center;
}

.bg-image {
        background-image: linear-gradient(to bottom, rgba(166, 132, 211, 0.52), rgba(182, 211, 128, 0.52)), url("<?php echo base_url('sitehome-assets/assets/images/poker-1.png');?>");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
}


    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        margin-top: 40px;
    }
    </style>
</head>
<!-- ============================================================== -->
<!-- signup form  -->
<!-- ============================================================== -->

<body class="bg-image">
    <!-- ============================================================== -->
    <!-- signup form  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
    <div class="row align-items-center">
        <div class="offset-lg-3 col-xl-6 col-lg-6 col-md-8 col-sm-12 offset-md-2 align-self-center">
        <div class="card">
            <div class="card-header bg-class">
                 <h3 class="card-title text-white pt-2">Registrations Form</h3>
            </div>
            <div class="card-body">
                <?php echo form_open('/auth/signup'); ?>
                <!-- <form class="container" action="" method="POST"> -->
                     <div class="container">
                        <div class="row">
                            <h5 class="ml-4"><span class="number">1</span>Personal Information</h5>
                        </div>
                        <div class="row justify-content-around">
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'First Name','name'=>'first_name','value'=>set_value('first_name')]);  ?>
                                    <span class="d-block text-danger"><?php  echo form_error('first_name');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="first-name" type="text" name="first_name" required="" placeholder="First Name" autocomplete="off"> -->
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'Last Name','name'=>'last_name','value'=>set_value('last_name')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('last_name');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="last-name" type="text" name="last_name" required="" placeholder="Last Name" autocomplete="off"> -->
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-around">
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'Company ','name'=>'company','value'=>set_value('company')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('company');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="company" type="text" name="company" required="" placeholder="Company" autocomplete="off"> -->
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'Address ','name'=>'address','value'=>set_value('address')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('address');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="address" type="text" name="address" required="" placeholder="Address" autocomplete="off"> -->
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-around">
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'City ','name'=>'city','value'=>set_value('city')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('city');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="city" type="text" name="city" required="" placeholder="City" autocomplete="off"> -->
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'State ','name'=>'state','value'=>set_value('state')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('state');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="state" type="text" name="state" required="" placeholder="State" autocomplete="off"> -->
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-around">
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'Country ','name'=>'country','value'=>set_value('country')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('country');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="country" type="text" name="country" required="" placeholder="Country" autocomplete="off"> -->
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'Pincode ','name'=>'pincode','value'=>set_value('pincode')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('pincode');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="pincode" type="text" name="pincode" required="" placeholder="Pincode" autocomplete="off"> -->
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="container">
                        <div class="row">
                            <h5 class="ml-4"><span class="number">2</span>How can we Contact you</h5>
                        </div>
                        <div class="row justify-content-around">
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'Email ','name'=>'email','value'=>set_value('email')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('email');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="email" type="email" name="email" required="" placeholder="E-mail" autocomplete="off">    -->
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_input(['class'=>'form-control','placeholder'=>'Phone ','name'=>'phone','value'=>set_value('phone')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('phone');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="phone" type="text" name="phone" required="" placeholder="Phone" autocomplete="off"> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <h5 class="ml-4"><span class="number">3</span>Account Details</h5>
                        </div>
                        <div class="row justify-content-around">
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                <?php echo form_input(['class'=>'form-control','placeholder'=>'Username','name'=>'username','value'=>set_value('username')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('username');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="username" type="text" name="username" required="" placeholder="Username" autocomplete="off"> -->
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_password(['class'=>'form-control','placeholder'=>'Password','name'=>'password','value'=>set_value('password')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('password');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="password" type="password" name="password" required="" placeholder="Password"> -->
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-around">
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                <?php echo form_password(['class'=>'form-control','placeholder'=>'Password Confirm','name'=>'confirm_password','value'=>set_value('confirm_password')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('confirm_password');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="confirm-password" type="password" name="confirm_password" required="" placeholder="Confirm Password"> -->
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-6 col-xm-12">
                                <div class="form-group">
                                <?php echo form_input(['class'=>'form-control','placeholder'=>'Payment Type','name'=>'payment_type','value'=>set_value('payment_type')]);  ?>
                                        <span class="d-block text-danger"><?php  echo form_error('payment_type');  ?></span>
                                    <!-- <input class="form-control form-control-lg" id="payment-type" type="text" name="payment_type" required="" placeholder="Payment Type" autocomplete="off"> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                                <h5 class="ml-4"><span class="number">4</span>What Do You Do?</h5>
                        </div>
                        <div class="row justify-content-around">
                            <div class="col-md-12 col-xl-12 col-sm-12 col-xm-12">
                                <div class="form-group">
                                    <?php echo form_dropdown(['name'=>'whatyoudo', 'class' => 'custom-select mr-sm-2', 'options'=> array(null=>'Choose one..','1'=>'Affiliate', '2'=>'Advertiser')]);  ?>
                                    <span class="d-block text-danger"><?php  echo form_error('whatyoudo');  ?></span>
                                </div>
                            </div>
                            <div class="col-md-12 col-xl-12 col-sm-12 col-xm-12">
                                <div class="form-group">
                                    <label class="custom-control custom-checkbox">
                                    <?php echo form_checkbox([ 'class'=>'custom-control-input','name'=> 'terms_n_conditions','value'=>1]);  ?>
                                    <span class="custom-control-label">By creating an account, you agree the <a href="#">terms and conditions</a></span>
                                        <span class="d-block text-danger"><?php  echo form_error('terms_n_conditions');  ?></span>
                                        <!-- <input class="custom-control-input" type="checkbox" name="terms_n_conditions"><span class="custom-control-label">By creating an account, you agree the <a href="#">terms and conditions</a></span> -->
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-xl-4 col-sm-6 col-xm-3">
                                <div class="form-group">
                                    <?php  echo form_submit(['type'=>'submit','class'=>'btn btn-primary','value'=>'Register My Account']);  ?>
                                    <!-- <button class="btn btn-primary" type="submit">Register My Account</button>  -->
                                </div>
                            </div>
                            <div class="col-md-8 col-xl-8 col-sm-6 col-xm-6 offset-md-0 border-left border-dark pt-2">
                                <a href="<?php echo base_url('login'); ?>" class="link mt-2">Have an Account login Here...</a>
                            <div>
                        </div>
                    </div>
                    <!-- Display the status message -->
                    <?php if(!empty($status)){ ?>
                         <div class="status text-primary <?php echo $status['type']; ?>"><?php echo $status['msg']; ?></div>
                    <?php } ?>

                    <!-- Register form -->
            </form>
            </div>
        </div>
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-12">
        <form class="splash-container" action="" method="POST">
        <div class="card">
            <div class="card-header">
                <h3 class="mb-1">Registrations Form</h3>
                <p>Please enter your user information.</p>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <input class="form-control form-control-lg" id="username" type="text" name="username" required="" placeholder="Username" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="password" type="password" name="password" required="" placeholder="Password">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="confirm-password" type="password" name="confirm_password" required="" placeholder="Confirm Password">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="email" type="email" name="email" required="" placeholder="E-mail" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="first-name" type="text" name="first_name" required="" placeholder="First Name" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="last-name" type="text" name="last_name" required="" placeholder="Last Name" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="company" type="text" name="company" required="" placeholder="Company" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="address" type="text" name="address" required="" placeholder="Address" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="city" type="text" name="city" required="" placeholder="City" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="state" type="text" name="state" required="" placeholder="State" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="pincode" type="text" name="pincode" required="" placeholder="Pincode" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="country" type="text" name="country" required="" placeholder="Country" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="phone" type="text" name="phone" required="" placeholder="Phone" autocomplete="off">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" id="payment-type" type="text" name="payment_type" required="" placeholder="Payment Type" autocomplete="off">
                </div>
                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="terms_n_conditions"><span class="custom-control-label">By creating an account, you agree the <a href="#">terms and conditions</a></span>
                    </label>
                </div>
                <div class="form-group pt-2">
                    <button class="btn btn-block btn-primary" type="submit">Register My Account</button>
                </div>
            </div>
            <div class="card-footer bg-white">
                <p>Already member? <a href="/" class="text-secondary">Login Here.</a></p>
            </div>
        </div>
    </form>
</div> -->
    </div>


</body>


</html>
