<!DOCTYPE html>
<html lang="en-US">

<head>
    <title>
        Affiliate | Operator
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../assets/libs/css/admin.css?v=1.3">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>

    <style>
        #all-affiliates, #my-requests, #all-deal-affiliates, #my-deal-requests{
            display: none;
        }
        .btn-details-manual {
            background-color: #dddddd;
            border: 0;
            padding-top: 0.25em;
            padding-bottom: 0.25em;
            border-radius: 4px;
            color: #505050;
            margin-top: 0.50em;
            margin-left: 5px;
        }
    </style>
</head>

<body>
    <!-- Header Starts here -->
    <div class="wrapper">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="logo"></div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
            <div class="notification">
                <div class="row notification-row">
                    <p class="notif-name"> Hi, <b> <?=$company?> </b></span></p>
                    <span class="notif"><i class="fa fa-bell" aria-hidden="true"></i></span>
                </div>
                <div class="row month">
                    Your <?=date('M Y')?> stats
                </div>
                <div class="row">
                    <ul class="stats-list">
                        <li>Total Clicks <br> <?=$total_clicks?></li>
                        <li>Sign Ups <br> <?=$total_signups?></li>
                        <li>FTD <br> <?=$total_deposits?></li>
                        <li>Profit <br> &euro; <?=$total_earnings?></li>
                        <!--li><button class="btn">Details</button></li-->
                    </ul>
                </div>
            </div>
        </div>
        </div>
        <!-- Header Ends here -->
        <!-- Navbar starts here -->
        <div class="container-fluid">
            <div class="row nav-pills-row">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?=(!isset($_GET['page']) || $_GET['page']=='home') ? 'active': ''?>" id="pills-home-tab" href="/advertiser" role="tab"
                            aria-controls="pills-home" aria-selected="true">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='stats') ? 'active': ''?>" id="pills-stats-tab"  href="/advertiser?page=stats" role="tab"
                            aria-controls="pills-stats" aria-selected="false">Stats</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='campaigns') ? 'active': ''?>" id="pills-campaign-tab"  href="/advertiser?page=campaigns" role="tab"
                            aria-controls="pills-campaign" aria-selected="false">Campaigns</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='offers') ? 'active': ''?>" id="pills-deals-tab"  href="/advertiser?page=offers" role="tab"
                            aria-controls="pills-deals" aria-selected="false">Offers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='payments') ? 'active': ''?>" id="pills-payment-tab"  href="/advertiser?page=payments" role="tab"
                            aria-controls="pills-payment" aria-selected="false">Payments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='promotions') ? 'active': ''?>" id="pills-promotions-tab"  href="/advertiser?page=promotions"
                            role="tab" aria-controls="pills-promotions" aria-selected="false">Promotions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='affiliates') ? 'active': ''?>" id="pills-affiliate-tab"  href="/advertiser?page=affiliates"
                            role="tab" aria-controls="pills-affiliates" aria-selected="false">Affiliates</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='negotiations') ? 'active': ''?>" id="pills-negotiate-tab"  href="/advertiser?page=negotiations"
                            role="tab" aria-controls="pills-negotiate" aria-selected="false">Negotiation Requests</a>
                    </li>
                    <li class="nav-item float-lg-right float-right profile-btn">
                        <a class="nav-link <?=(isset($_GET['page']) && $_GET['page']=='profile') ? 'active': ''?>" id="pills-profile-tab"  href="/advertiser?page=profile" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item float-lg-right float-right logout-btn">
                        <a class="nav-link"  href="/auth/logout">Logout</a>
                    </li>
                </ul>
            </div>
            <!-- Navbar Ends here -->
            <!-- Page Content begins here -->
            <div class="container-fluid tab-content-container">
                <div class="tab-content" id="pills-tabContent">
                    <!-- Home Tab content begins herre -->
                    <div class="tab-pane fade <?=(!isset($_GET['page']) || $_GET['page']=='home') ? 'show active': ''?>" id="pills-home" role="tabpanel"
                        aria-labelledby="pills-home-tab">
                        <div class="container">
                            <div class="row stats-row p-3">
                                <div class="col-md-2"><?=$total_affiliates?>
                                    <div class="box">Total Affiliates</div>
                                    <img class="tap" src="../assets/icons/affiliate_click.png" height="25" width="25" alt="Affiliate">
                                </div>
                                <div class="col-md-2"><?=$new_affiliates?>
                                  <div class="box">New Affiliates</div>
                                  <img class="icons" src="../assets/icons/add-new-affiliate.png" height="25" width="25"
                                      alt="New Affiliate">
                              </div>
                              <div class="col-md-2"><?=$total_clicks?>
                                  <div class="box">Total Clicks</div>
                                  <img class="icons" src="../assets/icons/list.svg" height="25" width="25" alt="Signups">
                              </div>
                                <div class="col-md-2"><?=$total_signups?>
                                    <div class="box">Signups</div>
                                    <img class="icons" src="../assets/icons/list.svg" height="25" width="25" alt="Signups">
                                </div>
                                <div class="col-md-2"><?=$total_deposits?>
                                    <div class="box">FTD</div>
                                    <img class="icons" src="../assets/icons/credit-card.svg" height="25" width="25"
                                        alt="FTD">
                                </div>
                                <div class="col-md-2"> &euro; <?=$total_earnings?><br>
                                    <div class="box">Total Earning for the Month</div>
                                    <img class="icons" src="../assets/icons/funds.svg" height="25" width="25"
                                        alt="Cash">
                                </div>
                            </div>
                        </div>
                        <div class="container wrapper-container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="card section-2">
                                        <div class="row">
                                            <img class="section_img" src="../assets/images/pie-chart.png" height="40"
                                                width="40" alt="Stats">
                                            <span class="text-img"> Statistics Breakdown</span>
                                            <div class="dropdown stats-brerakdown">
                                                <button class="btn btn-light dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <span id="selected">Choose option</span>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" onClick="affiliates()">Affiliates</a>
                                                    <a class="dropdown-item" onClick="newAffiliates()">New
                                                        Affiliates</a>
                                                    <a class="dropdown-item" onClick="signUps()">Signups</a>
                                                    <a class="dropdown-item" onClick="ftds()">FTD</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="container">
                                            <canvas id="myAffiliates" width="400" height="250"></canvas>
                                        </div>
                                    </div>
                                    <div class="card section-3">
                                        <div class="row">
                                            <img class="section_img" src="../assets/images/bar-graph.jpg" height="40"
                                                width="40" alt="Stats">
                                            <span class="text-img"> Past Month Stats</span>
                                            <div class="dropdown mothstats">
                                                <button class="btn btn-light dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <span id="monthstats">Choose option</span>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" onClick="profits()">Profit</a>
                                                    <a class="dropdown-item" onClick="registration()">Registration</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="container">
                                            <canvas id="mySales" width="400" height="250"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="bannerboard">
                                        <img class="banner-board img-fluid" src="../assets/images/ads-1.jpg"
                                            alt="banner board">
                                    </div>
                                    <div class="advboard">
                                        <img class="banner-board img-fluid" src="../assets/images/ads-2.jpg"
                                            alt="banner board">
                                    </div>
                                    <div class="prizeboard">
                                        <img class="banner-board img-fluid" src="../assets/images/ads-3.png"
                                            alt="banner board">
                                    </div>
                                    <div class="card payment-card">
                                        <div class="row">
                                            <img class="section_img" src="../assets/icons/images.png" height="40"
                                                width="40" alt="Stats">
                                            <span class="text-img"> Latest Payments</span>
                                            <button class="payment-btn btn-sm">All Payments</button>
                                        </div>

                                        <div class="row">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='stats') ? 'show active': ''?>" id="pills-stats" role="tabpanel" aria-labelledby="pills-stats-tab">
                        <div class="container statistic-container">
                            <div class="card time-card">
                                <div class="row">
                                    <div class="col-lg-6 calendar-container">
                                        <b>Time Period</b>
                                        <div class="row">
                                            <input type="date" class="date hasDatepicker" name="today_date"
                                                id="today_date" max="23-08-2019">
                                            <input type="date" name="previous_date" id="previous_date" min="23-05-2019">
                                            <button class="btn btn-success">View</button>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 quick_dates">
                                        <b>Quick Months</b>
                                        <div class="row">
                                            <button class="months">Month 1</button>
                                            <button class="months">Month 2</button>
                                            <button class="months">Month 3</button>
                                            <button class="months">Month 4</button>
                                            <button class="months">Month 5</button>
                                        </div>
                                        <div class="row time-period">
                                            <button class="months">Today</button>
                                            <button class="months">Yesterday</button>
                                            <button class="months">This Year</button>
                                            <button class="months">Last Year</button>
                                            <button class="months">All Time</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- New container -->

                            <div class="container">
                                <div class="row stats-row">
                                    <div class="col-md-2">0
                                        <div class="box">Total Affiliates</div>
                                        <img class="tap" src="../assets/icons/affiliate_click.png" height="25" width="25" alt="Tap">
                                    </div>
                                    <div class="col-md-2">0
                                      <div class="box">New Affiliates</div>
                                      <img class="icons" src="../assets/icons/visiting-card.svg" height="25" width="25"
                                          alt="CPA">
                                  </div>
                                    <div class="col-md-2">0
                                        <div class="box">Signups</div>
                                        <img class="icons" src="../assets/icons/list.svg" height="25" width="25" alt="List">
                                    </div>
                                    <div class="col-md-2">0
                                        <div class="box">FTD</div>
                                        <img class="icons" src="../assets/icons/credit-card.svg" height="25" width="25"
                                            alt="FTD">
                                    </div>
                                    <div class="col-md-2">0%
                                        <div class="box">Signup to deposit</div>
                                        <img class="icons" src="../assets/icons/double-angle-pointing-to-right.svg"
                                            height="20" width="20" alt="Right">
                                    </div>
                                    <div class="col-md-2"> &euro; 0<br>
                                        <div class="box">Total Earning for the Month</div>
                                        <img class="icons" src="../assets/icons/funds.svg" height="25" width="25"
                                            alt="Cash">
                                    </div>
                                </div>
                            </div>

                            <!-- Table section -->
                            <div class="card table-card">
                                <div class="search">
                                    <div class="row">
                                        <label for="Search"> Search: <input type="text" table="table_name"></label>
                                    </div>
                                </div>

                                <table class="payout-table">
                                    <thead>
                                        <th aria-sort="ascending">Affiliates</th>
                                        <th>Signups</th>
                                        <th>FTD</th>
                                        <th>Rev payout</th>
                                        <th>CPL payout</th>
                                        <th>Total payout</th>
                                    </thead>
                                    <tr>
                                        <td>{{Message}}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>

                                <table class="payout-table">
                                    <thead>
                                        <th aria-sort="ascending">Affiliates</th>
                                        <th>Signups</th>
                                        <th>FTD</th>
                                        <th>Rev payout</th>
                                        <th>CPL payout</th>
                                        <th>Total payout</th>
                                    </thead>
                                    <tr>
                                        <td>Total</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Statistic COntainer Ends here -->
                    <!-- Campaign container begins here -->
                    <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='campaigns') ? 'show active': ''?>" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">
                        <div class="card campaign-card">
                            <div class="container">
                                <h3>Campaigns</h3>
                              </div>
                                <table class='payout-table mx-4 my-2'>
                                  <thead>
                                    <th>Offers</th>
                                    <th>Clicks</th>
                                    <th>Sign Ups</th>
                                    <th>FTD</th>
                                    <th>Rev Payout</th>
                                    <th>CPL Payout</th>
                                    <th>Total Payout</th>
                                  </thead>
                                  <tr>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                  </tr>
                                </table>
                                <table class='payout-table mx-4 my-2'>
                                    <thead>
                                      <th>Offers</th>
                                      <th>Clicks</th>
                                      <th>Sign Ups</th>
                                      <th>FTD</th>
                                      <th>Rev Payout</th>
                                      <th>CPL Payout</th>
                                      <th>Total Payout</th>
                                    </thead>
                                    <tr>
                                      <td>0</td>
                                      <td>0</td>
                                      <td>0</td>
                                      <td>0</td>
                                      <td>0</td>
                                      <td>0</td>
                                      <td>0</td>
                                    </tr>
                                  </table>
                        </div>
                    </div>
                    <!-- Campaign container ends here -->
                    <!-- Deal container begins here -->
                    <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='offers') ? 'show active': ''?>" id="pills-deals" role="tabpanel" aria-labelledby="pills-deals-tab">
                        <div class="card deals-card">
                            <div class="container" style="margin-left:inherit;">
                                <h3>Campaigns Deals</h3>
                                <div class="row">
                                    <label class="deal-type" for="deal types"> <b>Deal Types</b></label>
                                    <div class="radio-class">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-outline-dark active">
                                                <input type="radio" name="options" id="option1" autocomplete="off"
                                                    checked>
                                                All Deals
                                            </label>
                                            <label class="btn btn-outline-dark">
                                                <input type="radio" name="options" id="option2" autocomplete="off">
                                                Revshare
                                            </label>
                                            <label class="btn btn-outline-dark">
                                                <input type="radio" name="options" id="option3" autocomplete="off"> CPA
                                            </label>
                                            <label class="btn btn-outline-dark">
                                                <input type="radio" name="options" id="option3" autocomplete="off"> CPL
                                            </label>
                                        </div>
                                    </div>

                                    <button class="btn btn-outline-dark btn-sm upload-button" data-toggle="modal" data-target="#offerUpload"> Upload Deals</button>
                                    <div class="modal fade" id="offerUpload" tabindex="-1" role="dialog" aria-labelledby="offerUpload" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="offerUpload">Upload Deal</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                              <form action="/advertiser/upload" method="post" enctype="multipart/form-data">
                                                  <div class="row">
                                                      <div class="col-lg-12 mx-2 my-3">
                                                          <div class="container">
                                                          <label for="newDeal" class="col-lg-5">Choose Banner:</label>
                                                          <input type="file" class="col-lg-6" name="banner"  id="banner">
                                                      </div>
                                                      </div>
                                                  <div class="col-lg-12 mx-2 my-3">
                                                      <div class="container">
                                                      <label for="Type of deal" class="col-lg-5">Deal Type &nbsp;</label>
                                                      <select name="deal_type" id="deal-type" class="col-lg-6">
                                                              <option value="REVENUE_SHARE" id="revshare">Revshare</option>
                                                              <option value="CPA" id="cpa">CPA</option>
                                                              <option value="CPC" id="cpc">CPC</option>
                                                              <option value="HYBRID" id="hybrid">Hybrid</option>
                                                      </select>
                                                  </div>
                                                  </div>
                                                  <div class="col-lg-12 mx-2 my-3" id="rev-share">
                                                      <div class="container">
                                                          <label for="Revshare" class="col-lg-5">Your Percentage:</label>
                                                          <input width="50" type="number" id="revshare-percent" class="col-lg-6" name="revenue_percentage" value=0 oninput="updateRevsharePercent()"> %
                                                      </div>

                                                      <div class="container">
                                                          <label for="Revshare" class="col-lg-5">AMS Percentage:</label>
                                                          <input width="50" type="number" id="revshare-percent-ams" class="col-lg-6" name="revenue_percentage_ams" value=1 disabled> %
                                                      </div>

                                                      <div class="container">
                                                          <label for="Revshare" class="col-lg-5">Total Percentage:</label>
                                                          <input width="50" type="number" id="revshare-percent-total" class="col-lg-6" name="revenue_percentage_total" value=1> %
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-12 mx-2 my-3" id="cpa-container">
                                                      <div class="container">
                                                          <label for="Revshare" class="col-lg-5">Your Amount:</label>
                                                          <input width="50" class="col-lg-6" type="number" id="cpa-amount" name="cpa_amount" oninput="updateCPA()" value=0> &euro;
                                                      </div>

                                                      <div class="container">
                                                          <label for="Revshare" class="col-lg-5">AMS Amount:</label>
                                                          <input width="50" class="col-lg-6" type="number" id="cpa-amount-ams" name="cpa_amount_ams" value=10 disabled> &euro;
                                                      </div>

                                                      <div class="container">
                                                          <label for="Revshare" class="col-lg-5">Total Amount:</label>
                                                          <input width="50" class="col-lg-6" type="number" id="cpa-amount-total" name="cpa_amount_total" value=10> &euro;
                                                      </div>

                                                        <div class="container">
                                                            <label for="action type mr-4" class="col-lg-5">Action:</label>
                                                            <select name="cpa_action" id="cpa-action" class="col-lg-6">
                                                                    <option value="SIGNUP" id="cpa-signup">Signup</option>
                                                                    <option value="DEPOSIT" id="cpa-deposit">Deposit</option>
                                                            </select>
                                                        </div>

                                                  </div>
                                                  <div class="col-lg-12 mx-2 my-3" id="cpc-container">
                                                      <div class="container">
                                                            <label for="CPC" class="col-lg-5">Clicks: </label>
                                                            <input width="50" type="number"  class="col-lg-6" id="cpc-clicks" name="cpc_click">
                                                      </div>
                                                      <div class="container">
                                                              <label for="CPC" class="col-lg-5">Your Amount: </label>
                                                              <input width="50" class="col-lg-6" type="number" id="cpc-percentage" name="cpc_amount" oninput="updateCPC()" value=0> &euro;
                                                      </div>
                                                      <div class="container">
                                                              <label for="CPC" class="col-lg-5">AMS Amount: </label>
                                                              <input width="50" class="col-lg-6" type="number" id="cpc-percentage-ams" name="cpc_amount_ams" value=10 disabled> &euro;
                                                      </div>
                                                      <div class="container">
                                                              <label for="CPC" class="col-lg-5">Total Amount: </label>
                                                              <input width="50" class="col-lg-6" type="number" id="cpc-percentage-total" name="cpc_amount_total" value=10> &euro;
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-12 mx-2 my-3" id="hybrid-container">
                                                      <div class="container">
                                                          <label for="Hybrid" class="col-lg-5">Your Amount: </label>
                                                          <input width="50" class="col-lg-6" type="number" id="hybrid-amount" name="hybrid_amount" oninput="updateHybridAmount()" value=0> &euro;
                                                      </div>
                                                      <div class="container">
                                                          <label for="Hybrid" class="col-lg-5">AMS Amount: </label>
                                                          <input width="50" class="col-lg-6" type="number" id="hybrid-amount-ams" name="hybrid_amount_ams" value=10 disabled> &euro;
                                                      </div>
                                                      <div class="container">
                                                          <label for="Hybrid" class="col-lg-5">Total Amount: </label>
                                                          <input width="50" class="col-lg-6" type="number" id="hybrid-amount-total" name="hybrid_amount_total" value=10> &euro;
                                                      </div>

                                                      <div class="container">
                                                          <label for="Hybrid" class="col-lg-5">Your Percentage: </label>
                                                          <input width="50" class="col-lg-6" type="number" id="hybrid-percentage" name="hybrid_percentage" oninput="updateHybridPercentage()" value=0> &euro;
                                                      </div>
                                                      <div class="container">
                                                          <label for="Hybrid" class="col-lg-5">AMS Percentage: </label>
                                                          <input width="50" class="col-lg-6" type="number" id="hybrid-percentage-ams" name="hybrid_percentage_ams" value=1 disabled> &euro;
                                                      </div>
                                                      <div class="container">
                                                          <label for="Hybrid" class="col-lg-5">Total Percentage: </label>
                                                          <input width="50" class="col-lg-6" type="number" id="hybrid-percentage-total" name="hybrid_percentage_total" value=1> &euro;
                                                      </div>
                                              </div>
                                              <div class="col-lg-12 mx-2 my-3">
                                                  <div class="container">
                                                  <label for="website" class="col-lg-5">Webite URL: &nbsp;</label>
                                                  <input type="text" id="website" class="col-lg-6" name="website"/>
                                              </div>
                                              </div>
                                              <div class="col-lg-12 mx-2 my-3">
                                                  <div class="container">
                                                  <label for="Details" class="col-lg-5">Comments: &nbsp;</label>
                                                  <textarea id="deal-details" class="col-lg-6" rows="4" cols="50" name="comment"></textarea>
                                              </div>
                                              </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              <input type="submit" class="btn btn-primary" name="submit" value="Save changes">
                                            </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="deals-search">
                                    <div class="row">
                                        <label class="label-deal" for="Search"><b> Search:</b> <input type="text"
                                                placeholder="Search Deals" table="capaign_deals"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container deal-container" style="margin-left:inherit;">
                            <?php
                            foreach($trackers as $index => $tracker)
                            {
                                if($index % 4 == 0)
                                {
                                    ?>
                                    <div class="row">
                                    <?php
                                }

                                ?>
                                <div class="col-lg-3">
                                    <div class="card deal-card">
                                        <div style="text-align: center;"><img src="<?=$tracker['banner_url']?>" height="200" width="200" alt="<?=$tracker['tracker_name']?>" style="margin-top:10px;"></div>
                                        <div class="card-body">
                                            <button class="btn-deals"><?=$tracker['deal_type']?></button>
                                            <button class="btn-details-manual" data-toggle="modal" data-target="#Modal-<?=$index?>"> Details</button>
                                            <button class="btn-details-manual" onclick="chooseAffiliate('<?=$tracker['tracker_id']?>')"> Choose Affiliate</button>
                                            <div class="modal fade" id="Modal-<?=$index?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-<?=$index?>" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel-<?=$index?>">Deal Details</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table>
                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td><input type="text" value="<?=$tracker['tracker_name']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Deal Type</td>
                                                                    <td><input type="text" value="<?=$tracker['deal_type']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Your Commission</td>
                                                                    <td><input type="text" value="<?=$tracker['commission']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>AMS Commission</td>
                                                                    <td><input type="text" value="<?=$tracker['ams_commission']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Commission</td>
                                                                    <td><input type="text" value="<?=$tracker['commission'] + $tracker['ams_commission']?>"></td>
                                                                </tr>
                                                                <?php
                                                                if($tracker['deal_type'] == 'HYBRID')
                                                                {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Your Percentage</td>
                                                                        <td><input type="text" value="<?=$tracker['commission_percentage_hybrid']?>"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>AMS Percentage</td>
                                                                        <td><input type="text" value="<?=$tracker['ams_commission_percentage_hybrid']?>"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Total Percentage</td>
                                                                        <td><input type="text" value="<?=$tracker['commission_percentage_hybrid'] + $tracker['ams_commission_percentage_hybrid']?>"></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <td>Website</td>
                                                                    <td><input type="text" value="<?=$tracker['website']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Tracking Code</td>
                                                                    <td>
                                                                        <textarea rows="5" cols="50">
                                                                            <script>var parameter = window.location.search.substr(1);parameter = parameter.split("=");var affiliate_id = parameter[1];var page="LENDING";var addScript = document.createElement("script");addScript.type = "text/javascript";addScript.src = "http://affiliate.duoex.com/tracking?aid=" + affiliate_id + "&page="+page+"&tid=<?=$tracker['tracker_id']?>";
                                                                                (document.getElementsByTagName("head")[0] || document.documentElement ).appendChild(addScript);
                                                                            </script>
                                                                        </textarea>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <!--button type="button" class="btn btn-primary">Save changes</button-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php

                                if($index % 4 == 3)
                                {
                                    ?>
                                    </div>
                                    <?php
                                }
                            }
                            if(isset($index) && $index % 4 != 3)
                            {
                                ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='payments') ? 'show active': ''?>" id="pills-payment" role="tabpanel" aria-labelledby="pills-payment-tab">
                        <div class="card payment-tab">
                            <div class="container">
                                <h3>Payments</h3>
                            </div>
                            <table class="payment-table">
                                <thead>
                                    <th aria-sort="ascending">Month</th>
                                    <th>Amount</th>
                                    <th>AMS Commission</th>
                                    <th>Final Amount</th>
                                    <th>Status</th>
                                </thead>
                                <?php foreach($payments as $transaction) : ?>
                                <tr>
                                    <td><?= $transaction['month'] ?></td>
                                    <td><?= $transaction['amount'] ?></td>
                                    <td><?= $transaction['ams_commission'] ?></td>
                                    <td><?= $transaction['final_amount'] ?></td>
                                    <td><?= $transaction['status'] ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <div class="empty-bin"></div>
                    </div>
                    <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='promotions') ? 'show active': ''?>" id="pills-promotions" role="tabpanel"
                        aria-labelledby="pills-promotions-tab">
                        <div class="card deals-card">
                            <div class="container">
                                <h3>Promotions</h3>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='affiliates') ? 'show active': ''?>" id="pills-affiliates" role="tabpanel"
                        aria-labelledby="pills-affiliates-tab">
                        <div class="card deals-card" style="margin-bottom:30px;">
                            <div class="container-fluid">
                                <h3>Affiliates</h3>
                                    <div class="radio-class">
                                        <div class="btn-group">
                                            <label class="btn btn-outline-dark active" onclick="showAffiliate('my-affiliates')" id="my-affiliates-label">
                                                My Affiliates
                                            </label>
                                            <label class="btn btn-outline-dark" onclick="showAffiliate('my-requests')" id="my-requests-label">
                                                My Requests
                                            </label>
                                            <label class="btn btn-outline-dark" onclick="showAffiliate('all-affiliates')" id="all-affiliates-label">
                                                Choose Affiliate
                                            </label>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="card payment-tab">
                            <table class="payment-table" id="my-affiliates">
                                <?php
                                if($my_affiliates)
                                {
                                    ?>
                                    <thead>
                                        <th class="col-lg-4">Name</th>
                                        <th class="col-lg-4">Clicks / Tracker</th>
                                        <th class="col-lg-4">Total Campaigns</th>
                                    </thead>
                                    <?php
                                    foreach($my_affiliates as $affiliate)
                                    {
                                        ?>
                                        <tr>
                                            <td><?=$affiliate['firstname'] . ' ' . $affiliate['lastname']?></td>
                                            <td><?=$affiliate['avg_lending_clicks']?></td>
                                            <td><?=$affiliate['campaigns_count']?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="card payment-tab">
                            <table class="payment-table" id="my-requests">
                                <?php
                                if($my_requests)
                                {
                                    ?>
                                    <thead>
                                        <th class="col-lg-4">Name</th>
                                        <th class="col-lg-4">Clicks / Tracker</th>
                                        <th class="col-lg-4">Total Campaigns</th>
                                    </thead>
                                    <?php
                                    foreach($my_requests as $affiliate)
                                    {
                                        ?>
                                        <tr>
                                            <td><?=$affiliate['firstname'] . ' ' . $affiliate['lastname']?></td>
                                            <td><?=$affiliate['avg_lending_clicks']?></td>
                                            <td><?=$affiliate['campaigns_count']?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="card payment-tab">
                            <table class="payment-table" id="all-affiliates">
                                <?php
                                if($all_affiliates)
                                {
                                    ?>
                                    <thead>
                                        <th class="col-lg-4">Name</th>
                                        <th class="col-lg-4">Clicks / Tracker</th>
                                        <th class="col-lg-4">Total Campaigns</th>
                                    </thead>
                                    <?php
                                    foreach($all_affiliates as $affiliate)
                                    {
                                        ?>
                                        <tr>
                                            <td><?=$affiliate['firstname'] . ' ' . $affiliate['lastname']?></td>
                                            <td><?=$affiliate['avg_lending_clicks']?></td>
                                            <td><?=$affiliate['campaigns_count']?></td>
                                            <td>
                                                <form action="/advertiser/request" method="POST">
                                                    <input type="hidden" name="affiliate_id" value="<?=$affiliate['affiliate_id']?>">
                                                    <input type="submit" name="submit" value="Choose">
                                                </form>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='negotiations') ? 'show active': ''?>" id="pills-negotiate" role="tabpanel" aria-labelledby="pills-negotiate-tab">
                        <div class="card deals-card">
                            <div class="container" style="margin-left:inherit;">
                                <h3>Campaigns Deals</h3>
                                <div class="row">
                                    <label class="deal-type" for="deal types"> <b>Deal Types</b></label>
                                    <div class="radio-class">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-outline-dark active">
                                                <input type="radio" name="options" id="option1" autocomplete="off"
                                                    checked>
                                                All Deals
                                            </label>
                                            <label class="btn btn-outline-dark">
                                                <input type="radio" name="options" id="option2" autocomplete="off">
                                                Revshare
                                            </label>
                                            <label class="btn btn-outline-dark">
                                                <input type="radio" name="options" id="option3" autocomplete="off"> CPA
                                            </label>
                                            <label class="btn btn-outline-dark">
                                                <input type="radio" name="options" id="option3" autocomplete="off"> CPL
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="deals-search">
                                    <div class="row">
                                        <label class="label-deal" for="Search"><b> Search:</b> <input type="text"
                                                placeholder="Search Deals" table="capaign_deals"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container deal-container" style="margin-left:inherit;">
                            <?php
                            foreach($negotiations as $index => $tracker)
                            {
                                if($index % 4 == 0)
                                {
                                    ?>
                                    <div class="row">
                                    <?php
                                }

                                ?>
                                <div class="col-lg-3">
                                    <form action="/tracker/negotiation" method="POST">
                                    <div class="card deal-card">
                                        <div style="text-align: center;"><img src="<?=$tracker['banner_url']?>" height="200" width="200" alt="<?=$tracker['tracker_name']?>" style="margin-top:10px;"></div>
                                        <div class="card-body">
                                            <button class="btn-deals"><?=$tracker['deal_type']?></button>

                                            <input type="button" class="btn-details" data-toggle="modal" data-target="#negotiation-<?=$index?>" value="Details">
                                            <div class="modal fade" id="negotiation-<?=$index?>" tabindex="-1" role="dialog" aria-labelledby="negotiationModalLabel-<?=$index?>" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="negotiationModalLabel-<?=$index?>">Deal Details</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table>
                                                                <tr>
                                                                    <td>Name</td>
                                                                    <td><input type="text" value="<?=$tracker['tracker_name']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Deal Type</td>
                                                                    <td><input type="text" value="<?=$tracker['deal_type']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Commission</td>
                                                                    <td><input type="text" value="<?=$tracker['commission']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>AMS Commission</td>
                                                                    <td><input type="text" value="<?=$tracker['ams_commission']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Total Commission</td>
                                                                    <td><input type="text" value="<?=$tracker['commission'] + $tracker['ams_commission']?>"></td>
                                                                </tr>
                                                                <?php
                                                                if($tracker['deal_type'] == 'HYBRID')
                                                                {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Your Percentage</td>
                                                                        <td><input type="text" value="<?=$tracker['commission_percentage_hybrid']?>"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>AMS Percentage</td>
                                                                        <td><input type="text" value="<?=$tracker['ams_commission_percentage_hybrid']?>"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Total Percentage</td>
                                                                        <td><input type="text" value="<?=$tracker['commission_percentage_hybrid'] + $tracker['ams_commission_percentage_hybrid']?>"></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <td>Proposed Offer (Commission)</td>
                                                                    <td><input type="text" value="<?=$tracker['new_commission']?>"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Final Offer (Commission)</td>
                                                                    <td><input type="text" value="<?=$tracker['adv_commission']?>" name="adv_commission"></td>
                                                                </tr>
                                                                <?php
                                                                if($tracker['deal_type'] == 'HYBRID')
                                                                {
                                                                    ?>
                                                                    <tr>
                                                                        <td>Proposed Offer (Percentage)</td>
                                                                        <td><input type="text" value="<?=$tracker['new_percentage_hybrid']?>"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Final Offer (Percentage)</td>
                                                                        <td><input type="text" value="<?=$tracker['adv_percentage_hybrid']?>" name="adv_percentage_hybrid"></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <!--button type="button" class="btn btn-primary">Save changes</button-->
                                                            <input type="submit" class="btn btn-secondary" value="Submit Final Offer" name="proposal">
                                                            <input type="hidden" name="negotiation_id" value="<?=$tracker['negotiation_id']?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </div>
                                <?php

                                if($index % 4 == 3)
                                {
                                    ?>
                                    </div>
                                    <?php
                                }
                            }
                            if(isset($index) && $index % 4 != 3)
                            {
                                ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        </div>
                        <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='affiliates-deal') ? 'show active': ''?>" id="pills-affiliates" role="tabpanel"
                            aria-labelledby="pills-affiliates-tab">
                            <div class="card deals-card" style="margin-bottom:30px;">
                                <div class="container-fluid">
                                    <h3>Affiliates</h3>
                                        <div class="radio-class">
                                            <div class="btn-group">
                                                <label class="btn btn-outline-dark active" onclick="showAffiliate('my-deal-affiliates')" id="my-deal-affiliates-label">
                                                    My Affiliates
                                                </label>
                                                <label class="btn btn-outline-dark" onclick="showAffiliate('my-deal-requests')" id="my-deal-requests-label">
                                                    My Requests
                                                </label>
                                                <label class="btn btn-outline-dark" onclick="showAffiliate('all-deal-affiliates')" id="all-deal-affiliates-label">
                                                    Choose Affiliate
                                                </label>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="card payment-tab">
                                <table class="payment-table" id="my-deal-affiliates">
                                    <?php
                                    if(isset($my_deal_affiliates))
                                    {
                                        ?>
                                        <thead>
                                            <th class="col-lg-4">Name</th>
                                            <th class="col-lg-4">Clicks / Tracker</th>
                                            <th class="col-lg-4">Total Campaigns</th>
                                        </thead>
                                        <?php
                                        foreach($my_deal_affiliates as $affiliate)
                                        {
                                            ?>
                                            <tr>
                                                <td><?=$affiliate['firstname'] . ' ' . $affiliate['lastname']?></td>
                                                <td><?=$affiliate['avg_lending_clicks']?></td>
                                                <td><?=$affiliate['campaigns_count']?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                            <div class="card payment-tab">
                                <table class="payment-table" id="my-deal-requests">
                                    <?php
                                    if(isset($my_deal_requests))
                                    {
                                        ?>
                                        <thead>
                                            <th class="col-lg-4">Name</th>
                                            <th class="col-lg-4">Clicks / Tracker</th>
                                            <th class="col-lg-4">Total Campaigns</th>
                                        </thead>
                                        <?php
                                        foreach($my_deal_requests as $affiliate)
                                        {
                                            ?>
                                            <tr>
                                                <td><?=$affiliate['firstname'] . ' ' . $affiliate['lastname']?></td>
                                                <td><?=$affiliate['avg_lending_clicks']?></td>
                                                <td><?=$affiliate['campaigns_count']?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                            <div class="card payment-tab">
                                <table class="payment-table" id="all-deal-affiliates">
                                    <?php
                                    if(isset($all_deal_affiliates))
                                    {
                                        ?>
                                        <thead>
                                            <th class="col-lg-4">Name</th>
                                            <th class="col-lg-4">Clicks / Tracker</th>
                                            <th class="col-lg-4">Total Campaigns</th>
                                        </thead>
                                        <?php
                                        foreach($all_deal_affiliates as $affiliate)
                                        {
                                            ?>
                                            <tr>
                                                <td><?=$affiliate['firstname'] . ' ' . $affiliate['lastname']?></td>
                                                <td><?=$affiliate['avg_lending_clicks']?></td>
                                                <td><?=$affiliate['campaigns_count']?></td>
                                                <td>
                                                    <form action="/advertiser/deal" method="POST">
                                                        <input type="hidden" name="affiliate_id" value="<?=$affiliate['affiliate_id']?>">
                                                        <input type="hidden" name="tracker_id" value="<?=$affiliate['deal_id']?>">
                                                        <input type="submit" name="submit" value="Choose">
                                                    </form>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    <div class="tab-pane fade <?=(isset($_GET['page']) && $_GET['page']=='profile') ? 'show active': ''?>" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card deals-card">
                            <div class="container">
                                <h3>Profile</h3>
                            </div>
                        </div>
                        <div class="container">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#account_info"
                                        role="tab" aria-controls="home" aria-selected="true">Account Info</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#contact_info"
                                        role="tab" aria-controls="profile" aria-selected="false">Contact Info</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#account_number"
                                        role="tab" aria-controls="contact" aria-selected="false">Account Number</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#payment_info"
                                        role="tab" aria-controls="contact" aria-selected="false">Payment Info</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="account_info" role="tabpanel"
                                    aria-labelledby="home-tab">
                                    <div class="card">
                                        <table class="user-info-1">
                                            <tr>
                                                <td> User Name
                                                    <p class="small_text">You cant change user name</p>
                                                </td>
                                                <td> {{User Name}}</td>
                                            </tr>
                                            <tr>
                                                <td> Display Name
                                                </td>
                                                <td><input type="text" class="display_name" id="display_name"></td>
                                            </tr>
                                            <tr>
                                                <td> Password
                                                    <p class="small_text">Leave it blank if you dont want to change</p>
                                                </td>
                                                <td><input type="password" class="password" id="password"></td>
                                            </tr>
                                            <tr>
                                                <td> Receive newsletter per email?
                                                </td>
                                                <td><label class="switch">
                                                        <input type="checkbox" value="vat_num" checked>
                                                        <span class="slider round"></span>
                                                      </label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td><button> Update</button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact_info" role="tabpanel"
                                    aria-labelledby="profile-tab">
                                    <div class="card">
                                            <table class="user-info-1">
                                                <tr>
                                                    <td> First Nme
                                                    </td>
                                                    <td><input type="text" class="first_name" id="first_name"></td>
                                                </tr>
                                                <tr>
                                                    <td>Last Name
                                                    </td>
                                                    <td><input type="text" class="last_name" id="last_name"></td>
                                                </tr>
                                                <tr>
                                                    <td> Date of Birth
                                                    </td>
                                                    <td>
                                                    <div class="row" style="margin-left: 0em;">
                                                        <input type="number" class="date" id="date" name="quantity" min="1" max="31">
                                                        <input type="number" class="month" id="month" name="quantity" min="1" max="12">
                                                        <input type="number" class="year" id="year" name="quantity" max="2019">
                                                    </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Phone Number
                                                    </td>
                                                    <td><input type="Number" class="phone_number" id="phone_number"></td>
                                                </tr>
                                                <tr>
                                                        <td> Email
                                                        </td>
                                                        <td><input type="email" class="email" id="email"></td>
                                                    </tr>
                                                    <tr>
                                                            <td>Skype
                                                            </td>
                                                            <td><input type="skype_mail" class="skype_mail" id="skype_mail"></td>
                                                        </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td><button> Update</button></td>
                                                </tr>
                                            </table>
                                        </div>
                                </div>
                                <div class="tab-pane fade" id="account_number" role="tabpanel"
                                    aria-labelledby="contact-tab">
                                    <div class="card">
                                            <table class="user-info-1">
                                                <tr>
                                                        <td> Company Name / Account Owner
                                                            </td>
                                                            <td><input type="text" class="company_name" id="company_name"></td>
                                                </tr>
                                                <tr>
                                                    <td> Address
                                                    </td>
                                                    <td><input type="text" class="address" id="address"></td>
                                                </tr>
                                                <tr>
                                                    <td> Postcode
                                                    </td>
                                                    <td><input type="text" class="postcode" id="postcode"></td>
                                                </tr>
                                                <tr>
                                                        <td>City
                                                        </td>
                                                        <td><input type="text" class="city" id="city"></td>
                                                    </tr>
                                                    <tr>
                                                            <td>Country
                                                            </td>
                                                            <td><input type="text" class="country" id="country"></td>
                                                        </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td><button> Update</button></td>
                                                </tr>
                                            </table>
                                        </div>
                                </div>
                                <div class="tab-pane fade" id="payment_info" role="tabpanel"
                                    aria-labelledby="profile-tab">
                                    <div class="container payment-info"><span class="general">General</span>
                                        <div class="card">
                                                <table class="user-info-1">
                                                    <tr>
                                                            <td> Payment Method
                                                                </td>
                                                                <td>                                            <div class="dropdown payment-dropdown">
                                                                        <button class="btn btn-light dropdown-toggle" type="button"
                                                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                                            aria-expanded="false">
                                                                            Please select payment method
                                                                        </button>
                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                            <a class="dropdown-item" href="#">Please select payment method</a>
                                                                            <a class="dropdown-item" href="#">Bankwire</a>
                                                                            <a class="dropdown-item" href="#">Skrill/ Moneybookers</a>
                                                                        </div>
                                                                    </div></td>
                                                    </tr>
                                                    <tr>
                                                        <td> Tax Registered
                                                        </td>
                                                        <td><input type="text" class="tax_num" id="tax_num"></td>
                                                    </tr>
                                                    <tr>
                                                        <td> Vat Number
                                                        </td>
                                                        <td><label class="switch">
                                                                <input type="checkbox" value="vat_num" checked>
                                                                <span class="slider round"></span>
                                                              </label></td>
                                                    </tr>
                                                    <tr>
                                                            <td>
                                                            </td>
                                                            <td><button> Update</button></td>
                                                        </tr>
                                                    </table>
                                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        &copy; CopyRight Blockmatrix
    </footer>

    <script src="../assets/libs/js/admin.js?v=1.8"></script>
    <script type="text/javascript">
        function updateRevsharePercent()
        {
            var advValue = Number(document.getElementById("revshare-percent").value);
            var amsValue = 1;
            var totalVlaue = advValue + amsValue;
            document.getElementById("revshare-percent-total").value = totalVlaue;
        }

        function updateCPA()
        {
            var advValue = Number(document.getElementById("cpa-amount").value);
            var amsValue = 10;
            var totalVlaue = advValue + amsValue;
            document.getElementById("cpa-amount-total").value = totalVlaue;
        }

        function updateCPC()
        {
            var advValue = Number(document.getElementById("cpc-percentage").value);
            var amsValue = 10;
            var totalVlaue = advValue + amsValue;
            document.getElementById("cpc-percentage-total").value = totalVlaue;
        }

        function updateHybridAmount()
        {
            var advValue = Number(document.getElementById("hybrid-amount").value);
            var amsValue = 10;
            var totalVlaue = advValue + amsValue;
            document.getElementById("hybrid-amount-total").value = totalVlaue;
        }

        function updateHybridPercentage()
        {
            var advValue = Number(document.getElementById("hybrid-percentage").value);
            var amsValue = 1;
            var totalVlaue = advValue + amsValue;
            document.getElementById("hybrid-percentage-total").value = totalVlaue;
        }

        function chooseAffiliate(trackerId)
        {
            window.location.href = '/advertiser?page=affiliates-deal&id=' + trackerId;
        }
    </script>
</body>

</html>
