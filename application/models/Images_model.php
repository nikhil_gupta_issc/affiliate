<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Images_model extends CI_Model
{
    public function addImage($imageData)
    {
        $insertImage = $this->db->insert('images', $imageData);
        if($insertImage )
        {
            $lastInsertedId = $this->db->insert_id();
            $this->db->from('images');
            $this->db->where('id', $lastInsertedId);
            $result = $this->db->get();
            return $result->result_array();
        }
    
        return false;
    }

    public function viewImage($id)
    {
        // $query = $this->db->query('SELECT * FROM images ORDER BY id DESC LIMIT 1');
        // $row = $query->row_array();
        // return $row;
        $this->db->from('images');
        $this->db->where('id', $id);
        $result = $this->db->get();
        return $result->result_array();
        // $lastInsertedId = $this->db->insert_id();
        // $this->db->from('images');
        // $this->db->where('id', $lastInsertedId);
        // $result = $this->db->get();
        // return $result->result_array();
    }

    public function allImages()
    {
        $this->db->from('images');
        $result = $this->db->get();
        return $result->result_array();
    }
}
?>