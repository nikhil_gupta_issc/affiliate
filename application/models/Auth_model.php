<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    public function authenticate($username, $password)
    {
        $this->db->from('affiliate');
        $this->db->where('alias', $username);
        $this->db->where('passwd', md5($password));
        $result = $this->db->get();
        return $result->result_array();
    }

    public function addAffiliate($affiliateData)
    {
        $insertAffiliate = $this->db->insert('affiliate', $affiliateData);
        if($insertAffiliate)
        {
            $lastInsertedId = $this->db->insert_id();
            $this->db->from('affiliate');
            $this->db->where('affiliate_id', $lastInsertedId);
            $result = $this->db->get();
            return $result->result_array();
        }
        return false;
    }
}
?>
