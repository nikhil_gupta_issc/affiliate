<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tracker_model extends CI_Model
{
    public function addTracker($trackerData)
    {
        $insertTracker = $this->db->insert('affiliate_tracker', $trackerData);
        if($insertTracker)
        {
            $lastInsertedId = $this->db->insert_id();
            $this->db->from('affiliate_tracker');
            $this->db->where('tracker_id', $lastInsertedId);
            $result = $this->db->get();
            return $result->result_array();
        }
        return false;
    }

    public function updateTracker($trackerData, $trackerId)
    {
        $this->db->where('tracker_id', $trackerId);
        $this->db->update('affiliate_tracker', $trackerData);
    }

    public function getTrackersByAffiliateId($affiliateId)
    {
        $this->db->from('affiliate_tracker');
        $this->db->where('affiliate_id', $affiliateId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getTrackerById($trackerId)
    {
        $this->db->from('affiliate_tracker');
        $this->db->where('tracker_id', $trackerId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function addTrackersClick($trackerClickData)
    {
        $insertTracker = $this->db->insert('trackers_clicks', $trackerClickData);
    }

    public function getTrackersClicks($trackerId, $fromDate = '', $toDate = '')
    {
        $this->db->select('count(*) as count, page');
        $this->db->from('trackers_clicks');
        $this->db->where('tracker_id', $trackerId);
        if($fromDate)
        {
            $this->db->where('date >=', $fromDate);
        }
        if($toDate)
        {
            $this->db->where('date <=', $toDate);
        }
        $this->db->group_by('page');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getTrackersClicksByAffiliate($affiliateId)
    {
        $this->db->select('count(*) as count, page');
        $this->db->from('trackers_clicks');
        $this->db->where('affiliate_id', $affiliateId);
        $this->db->group_by('page');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getTrackersClicksByAdvertiser($advertiserId, $fromDate = '', $toDate = '')
    {
        $this->db->select('count(trackers_clicks.id) as count, trackers_clicks.page');
        $this->db->from('trackers_clicks');
        $this->db->join('affiliate_tracker', 'affiliate_tracker.tracker_id = trackers_clicks.tracker_id');
        $this->db->where('affiliate_tracker.affiliate_id', $advertiserId);
        if($fromDate)
        {
            $this->db->where('trackers_clicks.date >=', $fromDate);
        }
        if($toDate)
        {
            $this->db->where('trackers_clicks.date <=', $toDate);
        }
        $this->db->group_by('trackers_clicks.page');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getAffiliateGraphData($affiliateId, $pageName){
        $this->db->select('count(*) as count, date, page');
        $this->db->from('trackers_clicks');
        $this->db->where('affiliate_id', $affiliateId);
        $this->db->where('page', $pageName);
        $this->db->where('DATE(date) >= DATE(NOW()) - INTERVAL 30 DAY');
        $this->db->group_by('page, date');
        $result = $this->db->get();

        return $result->result_array();

    }

    public function getAffiliateMonthyGraphData($affiliateId, $pageName)
    {
        $this->db->select(' count(*) as count, monthname(date) as month, page');
        $this->db->from('trackers_clicks');
        $this->db->where('affiliate_id', $affiliateId);
        $this->db->where('page', $pageName);
        $this->db->where('DATE(date) >= DATE(NOW()) - INTERVAL 6 MONTH');
        $this->db->group_by('page, month(date)');
        $result = $this->db->get();
        return $result->result_array();
    }
    public function getAdvertisersOverviewData()
    {
        $result = $this->db->query("SELECT tc.affiliate_id, af.alias,(select count(*) from trackers_clicks where trackers_clicks.page = 'SIGNUP'and trackers_clicks.affiliate_id = tc.affiliate_id) as signup,
        (select count(*) from trackers_clicks where trackers_clicks.page = 'LENDING'and trackers_clicks.affiliate_id = tc.affiliate_id) as lending,
        (select count(*) from trackers_clicks where trackers_clicks.page = 'DEPOSIT'and trackers_clicks.affiliate_id = tc.affiliate_id) as deposit
          FROM zenfox.trackers_clicks tc inner join affiliate af on af.role_id = 2 and af.affiliate_id = tc.affiliate_id group by tc.affiliate_id");
        // $this->db->select('affiliate.alias, affiliate.affiliate_id, count(*) as count, trackers_clicks.page');
        // $this->db->from('trackers_clicks');
        // $this->db->join('affiliate', 'affiliate.role_id = 2 and affiliate.affiliate_id = trackers_clicks.affiliate_id', 'inner');
        // $this->db->group_by('trackers_clicks.page, affiliate.affiliate_id');
        // $result = $this->db->get();
        return $result->result_array();
    }
    public function getTrackersClicksByDateRange($affiliateId,$fromDate, $toDate)
    {
        $this->db->select('count(*) as count, page');
        $this->db->from('trackers_clicks');
        $this->db->where('affiliate_id', $affiliateId);
        $this->db->where('date >=', $fromDate);
        $this->db->where('date <=', $toDate);
        $this->db->group_by('page');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getAdvertisersOverviewByDateRange($fromDate, $toDate)
    {
        $result = $this->db->query("SELECT tc.affiliate_id, af.alias,(select count(*) from trackers_clicks where trackers_clicks.page = 'SIGNUP'and
        trackers_clicks.affiliate_id = tc.affiliate_id and trackers_clicks.date BETWEEN '$fromDate' AND '$toDate') as signup,
        (select count(*) from trackers_clicks where trackers_clicks.page = 'LENDING'and trackers_clicks.affiliate_id = tc.affiliate_id
         and trackers_clicks.date BETWEEN '$fromDate' AND '$toDate') as lending,
        (select count(*) from trackers_clicks where trackers_clicks.page = 'DEPOSIT'and trackers_clicks.affiliate_id = tc.affiliate_id
         and trackers_clicks.date BETWEEN '$fromDate' AND '$toDate') as deposit
          FROM trackers_clicks tc inner join affiliate af on af.role_id = 2 and af.affiliate_id = tc.affiliate_id group by tc.affiliate_id;");

        return $result->result_array();
    }

    public function getAllTrackers($affiliateId, $trackerId = NULL)
    {
        $this->db->from('affiliate_tracker');
        if($affiliateId)
        {
            $this->db->where('affiliate_id', $affiliateId);
        }
        if($trackerId)
        {
            $this->db->where('tracker_id', $trackerId);
        }
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getCampaignsByAffiliate($affiliateId)
    {
        $this->db->from('campaigns');
        $this->db->join('affiliate_tracker', 'affiliate_tracker.tracker_id = campaigns.tracker_id');
        $this->db->join('affiliate', 'affiliate.affiliate_id = affiliate_tracker.affiliate_id');
	    $this->db->where('campaigns.affiliate_id', $affiliateId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getCampaignsByAdvertiser($advertiserId)
    {
        $this->db->from('campaigns');
        $this->db->join('affiliate_tracker', 'affiliate_tracker.tracker_id = campaigns.tracker_id');
        $this->db->join('affiliate', 'affiliate.affiliate_id = campaigns.affiliate_id');
	    $this->db->where('affiliate_tracker.affiliate_id', $advertiserId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getAllAffiliates($excludedIds)
    {
        $this->db->from('affiliate');
        $this->db->where('role_id', 1);
        if($excludedIds)
        {
            $this->db->where_not_in('affiliate_id', $excludedIds);
        }
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getAllAdvertisers($excludedIds)
    {
        $this->db->from('affiliate');
        $this->db->where('role_id', 2);
        if($excludedIds)
        {
            $this->db->where_not_in('affiliate_id', $excludedIds);
        }
        $result = $this->db->get();
        return $result->result_array();
    }

    public function addCampaign($campaignData)
    {
        $insertCampaign = $this->db->insert('campaigns', $campaignData);
    }

    public function addRequest($requestData)
    {
        $insertRequest = $this->db->insert('advertisers_requests', $requestData);
    }

    public function addDeal($requestData)
    {
        $insertRequest = $this->db->insert('trackers_requests', $requestData);
    }

    public function getRequestsByAdvertiser($advertiserId)
    {
        $this->db->from('advertisers_requests');
        $this->db->join('affiliate', 'affiliate.affiliate_id = advertisers_requests.affiliate_id');
        $this->db->where('advertisers_requests.advertiser_id', $advertiserId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getDealRequestsById($dealId)
    {
        $this->db->from('trackers_requests');
        $this->db->join('affiliate', 'affiliate.affiliate_id = trackers_requests.affiliate_id');
        $this->db->where('trackers_requests.tracker_id', $dealId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getRequestsByDeal($dealId)
    {
        $this->db->from('trackers_requests');
        $this->db->join('affiliate', 'affiliate.affiliate_id = trackers_requests.affiliate_id');
        $this->db->where('trackers_requests.tracker_id', $dealId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getRequestsByAffiliate($affiliateId)
    {
        $this->db->from('advertisers_requests');
        $this->db->join('affiliate', 'affiliate.affiliate_id = advertisers_requests.advertiser_id');
        $this->db->where('advertisers_requests.affiliate_id', $affiliateId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getDealsByAffiliate($affiliateId)
    {
        $this->db->from('trackers_requests');
        $this->db->join('affiliate_tracker', 'affiliate_tracker.tracker_id = trackers_requests.tracker_id');
        $this->db->join('affiliate', 'affiliate.affiliate_id = affiliate_tracker.affiliate_id');
        $this->db->where('trackers_requests.affiliate_id', $affiliateId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getLendingClicksByAffiliate($affiliateId)
    {
        $this->db->select('count(*) as count, trackers_clicks.tracker_id AS tracker_id');
        $this->db->from('trackers_clicks');
        $this->db->where('affiliate_id', $affiliateId);
        $this->db->where('page', 'LENDING');
        $this->db->group_by('trackers_clicks.tracker_id');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getCampaignsCountByAffiliate($affiliateId)
    {
        $this->db->select('count(*) as count');
        $this->db->from('campaigns');
	    $this->db->where('affiliate_id', $affiliateId);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function addNegotiation($negotiationData)
    {
        $insertRequest = $this->db->insert('commission_negotiation', $negotiationData);
    }

    public function updateNegotiation($negotiationId, $negotiationData)
    {
        $this->db->where('id', $negotiationId);
        $this->db->update('commission_negotiation', $negotiationData);
    }

    public function getLastNegotiation($affiliateId, $trackerId)
    {
        $this->db->from('commission_negotiation');
	    $this->db->where('affiliate_id', $affiliateId);
        $this->db->where('tracker_id', $trackerId);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getLastNegotiationByTracker($trackerId)
    {
        $this->db->select('*, commission_negotiation.id AS negotiation_id');
        $this->db->from('commission_negotiation');
        $this->db->join('affiliate', 'affiliate.affiliate_id = commission_negotiation.affiliate_id');
        $this->db->where('commission_negotiation.tracker_id', $trackerId);
        $this->db->order_by('commission_negotiation.id', 'desc');
        $result = $this->db->get();
        return $result->result_array();
    }
}
?>
