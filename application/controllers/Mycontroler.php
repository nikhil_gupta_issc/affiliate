<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mycontroler extends CI_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
		$this->load->model(array('tracker_model'));
        if(!$this->session->user)
        {
            redirect('/');
        }
        $this->data = array(
            'title' => 'Page Title',
            'userData' => $this->session->user,
            'css' => $this->config->item('css')
        );
    }

    public function index()
    {
        $data = $this->data;
        $this->load->view('myview', $data);
        
    }
}
?>