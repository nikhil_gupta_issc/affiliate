<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
	 public function __construct()
	 {
		 parent::__construct();
		 $this->load->model(array('auth_model'));
		 $this->load->helper('url');
	 }

	 public function index()
	 {
		 $this->load->view('welcome_message');
	 }

	 public function signup()
	 {
		$data = $formData = array();

		 if($_POST)
		 {
			$formData = $this->input->post();

            $this->form_validation->set_rules('first_name', 'FirstName', 'required');
			$this->form_validation->set_rules('last_name', 'LastName', 'required');
			$this->form_validation->set_rules('company', 'Company', 'required');
			$this->form_validation->set_rules('address', 'Address', 'required');
			$this->form_validation->set_rules('city', 'City', 'required');
			$this->form_validation->set_rules('state', 'State', 'required');
			$this->form_validation->set_rules('country', 'Country', 'required');
			$this->form_validation->set_rules('pincode', 'Pincode', 'required');
			$this->form_validation->set_rules('email','Email','required|valid_email'); //|is_unique[users.email
			$this->form_validation->set_rules('phone', 'Mobile Number', 'required|regex_match[/^[0-9]{10}$/]');
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
			$this->form_validation->set_rules('payment_type','User payment_type','required');
			$this->form_validation->set_rules('whatyoudo','What You Do','required');
			$this->form_validation->set_rules('terms_n_conditions','Terms and Conditions','trim|required|greater_than[0]');

			$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

			if($this->form_validation->run() == true){
				$affiliateData['alias'] = $_POST['username'];
				$affiliateData['passwd'] = md5($_POST['password']);
				$affiliateData['firstname'] = $_POST['first_name'];
				$affiliateData['lastname'] = $_POST['last_name'];
				$affiliateData['email'] = $_POST['email'];
				$affiliateData['company'] = $_POST['company'];
				$affiliateData['address'] = $_POST['address'];
				$affiliateData['city'] = $_POST['city'];
				$affiliateData['state'] = $_POST['state'];
				$affiliateData['country'] = $_POST['country'];
				$affiliateData['zip'] = $_POST['pincode'];
				$affiliateData['phone'] = $_POST['phone'];
				$affiliateData['role_id'] = $_POST['whatyoudo'];
				$affiliateData['payment_type'] = $_POST['payment_type'];

				$insertAffiliate = $this->auth_model->addAffiliate($affiliateData);
				if($insertAffiliate)
				{
					$this->session->set_userdata('user', $insertAffiliate[0]);
					switch($insertAffiliate[0]['role_id'])
					{
						case 1:
							redirect('/affiliate');
							break;
						case 2:
							redirect('/advertiser');
							break;
					}
				}
			}

		 }
		 $this->load->view('signup');
	 }

	 public function logout()
	 {
		 $this->session->unset_userdata('user');
		 $this->load->helper('url');
		 redirect('/');
	 }
}
