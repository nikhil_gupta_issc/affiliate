<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('auth_model'));
		$this->load->library('session');
		$this->load->helper('url');

		// if($this->session->user)
		// {
		// 	redirect('/dashboard');
		// }

	}

	public function index()
	{
		if($_POST)
		{
			$username = $_POST['username'];
			$password = $_POST['password'];

			$authenticate = $this->auth_model->authenticate($username, $password);
			if($authenticate)
			{
				$this->session->set_userdata('user', $authenticate[0]);
				switch($authenticate[0]['role_id']){
					case 1:
						redirect('/affiliate');
						break;
					case 2:
						redirect('/advertiser');
						break;
				}
			}
			else {
				$this->session->set_flashdata('Login_failed','Invalid Username/Password');
      			return redirect('login');
			}
		}

		$this->load->view('welcome_message');
	}
}
