<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tracker extends CI_Controller
{
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct()
	 {
		 parent::__construct();
		 $this->load->helper('url');
		 $this->load->model(array('tracker_model'));

		 if(!$this->session->user)
		 {
			 redirect('/');
		 }
	 }

	 public function create()
	 {
		 if($_POST)
		 {
			 $trackerData['tracker_type'] = $_POST['tracker_type'];
			 $trackerData['tracker_name'] = $_POST['tracker_name'];
			 $trackerData['tracker_model'] = $_POST['tracker_model'];
			 $trackerData['commission_type'] = $_POST['commission_type'];
			 $trackerData['commission'] = $_POST['commission'];
			 $trackerData['affiliate_id'] = $this->session->user['affiliate_id'];
			 $trackerData['scheme_id'] = 1;

			 $insertTracker = $this->tracker_model->addTracker($trackerData);
			 if($insertTracker)
			 {
				 redirect('/banner?tid=' . $insertTracker[0]['tracker_id']);
			 }
		 }
		 $this->load->view('tracker_create', $this->session->user);
	 }

	 public function view()
	 {
		 $affiliateId = $this->session->user['affiliate_id'];
		 $affiliateTrackers = $this->tracker_model->getTrackersByAffiliateId($affiliateId);

		 $data['userData'] = $this->session->user;
		 $data['trackers'] = $affiliateTrackers;
		 $this->load->view('tracker_view', $data);
	 }

	 public function campaign()
	 {
		 $trackerId = $this->input->post('tracker_id');
		 $affiliateId = $this->session->user['affiliate_id'];

		 if($this->input->post('proposal'))
		 {
			 //$trackerDetail = $this->tracker_model->getTrackerById($trackerId);
			 $negotiationData['affiliate_id'] = $affiliateId;
			 $negotiationData['tracker_id'] = $trackerId;
			 $negotiationData['prev_commission'] = $this->input->post('prev_commission');
			 $negotiationData['new_commission'] = $this->input->post('my_offer');
			 $negotiationData['prev_percentage_hybrid'] = $this->input->post('prev_percentage_hybrid');
			 $negotiationData['new_percentage_hybrid'] = $this->input->post('my_offer_percentage');
			 $this->tracker_model->addNegotiation($negotiationData);
		 }
		 else
		 {
			 $campaignData['affiliate_id'] = $affiliateId;
			 $campaignData['tracker_id'] = $trackerId;
			 $campaignData['enabled'] = 1;

			 $this->tracker_model->addCampaign($campaignData);
		 }
		 redirect('/affiliate?page=campaign');
	 }

	 public function negotiation()
	 {
		 $negotiationId = $this->input->post('negotiation_id');

		 $negotiationData['adv_commission'] = $this->input->post('adv_commission');
		 $negotiationData['adv_percentage_hybrid'] = $this->input->post('adv_percentage_hybrid');
		 $this->tracker_model->updateNegotiation($negotiationId, $negotiationData);
		 redirect('/advertiser');
	 }
}
