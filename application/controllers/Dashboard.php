<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	 public function __construct()
	 {
		 parent::__construct();
		 $this->load->helper('url');
		 $this->load->model(array('tracker_model'));

		 if(!$this->session->user)
		 {
			 redirect('/');
		 }
	 }

	 public function index()
	 {
		 $yesterday_date = date('Y-m-d', strtotime("-1 days"));
		 $one_week_before_date = date('Y-m-d', strtotime("-7 days"));

		 $affiliateId = $this->session->user['affiliate_id'];
		 $affiliateTrackers = $this->tracker_model->getTrackersByAffiliateId($affiliateId);
		 $trackersClicks = $this->tracker_model->getTrackersClicksByDateRange($one_week_before_date, $yesterday_date);
		 $totalEarning = 0;
		 if($affiliateTrackers)
		 {
			 foreach($affiliateTrackers as $index => $trackerDetail)
			 {
				 $totalEarning += $trackerDetail['earning'];
			 }

			 foreach($affiliateTrackers as $index => $trackerDetail)
			 {
				 $trackerId = $trackerDetail['tracker_id'];
				 $totalClicks = 0;
				 $revenueString = '';
				 $clicksDate = array();
				 $clickIndex = 0;
				 if($trackersClicks)
				 {
					 foreach($trackersClicks as $click)
					 {
						 if($click['tracker_id'] == $trackerId)
						 {
							 $totalClicks += $click['no_of_clicks'];
							 $revenueString .= $click['no_of_clicks'] . ',';
							 $clicksDate['date'][$clickIndex] = $click['date'];
							 $clicksDate['clicks'][$clickIndex] = $click['no_of_clicks'];
							 $clickIndex++;
						 }
					 }
				 } else {
					$totalClicks = 0;
					$revenueString = '';
					$clicksDate = array();
					$clickIndex = 0;
				 }
				 $affiliateTrackers[$index]['clicks'] = $totalClicks;
				 $affiliateTrackers[$index]['revenueString'] = $revenueString ? substr($revenueString, 0, -1) : '';
				 $totalEarning = $totalEarning ? $totalEarning : 1;
				 $affiliateTrackers[$index]['earning'] = number_format($affiliateTrackers[$index]['earning'] * 100 / $totalEarning, 2);
				 $affiliateTrackers[$index]['click_date'] = $clicksDate;

				 /*$curl = curl_init();
				 curl_setopt_array($curl, array(
					 CURLOPT_URL => "http://affiliate.duoex.com/analytics/report.php?page=?tracker_id=" . $trackerId,
					 CURLOPT_RETURNTRANSFER => true,
					 CURLOPT_CUSTOMREQUEST => "GET",
					 CURLOPT_HTTPHEADER => array(
						 "Cache-Control: no-cache",
						 "Content-Type: application/json"
					 ),
				 ));

				 $response = curl_exec($curl);
				 $err = curl_error($curl);
				 curl_close($curl);
				 if ($err)
				 {
					 echo "cURL Error #:" . $err;
				 }
				 else
				 {
					 $clicks = json_decode($response, true);
					 $affiliateTrackers[$index]['clicks'] = $clicks['count'];
				 }*/
			 }
		 }

		 $data['userData'] = $this->session->user;
		 $data['trackers'] = $affiliateTrackers;
		 $data['jsonTrackers'] = json_encode($affiliateTrackers);
		 $data['trackersCount'] = count($affiliateTrackers);
		 $data['totalEarning'] = $totalEarning;
		 //print_r($data);
		 $this->load->view('dashboard', $data);
	 }
}
