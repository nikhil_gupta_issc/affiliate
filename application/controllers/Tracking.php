<?php
class Tracking extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('tracker_model'));
    }

    public function index()
    {
        $trackerClickData['tracker_id'] = $_GET['tid'];
        $trackerClickData['affiliate_id'] = $_GET['aid'];
        $trackerClickData['no_of_clicks'] = 1;
        $trackerClickData['page'] = $_GET['page'];
        $trackerClickData['date'] = date('Y-m-d');

        $this->tracker_model->addTrackersClick($trackerClickData);
    }
}
?>
