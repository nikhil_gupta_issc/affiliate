<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Affiliate extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        if(!$this->session->user)
        {
            redirect('/');
        }

        $this->load->model(array('tracker_model'));
    }

    public function index()
    {
        $advertiserId = $this->input->get('adv_id');
        $dealId = $this->input->get('d_id');
        $data['userData'] = $this->session->user;
        $data['page_title'] = "Welcome to New Dashboard Home Page";
        $data['payments'] = $this->get_payments();
        $data['affiliate_manager'] = array('first_name'=>'Nikhil Kumar', 'last_name'=>'Gupta',
         'skyp_id'=>'nikhil.kumar', 'email'=>'nikhil.kumar@blockmatrix.com');
        $data['advertiser_id'] = $advertiserId;

        $affiliateId = $this->session->user['affiliate_id'];
        $allTrackers = $this->tracker_model->getAllTrackers($advertiserId, $dealId);
        $allCampaigns = $this->tracker_model->getCampaignsByAffiliate($affiliateId);
        $allClicks = $this->tracker_model->getTrackersClicksByAffiliate($affiliateId);
        $allRequests = $this->tracker_model->getRequestsByAffiliate($affiliateId);
        $allDeals = $this->tracker_model->getDealsByAffiliate($affiliateId);
        $data['advertisers_requests'] = $allRequests;
        $data['deals_requests'] = $allDeals;

        $totalClicks = 0;
        $totalSignups = 0;
        $totalDeposits = 0;

        if($allClicks)
        {
            foreach($allClicks as $click)
            {
                switch($click['page'])
                {
                    case 'LENDING':
                        $totalClicks += $click['count'];
                        break;
                    case 'SIGNUP':
                        $totalSignups += $click['count'];
                        break;
                    case 'DEPOSIT':
                        $totalDeposits += $click['count'];
                        break;
                }
            }
        }

        $totalAdvertisers = array();
        $newAdvertisers = array();
        $allAdvertisers = $this->tracker_model->getAllAdvertisers(array());
        if($allCampaigns)
        {
            foreach($allCampaigns as $campaign)
            {
                if(!in_array($campaign['affiliate_id'], $totalAdvertisers))
                {
                    $totalAdvertisers[] = $campaign['affiliate_id'];
                    if(date('m', strtotime($campaign['created'])) == date('m'))
                    {
                        $newAdvertisers[] = $campaign['affiliate_id'];
                    }
                }
            }
        }

        $date_1 = strtotime(date('Y-m-d'));
        $date_2 = strtotime($data['userData']['created']);

        $daysDiff = floor(($date_1 - $date_2) / (24*60*60));

        $currentYear = date('Y');
        $currentMonth = date('m');

        $affiliateRegisteredYear = date('Y', strtotime($data['userData']['created']));
        $affiliateRegisteredMonth = date('m', strtotime($data['userData']['created']));

        $yearDiff = $currentYear - $affiliateRegisteredYear;
        $monthDiff = $currentMonth - $affiliateRegisteredMonth;

        $totalMonthDiff = $yearDiff * 12 + $monthDiff;
        $earnings = 0;

        if($allTrackers)
        {
            foreach($allTrackers as $index => $tracker)
            {
                $trackerClicks = $this->tracker_model->getTrackersClicks($tracker['tracker_id']);
                $lastNegotiation = $this->tracker_model->getLastNegotiation($affiliateId, $tracker['tracker_id']);
                $tracker['clicks'] = $trackerClicks;
                $tracker['negotiations'] = $lastNegotiation ? $lastNegotiation[0] : array();
                $allTrackers[$index] = $tracker;

                if($allCampaigns)
                {
                    foreach($allCampaigns as $index => $campaign)
                    {
                        if($campaign['tracker_id'] == $tracker['tracker_id'])
                        {
                            $campaign['negotiations'] = $lastNegotiation ? $lastNegotiation[0] : array();
                            $allCampaigns[$index] = $campaign;
                        }
                    }
                }

                switch($tracker['deal_type'])
                {
                    case 'REVENUE_SHARE':
                        if($trackerClicks)
                        {
                            foreach($trackerClicks as $index => $value)
                            {
                                if($value['page'] == 'DEPOSIT')
                                {
                                    $earnings += $value['count'] * $tracker['commission'];
                                }
                            }
                        }
                        break;
                    case 'CPC':
                        if($trackerClicks)
                        {
                            foreach($trackerClicks as $index => $value)
                            {
                                if($value['page'] == 'LENDING')
                                {
                                    $earnings += $value['count'] * $tracker['commission'];
                                }
                            }
                        }
                        break;
                    case 'CPA':
                        if($trackerClicks)
                        {
                            foreach($trackerClicks as $index => $value)
                            {
                                if($value['page'] == $tracker['cpa_action'])
                                {
                                    $earnings += $value['count'] * $tracker['commission'];
                                }
                            }
                        }
                        break;
                    case 'HYBRID':
                        $totalDeposits += $click['count'];
                        break;
                }
            }
        }

        $avgMonthEarnings = 0;
        $profitPerDay = 0;

        if($monthDiff)
        {
            $avgMonthEarnings = $earnings / $monthDiff;
        }
        if($daysDiff)
        {
            $profitPerDay = $earnings / $daysDiff;
        }

        $affiliateGraphData['SIGNUPS'] = $this->tracker_model->getAffiliateGraphData($affiliateId, 'SIGNUP');
        $affiliateGraphData['LENDINGS'] = $this->tracker_model->getAffiliateGraphData($affiliateId, 'LENDING');
        $affiliateGraphData['DEPOSITS'] = $this->tracker_model->getAffiliateGraphData($affiliateId, 'DEPOSIT');
        $affiliateMonthlyGraphData['LENDING'] = $this->tracker_model->getAffiliateMonthyGraphData($affiliateId, 'LENDING');
        $affiliateMonthlyGraphData['SIGNUP'] = $this->tracker_model->getAffiliateMonthyGraphData($affiliateId, 'SIGNUP');
        $affiliateMonthlyGraphData['DEPOSIT'] = $this->tracker_model->getAffiliateMonthyGraphData($affiliateId, 'DEPOSIT');

        $sg_data = $this->format_data_for_graph($affiliateGraphData['SIGNUPS']);
        $lg_data = $this->format_data_for_graph($affiliateGraphData['LENDINGS']);
        $dg_data = $this->format_data_for_graph($affiliateGraphData['DEPOSITS']);

        $lg_monthly_data = $this->format_data_for_monthly_graph($affiliateMonthlyGraphData['LENDING']);
        $sg_monthly_data = $this->format_data_for_monthly_graph($affiliateMonthlyGraphData['SIGNUP']);
        $dg_monthly_data = $this->format_data_for_monthly_graph($affiliateMonthlyGraphData['DEPOSIT']);

        $data['advertisers'] = $this->tracker_model->getAdvertisersOverviewData();

        $data['trackers'] = $allTrackers;
        $data['campaigns'] = $allCampaigns;
        $data['total_clicks'] = $totalClicks + $totalSignups + $totalDeposits;
        $data['total_signups'] = $totalSignups;
        $data['total_deposits'] = $totalDeposits;
        $data['total_advertisers'] = count($totalAdvertisers);
        $data['new_advertisers'] = count($newAdvertisers);
        $data['avg_month_earning'] = $avgMonthEarnings;
        $data['total_earnings'] = $earnings;
        $data['profit_per_day'] = $profitPerDay;
        $data['all_advertisers'] = $allAdvertisers;
        $data['signup_to_deposit'] = 10;

        $data['signup_grpah'] = json_encode($sg_data);
        $data['deposits_grpah'] = json_encode($dg_data);
        $data['lindings_grpah'] = json_encode($lg_data);
        $data['lendings_monthly_grpah'] = json_encode($lg_monthly_data);
        $data['signup_monthly_grpah'] = json_encode($sg_monthly_data);
        $data['deposit_monthly_grpah'] = json_encode($dg_monthly_data);

        $this->load->view('affiliate', $data);
    }

    public function format_data_for_graph($graphdata_array)
    {
        $temp_array = [];
        foreach($graphdata_array as $row) {
            $temp_array['label'][] = $row['date'];
            $temp_array['data'][] = (int) $row['count'];
        }
        return $temp_array;
    }

    public function format_data_for_monthly_graph($monthly_graphdata_array)
    {
        $temp_array = [];
        foreach($monthly_graphdata_array as $row) {
            $temp_array['label'][] = $row['month'];
            $temp_array['data'][] = (int) $row['count'];
        }
        return $temp_array;
    }

    public function get_advertisers($strat_date = null, $end_date = null)
    {
        $advertisers = array(
            array('name'=>'Advertiser 1', 'clicks'=> 500, 'signups'=>250, 'first_time_deposit'=>5000, 'rev_payout'=>300, 'cost_per_action'=>2, 'cpa_payout'=>150, 'cpl_payout'=>350, 'total_payout'=>800),
            array('name'=>'Advertiser 2', 'clicks'=> 300, 'signups'=>450, 'first_time_deposit'=>2000, 'rev_payout'=>600, 'cost_per_action'=>1, 'cpa_payout'=>350, 'cpl_payout'=>150, 'total_payout'=>700),
            array('name'=>'Advertiser 3', 'clicks'=> 600, 'signups'=>750, 'first_time_deposit'=>7000, 'rev_payout'=>400, 'cost_per_action'=>3, 'cpa_payout'=>250, 'cpl_payout'=>450, 'total_payout'=>900),
        );

        return $advertisers;
    }

    public function get_payments()
    {
        $payments = array(
            array('month'=>'01-07-2019', 'amount'=>'5000', 'ams_commission' => '1000', 'final_amount' => '4000', 'status'=>'Paid'),
            array('month'=>'01-08-2019', 'amount'=>'6000', 'ams_commission' => '1500', 'final_amount' => '4500', 'status'=>'Due'),
            array('month'=>'01-09-2019', 'amount'=>'7000', 'ams_commission' => '2000', 'final_amount' => '5000', 'status'=>'Paid')
        );

        return $payments;
    }

    public function getstatsbytimeintervel()
    {
        $affiliateId = $this->session->user['affiliate_id'];
        $data = array();
        if($_POST){
            $from_date = $_POST['from_date'];
            $to_date = $_POST['to_date'];
            $result_array = $this->tracker_model->getTrackersClicksByDateRange($affiliateId, $from_date, $to_date);
            if($result_array){
                $overview_data = $this->calculate_overview_counts($result_array);
                $data['overview_stats'] = $overview_data;
            }else{
                $data['error_overview_stats'] = "Data not found for selected Date range";
            }

            $advertisers_by_timeperiod = $this->tracker_model->getAdvertisersOverviewByDateRange($from_date, $to_date);
            if($advertisers_by_timeperiod){
                $data['advertisers'] = $advertisers_by_timeperiod;
            }
            else
            $data['error_advertisers'] = "Advertisers Data not found on this time Inetervel";
        }
        else
            $data['error_post_error'] = 'it is a bad luck ... not what you want...';

        echo json_encode($data);


    }

    public function calculate_overview_counts($allClicks){
        $overview_couters = array('totalClicks'=> 0, 'totalSignups'=> 0, 'totalDeposits'=> 0 );
        if($allClicks)
        {
            foreach($allClicks as $click)
            {
                switch($click['page'])
                        {
                            case 'LENDING':
                                $overview_couters['totalClicks'] += $click['count'];
                                break;
                            case 'SIGNUP':
                                $overview_couters['totalSignups'] += $click['count'];
                                break;
                            case 'DEPOSIT':
                                $overview_couters['totalDeposits'] += $click['count'];
                                break;
                        }
            }
        }

        return $overview_couters;
    }

}
?>
