<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
class Sitehome extends CI_Controller
{
    public function __construct()
	 {
		 parent::__construct();
         if($this->session->user)
         {
             $userData = $this->session->user;
             switch($userData['role_id']){
                 case 1:
                     redirect('/affiliate');
                     break;
                 case 2:
                     redirect('/advertiser');
                     break;
             }
         }
         $this->load->helper('url');
         $this->load->library('form_validation');

     }

     public function index(){
        $data = $formData = array();

        if($this->input->post('contactSubmit')){
            $formData = $this->input->post();

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('subject', 'Subject', 'required');
            $this->form_validation->set_rules('message', 'Message', 'required');

            if($this->form_validation->run() == true){

                $mailData = array(
                    'name' => $formData['name'],
                    'email' => $formData['email'],
                    'subject' => $formData['subject'],
                    'message' => $formData['message']
                );

                $send = $this->sendEmail($mailData);
                //$send = true;

                if($send){
                    // Unset form data
                    $formData = array();

                    $data['status'] = array(
                        'type' => 'success',
                        'msg' => 'Your contact request has been submitted successfully.'
                    );
                }else{
                    $data['status'] = array(
                        'type' => 'error',
                        'msg' => 'Some problems occured, please try again.'
                    );
                }

            }

        }
        $data['postData'] = $formData;
        $this->load->view('site_home', $data);

     }

     private function sendEmail($mailData){

        $this->load->library('email');

        // Mail config
        $to = 'support@blocmatrix.com';
        $from = 'support@blocmatrix.com';
        $fromName = 'Blockmatrix-Affiliate';
        $mailSubject = 'Contact Request Submitted by '.$mailData['name'];

        // Mail content
        $mailContent = '
            <h2>Contact Request Submitted</h2>
            <p><b>Name: </b>'.$mailData['name'].'</p>
            <p><b>Email: </b>'.$mailData['email'].'</p>
            <p><b>Subject: </b>'.$mailData['subject'].'</p>
            <p><b>Message: </b>'.$mailData['message'].'</p>
        ';

        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->to($to);
        $this->email->from($from, $fromName);
        $this->email->subject($mailSubject);
        $this->email->message($mailContent);

        // Send email & return status
        return $this->email->send()?true:false;
    }
}

?>
