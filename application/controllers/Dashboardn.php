<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboardn extends CI_Controller {
    public function __construct()
    {
        parent:: __construct();
        if(!$this->session->user)
        {
            redirect('/login');
        }
        
    }

    public function index()
    {
        $data['userData'] = $this->session->user;
        $data['page_title'] = "Welcome to New Dashboard Home Page";
        $data['home_overview'] =  array('clicks' => 250, 'signups' => 75,
         'fist_time_deposit'=>5000, 'cost_per_action'=>1, 'signup_to_deposit'=>10, 'profit_per_day'=>75 );
        $data['affiliate_manager'] = array('first_name'=>'Nikhil Kumar', 'last_name'=>'Gupta',
         'skyp_id'=>'nikhil.kumar', 'email'=>'nikhil.kumar@blockmatrix.com');
        $data['advertisers'] = $this->get_advertisers();
        $data['payments'] = $this->get_payments();
        $this->load->view('dashboardn', $data);
    }

    public function get_advertisers($strat_date = null, $end_date = null)
    {
        $advertisers = array(
            array('name'=>'Advertiser 1', 'clicks'=> 500, 'signups'=>250, 'first_time_deposit'=>5000, 'rev_payout'=>300, 'cost_per_action'=>2, 'cpa_payout'=>150, 'cpl_payout'=>350, 'total_payout'=>800),
            array('name'=>'Advertiser 2', 'clicks'=> 300, 'signups'=>450, 'first_time_deposit'=>2000, 'rev_payout'=>600, 'cost_per_action'=>1, 'cpa_payout'=>350, 'cpl_payout'=>150, 'total_payout'=>700),
            array('name'=>'Advertiser 3', 'clicks'=> 600, 'signups'=>750, 'first_time_deposit'=>7000, 'rev_payout'=>400, 'cost_per_action'=>3, 'cpa_payout'=>250, 'cpl_payout'=>450, 'total_payout'=>900),
        );

        return $advertisers;
    }

    public function get_payments()
    {
        $payments = array(
            array('period'=>'23-08-2019', 'created'=>'20-08-2019', 'Amount'=>'5000','status'=>'Paid'),
            array('period'=>'04-08-2019', 'created'=>'02-08-2019', 'Amount'=>'5000','status'=>'Due'),
            array('period'=>'03-08-2019', 'created'=>'01-08-2019', 'Amount'=>'5000','status'=>'Paid'),
        );

        return $payments;
    }
}
?>