<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demographs extends CI_Controller
{
    public function __construct()
	 {
		 parent::__construct();
		 if(!$this->session->user)
		 {
			 redirect('/');
		 }
     }
    public function index(){

        $data = array();

        $lables = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul','Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $dataset_one = [2, 3, 4, 1, 2, 3, 2, 3, 4, 1, 1, 3];
        $dataset_two = [4, 6, 8, 2, 4, 6, 4, 6, 8, 2, 4, 6];

        $data['impressions_graph'] = json_encode(array('lables'=> $lables, 'dataset_one'=> $dataset_one, 'dataset_two'=> $dataset_two));
        $data['clicks_graph'] = json_encode(array('lables'=> $lables, 'dataset_one'=> $dataset_one, 'dataset_two'=> $dataset_two));
        $data['actions_graph'] = json_encode(array('lables'=> $lables, 'dataset_one'=> $dataset_one, 'dataset_two'=> $dataset_two));
        $this->load->view('demographs', $data);
    }
}