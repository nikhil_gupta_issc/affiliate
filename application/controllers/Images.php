<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('Images_model');

        if(!$this->session->user)
		 {
			 redirect('/');
         }
        $data['userData'] = $this->session->user;
    }

	public function upload(){
        $data['userData'] = $this->session->user;
        $data['error'] = "";
        $this->load->view('image_upload', $data);
    }
    public function do_upload(){
        $data['userData'] = $this->session->user;
        if($_POST){
            $imageData['title'] = $_POST['title'];
            $imageData['description'] = $_POST['description'];
            $config = array(
                'upload_path' => './uploads/',
                'allowed_types' => 'gif|jpg|png|jpeg',
                'overwrite' => TRUE
            );
            $this->load->library('upload', $config);
            if($this->upload->do_upload('imageToUpload') &&($imageData['title'] != '')
             && ($imageData['description'] != ''))
            {
                $uplodedImage = array('upload_data' => $this->upload->data());
                $imageData['url'] = $this->upload->data('file_name');
                $insertImage = $this->Images_model->addImage($imageData);
                if($insertImage)
                {
                    $this->view($insertImage[0]['id']);
                    //redirect('/img/' . $insertImage[0]['id']);
                }

            }
            else
            {
                //$data['error'] = array('file_error' => $this->upload->display_errors(), 'filed_errors' => "All Fileds are requiered" );
                //print_r($data);
                echo '<script> alert("Some thing went worng..."); </script>';
                $this->load->view('image_upload', $data);
            }

        }


    }
	 public function view($slug = null)
	 {
         //$dataImage = $this->Images_model->viewImage($slug);
         $data['userData'] = $this->session->user;
         $data['image'] = $this->Images_model->viewImage($slug);
         if(empty($data['image'])){
             show_404();
         }

		 $this->load->view('image_view', $data);
     }

     public function index(){
        $data['userData'] = $this->session->user;
        $data['allImages'] = $this->Images_model->allImages();
        // echo '<pre>';
        // print_r($data['allImages']);
        $this->load->view('images_all', $data);
     }
}
