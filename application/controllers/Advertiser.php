<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advertiser extends CI_Controller
{
	 public function __construct()
	 {
		 parent::__construct();
		 if(!$this->session->user)
		 {
			 redirect('/');
		 }
		 $this->load->model(array('tracker_model'));
	 }

	 public function index()
	 {
         $advertiserDetail = $this->session->user;
		 $affiliateId = $advertiserDetail['affiliate_id'];
		 $trackers = $this->tracker_model->getTrackersByAffiliateId($affiliateId);
		 $myCampaigns = $this->tracker_model->getCampaignsByAdvertiser($affiliateId);

		 $allTrackers = $this->tracker_model->getAllTrackers($affiliateId);

		 $date_1 = strtotime(date('Y-m-d'));
         $date_2 = strtotime($advertiserDetail['created']);

         $daysDiff = floor(($date_1 - $date_2) / (24*60*60));

         $currentYear = date('Y');
         $currentMonth = date('m');

         $affiliateRegisteredYear = date('Y', strtotime($advertiserDetail['created']));
         $affiliateRegisteredMonth = date('m', strtotime($advertiserDetail['created']));

         $yearDiff = $currentYear - $affiliateRegisteredYear;
         $monthDiff = $currentMonth - $affiliateRegisteredMonth;

         $totalMonthDiff = $yearDiff * 12 + $monthDiff;
         $earnings = 0;

		 $fromDate = date('Y-m') . '-1';
		 $toDate = date('Y-m-d');
		 $finalNegotiations = array();

		 if($allTrackers)
         {
             foreach($allTrackers as $index => $tracker)
             {
                 $trackerClicks = $this->tracker_model->getTrackersClicks($tracker['tracker_id'], $fromDate, $toDate);
				 $negotiations = $this->tracker_model->getLastNegotiationByTracker($tracker['tracker_id']);
				 if($negotiations)
				 {
					 $traversedAffiliate = array();
					 foreach($negotiations as $negotiation)
					 {
						 if(!in_array($negotiation['affiliate_id'], $traversedAffiliate))
						 {
							 $negotiation['banner_url'] = $tracker['banner_url'];
							 $negotiation['tracker_name'] = $tracker['tracker_name'];
							 $negotiation['deal_type'] = $tracker['deal_type'];
							 $negotiation['commission'] = $tracker['commission'];
							 $negotiation['ams_commission'] = $tracker['ams_commission'];
							 $negotiation['commission_percentage_hybrid'] = $tracker['commission_percentage_hybrid'];
							 $negotiation['ams_commission_percentage_hybrid'] = $tracker['ams_commission_percentage_hybrid'];
							 $finalNegotiations[] = $negotiation;
							 $traversedAffiliate[] = $negotiation['affiliate_id'];
						 }
					 }
				 }
                 $tracker['clicks'] = $trackerClicks;
                 $allTrackers[$index] = $tracker;

                 switch($tracker['deal_type'])
                 {
                     case 'REVENUE_SHARE':
                         if($trackerClicks)
                         {
                             foreach($trackerClicks as $index => $value)
                             {
                                 if($value['page'] == 'DEPOSIT')
                                 {
                                     $earnings += $value['count'] * $tracker['commission'];
                                 }
                             }
                         }
                         break;
                     case 'CPC':
                         if($trackerClicks)
                         {
                             foreach($trackerClicks as $index => $value)
                             {
                                 if($value['page'] == 'LENDING')
                                 {
                                     $earnings += $value['count'] * $tracker['commission'];
                                 }
                             }
                         }
                         break;
                     case 'CPA':
                         if($trackerClicks)
                         {
                             foreach($trackerClicks as $index => $value)
                             {
                                 if($value['page'] == $tracker['cpa_action'])
                                 {
                                     $earnings += $value['count'] * $tracker['commission'];
                                 }
                             }
                         }
                         break;
                     case 'HYBRID':
						 if($trackerClicks)
                         {
                             foreach($trackerClicks as $index => $value)
                             {
                                 if($value['page'] == 'DEPOSIT')
                                 {
                                     $earnings += $value['count'] * $tracker['commission'];
                                 }
                             }
                         }
                         break;
                 }
             }
         }

         $avgMonthEarnings = 0;
         $profitPerDay = 0;

         if($monthDiff)
         {
             $avgMonthEarnings = $earnings / $monthDiff;
         }
         if($daysDiff)
         {
             $profitPerDay = $earnings / $daysDiff;
         }

		 $allClicks = $this->tracker_model->getTrackersClicksByAdvertiser($affiliateId, $fromDate, $toDate);

		 $totalClicks = 0;
         $totalSignups = 0;
         $totalDeposits = 0;

         if($allClicks)
         {
             foreach($allClicks as $click)
             {
                 switch($click['page'])
                 {
                     case 'LENDING':
                         $totalClicks += $click['count'];
                         break;
                     case 'SIGNUP':
                         $totalSignups += $click['count'];
                         break;
                     case 'DEPOSIT':
                         $totalDeposits += $click['count'];
                         break;
                 }
             }
         }

		 $dealId = isset($_GET['id']) && $_GET['id'] ? $_GET['id'] : '';

		 $myAffiliates = array();
		 $myAffiliatesDetails = array();
		 $myDealAffiliates = array();
		 $myDealIds = array();
		 if($myCampaigns)
		 {
			 foreach($myCampaigns as $campaign)
			 {
				 $lendingClicks = $this->tracker_model->getLendingClicksByAffiliate($campaign['affiliate_id']);
				 $campaignCount = $this->tracker_model->getCampaignsCountByAffiliate($campaign['affiliate_id']);
				 $campaign['campaigns_count'] = count($campaignCount);
				 $avgLendingClicks = 0;
				 if($lendingClicks)
				 {
					 $totalLendingClicks = 0;
					 foreach($lendingClicks as $lendingClick)
					 {
						 $totalLendingClicks += $lendingClick['count'];
					 }

					 $avgLendingClicks = $totalLendingClicks / count($lendingClicks);
				 }
				 $campaign['avg_lending_clicks'] = ceil($avgLendingClicks);
				 if(!in_array($campaign['affiliate_id'], $myAffiliates))
				 {
					 $myAffiliates[] = $campaign['affiliate_id'];
					 $myAffiliatesDetails[] = $campaign;
				 }
				 if($dealId && $dealId == $campaign['tracker_id'] && !in_array($campaign['affiliate_id'], $myDealIds))
				 {
					 $myDealAffiliates[] = $campaign;
					 $myDealIds[] = $campaign['affiliate_id'];
				 }
			 }
			 $advertiserDetail['my_deal_affiliates'] = $myDealAffiliates;
		 }

		 $myRequests = $this->tracker_model->getRequestsByAdvertiser($affiliateId);
		 $excludedIds = array();
		 if($myRequests)
		 {
			 foreach($myRequests as $index => $myRequest)
			 {
				 if(!in_array($myRequest['affiliate_id'], $excludedIds))
				 {
					 $excludedIds[] = $myRequest['affiliate_id'];
					 $lendingClicks = $this->tracker_model->getLendingClicksByAffiliate($myRequest['affiliate_id']);
					 $campaignCount = $this->tracker_model->getCampaignsCountByAffiliate($myRequest['affiliate_id']);
					 $myRequest['campaigns_count'] = count($campaignCount);
					 $avgLendingClicks = 0;
					 if($lendingClicks)
					 {
						 $totalLendingClicks = 0;
						 foreach($lendingClicks as $lendingClick)
						 {
							 $totalLendingClicks += $lendingClick['count'];
						 }

						 $avgLendingClicks = $totalLendingClicks / count($lendingClicks);
					 }
					 $myRequest['avg_lending_clicks'] = ceil($avgLendingClicks);
					 $myRequests[$index] = $myRequest;
				 }
			 }
		 }

		 if($dealId)
		 {
			 $myDealRequests = $this->tracker_model->getDealRequestsById($dealId);
			 $excludedDealIds = array();
			 if($myDealRequests)
			 {
				 foreach($myDealRequests as $index => $myRequest)
				 {
					 if(!in_array($myRequest['affiliate_id'], $excludedDealIds))
					 {
						 $excludedDealIds[] = $myRequest['affiliate_id'];
						 $lendingClicks = $this->tracker_model->getLendingClicksByAffiliate($myRequest['affiliate_id']);
						 $campaignCount = $this->tracker_model->getCampaignsCountByAffiliate($myRequest['affiliate_id']);
						 $myRequest['campaigns_count'] = count($campaignCount);
						 $avgLendingClicks = 0;
						 if($lendingClicks)
						 {
							 $totalLendingClicks = 0;
							 foreach($lendingClicks as $lendingClick)
							 {
								 $totalLendingClicks += $lendingClick['count'];
							 }

							 $avgLendingClicks = $totalLendingClicks / count($lendingClicks);
						 }
						 $myRequest['avg_lending_clicks'] = ceil($avgLendingClicks);
						 $myDealRequests[$index] = $myRequest;
					 }
				 }
				 $advertiserDetail['my_deal_requests'] = $myDealRequests;
			 }

			 $allDealAffiliates = $this->tracker_model->getAllAffiliates($excludedDealIds);
			 $allDealAffiliatesDetails = array();

			 if($allDealAffiliates)
			 {
				 foreach($allDealAffiliates as $affiliate)
				 {
					 if(!in_array($affiliate['affiliate_id'], $myDealIds))
					 {
						 $lendingClicks = $this->tracker_model->getLendingClicksByAffiliate($affiliate['affiliate_id']);
						 $campaignCount = $this->tracker_model->getCampaignsCountByAffiliate($affiliate['affiliate_id']);
						 $affiliate['campaigns_count'] = count($campaignCount);
						 $avgLendingClicks = 0;
						 if($lendingClicks)
						 {
							 $totalLendingClicks = 0;
							 foreach($lendingClicks as $lendingClick)
							 {
								 $totalLendingClicks += $lendingClick['count'];
							 }

							 $avgLendingClicks = $totalLendingClicks / count($lendingClicks);
						 }

						 $affiliate['avg_lending_clicks'] = ceil($avgLendingClicks);
						 $affiliate['deal_id'] = $dealId;
						 $allDealAffiliatesDetails[] = $affiliate;
					 }
				 }
				 $advertiserDetail['all_deal_affiliates'] = $allDealAffiliatesDetails;
			 }
		 }

		 $allAffiliates = $this->tracker_model->getAllAffiliates($excludedIds);
		 $allAffiliatesDetails = array();

		 if($allAffiliates)
		 {
			 foreach($allAffiliates as $affiliate)
			 {
				 if(!in_array($affiliate['affiliate_id'], $myAffiliates))
				 {
					 $lendingClicks = $this->tracker_model->getLendingClicksByAffiliate($affiliate['affiliate_id']);
					 $campaignCount = $this->tracker_model->getCampaignsCountByAffiliate($affiliate['affiliate_id']);
					 $affiliate['campaigns_count'] = count($campaignCount);
					 $avgLendingClicks = 0;
					 if($lendingClicks)
					 {
						 $totalLendingClicks = 0;
						 foreach($lendingClicks as $lendingClick)
						 {
							 $totalLendingClicks += $lendingClick['count'];
						 }

						 $avgLendingClicks = $totalLendingClicks / count($lendingClicks);
					 }

					 $affiliate['avg_lending_clicks'] = ceil($avgLendingClicks);
					 $allAffiliatesDetails[] = $affiliate;
				 }
			 }
		 }

		 $affiliates = $this->tracker_model->getAllAffiliates(array());
		 $totalAffiliates = 0;
		 $newAffiliates = 0;

		 if($affiliates)
         {
             foreach($affiliates as $affiliate)
             {
				 $totalAffiliates++;
				 if(date('m', strtotime($affiliate['created'])) == date('m'))
				 {
					 $newAffiliates++;
				 }
             }
         }

		 $payments = array(
             array('month'=>'01-07-2019', 'amount'=>'5000', 'ams_commission' => '1000', 'final_amount' => '6000', 'status'=>'Paid'),
             array('month'=>'01-08-2019', 'amount'=>'6000', 'ams_commission' => '1500', 'final_amount' => '6500', 'status'=>'Due'),
             array('month'=>'01-09-2019', 'amount'=>'7000', 'ams_commission' => '2000', 'final_amount' => '9000', 'status'=>'Paid')
         );

		 $advertiserDetail['payments'] = $payments;
		 $advertiserDetail['my_affiliates'] = $myAffiliatesDetails;
		 $advertiserDetail['all_affiliates'] = $allAffiliatesDetails;
		 $advertiserDetail['my_requests'] = $myRequests;
		 $advertiserDetail['trackers'] = $trackers;
		 $advertiserDetail['total_clicks'] = $totalClicks + $totalSignups + $totalDeposits;
         $advertiserDetail['total_signups'] = $totalSignups + $totalDeposits;
         $advertiserDetail['total_deposits'] = $totalDeposits;
		 $advertiserDetail['total_affiliates'] = $totalAffiliates;
		 $advertiserDetail['new_affiliates'] = $newAffiliates;
		 $advertiserDetail['avg_month_earning'] = $avgMonthEarnings;
         $advertiserDetail['total_earnings'] = $earnings;
         $advertiserDetail['profit_per_day'] = $profitPerDay;
		 $advertiserDetail['negotiations'] = $finalNegotiations;

		 $this->load->view('advertiser', $advertiserDetail);
	 }

     public function upload()
     {
		 $advertiserDetail = $this->session->user;

		 $uploadFilePath = './uploads/';
		 $fileName = time() . '|' . $_FILES['banner']['name'];
		 $config['upload_path'] = $uploadFilePath;
		 $config['remove_spaces'] = FALSE;
		 $config['file_name'] = $fileName;
		 $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
		 $config['max_size'] = 2048;
		 $config['max_width'] = 500;
		 $config['max_height'] = 500;

		 $this->load->library('upload', $config);

		 if (!$this->upload->do_upload('banner'))
		 {
			 print_r($this->upload->display_errors());
		 }
		 else
		 {
			 $trackerData['tracker_type'] = 1;
			 $trackerData['affiliate_id'] = $advertiserDetail['affiliate_id'];
			 $trackerData['deal_type'] = $this->input->post('deal_type');
			 $trackerData['commission_type'] = 'AMOUNT';
			 $trackerData['cpa_action'] = '';
			 $trackerData['ams_commission'] = 10;
			 if($trackerData['deal_type'] == 'REVENUE_SHARE')
			 {
				 $trackerData['commission_type'] = 'PERCENTAGE';
				 $trackerData['ams_commission'] = 1;
			 }

			 switch($trackerData['deal_type'])
			 {
				 case 'REVENUE_SHARE':
				 	$commission = $this->input->post('revenue_percentage');
					break;
				 case 'CPA':
				 	$commission = $this->input->post('cpa_amount');
					$trackerData['cpa_action'] = $this->input->post('cpa_action');
					break;
				 case 'CPC':
				 	$commission = $this->input->post('cpc_amount');
					break;
				 case 'HYBRID':
				 	$commission = $this->input->post('hybrid_amount');
					$trackerData['commission_type'] = 'PERCENTAGE_AMOUNT';
					$trackerData['commission_percentage_hybrid'] = $this->input->post('hybrid_percentage');
					$trackerData['ams_commission_percentage_hybrid'] = 1;
					$trackerData['cpa_action'] = 'DEPOSIT';
					break;
			 }
			 $trackerData['commission'] = $commission;
			 $trackerData['cpc_click'] = $this->input->post('cpc_click') ? $this->input->post('cpc_click') : 0;
			 $trackerData['banner_url'] = base_url() . 'uploads/' . $fileName;
			 $trackerData['image_name'] = $fileName;
			 $trackerData['comment'] = $this->input->post('comment');
			 $trackerData['website'] = $this->input->post('website');

			 $insertTracker = $this->tracker_model->addTracker($trackerData);
			 if($insertTracker)
			 {
				 $trackerId = $insertTracker[0]['tracker_id'];
				 $trackerData['tracker_name'] = 'TRACKER-' . $trackerId;
				 $trackerData['promotion_code'] = "<div><a href='" . $trackerData['website'] . "?tracker=" . $trackerId . "'><img src='" . $trackerData['banner_url'] . "'></a></div>";

				 $this->tracker_model->updateTracker($trackerData, $trackerId);

				 $trackerDetail = $this->tracker_model->getTrackerById($trackerId);
				 redirect('/advertiser');
			 }
		 }
     }

     public function do_upload()
     {
         $data['userData'] = $this->session->user;
         if($_POST)
         {
             $imageData['title'] = $_POST['title'];
             $imageData['description'] = $_POST['description'];
             $config = array(
                 'upload_path' => './uploads/',
                 'allowed_types' => 'gif|jpg|png|jpeg',
                 'overwrite' => TRUE
             );
             $this->load->library('upload', $config);
             if($this->upload->do_upload('imageToUpload') &&($imageData['title'] != '')
              && ($imageData['description'] != ''))
             {
                 $uplodedImage = array('upload_data' => $this->upload->data());
                 $imageData['url'] = $this->upload->data('file_name');
                 $insertImage = $this->Images_model->addImage($imageData);
                 if($insertImage)
                 {
                     $this->view($insertImage[0]['id']);
                 }

             }
             else
             {
                 echo '<script> alert("Some thing went worng..."); </script>';
                 $this->load->view('image_upload', $data);
             }

         }
     }

	 public function request()
	 {
		 $advertiserDetail = $this->session->user;
		 $data['affiliate_id'] = $this->input->post('affiliate_id');
		 $data['advertiser_id'] = $advertiserDetail['affiliate_id'];
		 $this->tracker_model->addRequest($data);
		 redirect('/advertiser');
	 }

	 public function deal()
	 {
		 $advertiserDetail = $this->session->user;
		 $data['affiliate_id'] = $this->input->post('affiliate_id');
		 $data['tracker_id'] = $this->input->post('tracker_id');
		 $this->tracker_model->addDeal($data);
		 redirect('/advertiser?page=affiliates-deal&id=' . $data['tracker_id']);
	 }
 }
 ?>
