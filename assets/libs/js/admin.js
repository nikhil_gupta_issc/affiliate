$('.stats-brerakdown a').click(function(){
    $('#selected').text($(this).text());
  });

  $('.mothstats a').click(function(){
    $('#monthstats').text($(this).text());
  });


  $(function() {
    $('#cpa-container, #cpc-container, #hybrid-container').hide();
    $('#deal-type').change(function(){
        if($('#deal-type').val() == 'REVENUE_SHARE') {
            $('#rev-share').show();
        } else {
            $('#rev-share').hide();
        }

        if($('#deal-type').val() == 'CPA') {
            $('#cpa-container').show();
        } else {
            $('#cpa-container').hide();
        }

        if($('#deal-type').val() == 'CPC') {
            $('#cpc-container').show();
        } else {
            $('#cpc-container').hide();
        }

        if($('#deal-type').val() == 'HYBRID') {
            $('#hybrid-container').show();
        } else {
            $('#hybrid-container').hide();
        }
    });
});

profits();
affiliates();

function affiliates() {

var ctx = document.getElementById('myAffiliates').getContext('2d');
this.myAffiliates = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['March', 'April', 'May', 'June', 'July', 'August'],
        datasets: [{
            label: ' of Affiliates',
            data: [6, 8, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        events: []
    }
});

}



function newAffiliates() {

    var ctx = document.getElementById('myAffiliates').getContext('2d');
    this.myAffiliates = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['March', 'April', 'May', 'June', 'July', 'August'],
        datasets: [{
            label: ' of New Affiliates',
            data: [1,2, 3, 4, 4, 6],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        events: []
    }
});



}

function signUps() {

    var ctx = document.getElementById('myAffiliates').getContext('2d');
var myAffiliates = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['March', 'April', 'May', 'June', 'July', 'August'],
        datasets: [{
            label: '# of New SignUps',
            data: [1,2, 3, 4, 4, 6],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        events: []
    }
});

}

function ftds() {

    var ctx = document.getElementById('myAffiliates').getContext('2d');
var myAffiliates = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['March', 'April', 'May', 'June', 'July', 'August'],
        datasets: [{
            label: '# of FTDs',
            data: [6,5, 3, 5, 4, 1],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        events: []
    }
});

}

function profits() {

    var ctx = document.getElementById('myAffiliates').getContext('2d');
var myAffiliates = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['March', 'April', 'May', 'June', 'July', 'August'],
        datasets: [{
            label: '# of FTDs',
            data: [6,5, 3, 5, 4, 6],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        events: []
    }
});

}



function profits() {
var ctx = document.getElementById('mySales').getContext('2d');
var mySales = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['March', 'April', 'May', 'June', 'July', 'August'],
        datasets: [{
            label: '# Profits',
            data: [12, 18, 73, 25, 32, 43],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        events: []
    }
});

}


function registration() {
    var ctx = document.getElementById('mySales').getContext('2d');
var mySales = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['March', 'April', 'May', 'June', 'July', 'August'],
        datasets: [{
            label: '# of Registrations',
            data: [10, 16, 23, 25, 32, 43],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        events: []
    }
});
}

function showAffiliate(id){
    document.getElementById("my-affiliates").style.display = "none";
    document.getElementById("all-affiliates").style.display = "none";
    document.getElementById("my-requests").style.display = "none";
    document.getElementById("my-deal-affiliates").style.display = "none";
    document.getElementById("all-deal-affiliates").style.display = "none";
    document.getElementById("my-deal-requests").style.display = "none";
    document.getElementById(id).style.display = "block";

    document.getElementById("my-affiliates-label").className = "btn btn-outline-dark";
    document.getElementById("all-affiliates-label").className = "btn btn-outline-dark";
    document.getElementById("my-requests-label").className = "btn btn-outline-dark";
    document.getElementById("my-deal-affiliates-label").className = "btn btn-outline-dark";
    document.getElementById("all-deal-affiliates-label").className = "btn btn-outline-dark";
    document.getElementById("my-deal-requests-label").className = "btn btn-outline-dark";
    var idLabel = id + "-label";
    document.getElementById(idLabel).className = "btn btn-outline-dark active";
}
